## Introduction ##

A SQL-like language for distributed-memory graph processing.
It supports:

* A subset of SQL that can be efficiently compiled to the vertex-centric model.
* The detection and optimization of several communication patterns that are not well handled in traditional graph processing systems.

The syntax can be found in the folder `doc/`.
Examples are in the folder `examples/`.

## Installation ##

### The Compiler ###

The compiler requires the following softwares:

1. [cmake](https://cmake.org/)
2. a c++ compiler (C++11 compatible)

The following commands build the compiler (Linux, Mac OX):

```
$ cd $(top)
$ mkdir build && cd build
$ cmake ..
$ make
```

The compiler is a single executable `./compile`

### The Graph Processing System ###

We use the [pregel-channel system](https://bitbucket.org/zyz915/pregel-channel) as our backend.
The generated code can be further compiled (by an MPI C++ compiler) and executed with this project.
Install this system and have a try!

## Contact ##

Yongzhe Zhang (Ph.D. student)  
The Graduate University of Advanced Studies (SOKENDAI), Japan  
National Institute of Informatics  
Email: zyz915@nii.ac.jp