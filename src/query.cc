#include <algorithm>
#include <cmath>
#include <cfloat>
#include <exception>

#include "ast.h"
#include "config.h"
#include "intermediate.h"
#include "query.h"
#include "types.h"

using namespace std;

static char buf[120];

// test whether a is a subset of b (binary representation)
static inline bool is_subset(int a, int b) {
	return !(a & ~b);
}

// generate a zero/inf/-inf with specified type
static ConstIR *getNumIR(Type *type, FLAGS spl) {
	const type_info &ti = typeid(*type);
	if (ti == typeid(VidType))
		return new IntIR(0, spl);
	if (ti == typeid(IntType))
		return new IntIR(0, spl);
	if (ti == typeid(FloatType))
		return new FloatIR(0, spl);
	if (ti == typeid(BoolType))
		return new BoolIR(spl == FLAGS::MAX);
	fail("getNumIR() -- type error");
	return nullptr;
}

static CombinerInfo* calcAgg(Type *t, Operator *op) {
	if (op == nullptr) {
		ConstIR *c = getNumIR(t, FLAGS::NORMAL);
		return new CombinerInfo(t, getOper(-1), c);
	}
	switch (op->type) {
	//case tok_max:
	case tok_min:
		return new CombinerInfo(t, op, getNumIR(t, FLAGS::MAX));
	case tok_sum:
		return new CombinerInfo(t, op, getNumIR(t, FLAGS::NORMAL));
	case tok_cnt:
		return new CombinerInfo(getIntType(), op, new IntIR(0));
	//case tok_avg:
	}
	fail("calcAgg() not implemented.");
	return nullptr;
}

VertexSet::VertexSet():Schema(getVidType()) {
}

double VertexSet::numRows() const {
	return getConfig()->n;
}

int VertexSet::numCols() const {
	return 1;
}

Type *VertexSet::getType(int index) const {
	return type_;
}

double VertexTable::numRows() const {
	return getConfig()->n;
}

int VertexTable::numCols() const {
	return 2;
}

Type *VertexTable::getType(int index) const {
	return (index == 0 ? getVidType() : type_);
}

int EdgeTable::numCols() const {
	return (type_->equals(getUnitType()) ? 2 : 3);
}

Type *EdgeTable::getType(int index) const {
	return (index < 2 ? getVidType() : type_);
}

double EdgeTable::numRows() const {
	return getConfig()->m;
}

double GlobalValue::numRows() const {
	return 0.0; // should not reach
}

int GlobalValue::numCols() const {
	return 1;
}

Type *GlobalValue::getType(int index) const {
	return type_;
}

int ExecPlan::step() const {
	return step_ + (getLoc() != getDest());
}

VertexTableAccess::VertexTableAccess(TableExpr *t, Schema *s, int dest, Context *context) {
	if (typeid(*s) != typeid(VertexTable) &&
		typeid(*s) != typeid(VertexSet))
		prtError("Constructor of VertexTableAccess");
	bool has_attr = typeid(*s) == typeid(VertexTable);
	tn_ = t->getName();
	prim_ = context->lookup(t->getVar(0)->getString());
	attr_ = has_attr ? context->lookup(t->getVar(1)->getString()) : -1;
	dest_ = dest;
	mask_ = (1 << prim_);
	if (has_attr)
		mask_ = mask_ | (1 << attr_);
	rows_ = s->numRows();
}

int VertexTableAccess::getLoc() const {
	return prim_;
}

int VertexTableAccess::getDest() const {
	return dest_;
}

int VertexTableAccess::primaryKey() const {
	return prim_;
}

void VertexTableAccess::print(FILE *f) const {
	fprintf(f, "(VTA (Dest %d) %d)", prim_, attr_);
}

TableStat VertexTableAccess::stat(Conjunction *ps, int mask, Context *context) const {
	int bytes = 0;
	for (int i = 0; i < context->size(); i++)
		if (1 & (mask >> i))
			bytes += context->getType(i)->bytes();
	double cost = ((dest_ == -1 || dest_ == prim_) ? 0 : bytes * rows_);
	return TableStat(bytes, rows_, cost);
}

void VertexTableAccess::prepare(IREnv *env, int loopLv) {
	step_ = 0;
}

EdgeTableAccess::EdgeTableAccess(TableExpr *t, Schema *s, int dest, Context *context) {
	if (typeid(*s) != typeid(EdgeTable))
		prtError("Constructor of EdgeTableAccess");
	tn_ = t->getName();
	fst_ = context->lookup(t->getVar(0)->getString());
	snd_ = context->lookup(t->getVar(1)->getString());
	attr_ = (s->numCols() < 3 ? -1 :
			context->lookup(t->getVar(2)->getString()));
	dest_ = dest;
	mask_ = (1 << fst_) | (1 << snd_);
	if (attr_ != -1)
		mask_ = mask_ | (1 << attr_);
	rows_ = s->numRows();
}

int EdgeTableAccess::getLoc() const {
	return fst_;
}

int EdgeTableAccess::getDest() const {
	return dest_;
}

int EdgeTableAccess::primaryKey() const {
	return -1;
}

void EdgeTableAccess::print(FILE *f) const {
	fprintf(f, "(ETA (Dest %d) %d %d %d)", dest_, fst_, snd_, attr_);
}

TableStat EdgeTableAccess::stat(Conjunction *preds, int mask, Context *context) const {
	int bytes = 0;
	for (int i = 0; i < context->size(); i++)
		if (1 & (mask >> i))
			bytes += context->getType(i)->bytes();
	double cost = ((dest_ == -1 || dest_ == fst_) ? 0 : bytes * rows_);
	return TableStat(bytes, rows_, cost);
}

void EdgeTableAccess::prepare(IREnv *env, int loopLv) {
	step_ = 0;
}

JoinPlan::JoinPlan(ExecPlan *a, ExecPlan *b, int comm, int dest) {
	lhs_ = a;
	rhs_ = b;
	// TODO: this is not correct!! it should be top-down!
	mask_ = rhs_->mask() | lhs_->mask();
	fail(a->getDest() != comm, "JoinPlan::JoinPlan() -- lhs");
	fail(b->getDest() != comm, "JoinPlan::JoinPlan() -- rhs");
	comm_ = comm;
	dest_ = dest;
}

int JoinPlan::getLoc() const {
	return comm_;
}

int JoinPlan::getDest() const {
	return dest_;
}

int JoinPlan::primaryKey() const {
	int pr1 = lhs_->primaryKey();
	int pr2 = rhs_->primaryKey();
	if (pr1 == -1 || pr2 == -1) return -1;
	if (comm_ == pr1) return pr2;
	if (comm_ == pr2) return pr1;
	return -1;
}

static Conjunction *emp = new Conjunction();
TableStat JoinPlan::stat(Conjunction *ps, int mask, Context *context) const {
	// int m = ~(1 << dest_) & mask;
	TableStat ls = lhs_->stat(emp, mask & lhs_->mask(), context);
	TableStat rs = rhs_->stat(emp, mask & rhs_->mask(), context);
	// the column size
	int bytes = 0;
	for (int i = 0; i < context->size(); i++)
		if (is_subset(1 << i, mask))
			bytes += context->getType(i)->bytes();
	// estimate table size
	int pr = primaryKey();
	double rows = ls.rows * rs.rows;
	if (pr == dest_)
		rows = 1.0;
	// estimate transmission cost
	double cost = ls.cost + rs.cost;
	if (dest_ != -1 && comm_ != dest_)
		cost += rows * bytes;
	return TableStat(bytes, rows, cost);
}

void JoinPlan::print(FILE *f) const {
	fprintf(f, "(JoinPlan (Dest %d) ", dest_);
	lhs_->print(f);
	fprintf(f, " ");
	rhs_->print(f);
	fprintf(f, ")");
}

void JoinPlan::prepare(IREnv *env, int loopLv) {
	lhs_->prepare(env, loopLv);
	rhs_->prepare(env, loopLv);
	step_ = max(lhs_->step(), rhs_->step());
}

RequestPlan::RequestPlan(ExecPlan *a, ExecPlan *b, int comm, int dest) {
	req_ = a;
	resp_ = b;
	mask_ = a->mask() | b->mask();
	fail(a->getDest() != dest, "RequestPlan::RequestPlan() -- req");
	fail(b->getDest() != comm, "RequestPlan::RequestPlan() -- resp");
	comm_ = comm;
	dest_ = dest;
}

int RequestPlan::getLoc() const {
	fail("should not be invoked");
	return dest_;
}

int RequestPlan::getDest() const {
	return dest_;
}

int RequestPlan::primaryKey() const {
	return req_->primaryKey();
}

TableStat RequestPlan::stat(Conjunction *ps, int mask, Context *context) const {
	TableStat ls = req_->stat(emp, mask & req_->mask(), context);
	TableStat rs = resp_->stat(emp, mask & resp_->mask(), context);
	// the column size
	int bytes = 0;
	for (int i = 0; i < context->size(); i++)
		if (is_subset(1 << i, mask))
			bytes += context->getType(i)->bytes();
	// estimate table size & plan cost
	double cost = ls.cost + rs.cost + ls.rows * rs.bytes * (1.0 - exp(ls.rows));
	return TableStat(bytes, ls.rows, cost);
}

int RequestPlan::step() const {
	return step_ + 2;
}

void RequestPlan::print(FILE *f) const {
	fprintf(f, "(RequestPlan (Dest %d) ", dest_);
	req_->print(f);
	fprintf(f, " ");
	resp_->print(f);
	fprintf(f, ")");
}

void RequestPlan::prepare(IREnv *env, int loopLv) {
	req_->prepare(env, loopLv);
	resp_->prepare(env, loopLv);
	step_ = max(req_->step(), resp_->step());
}

ScatterPlan::ScatterPlan(ExecPlan *a, ExecPlan *b, int comm, int dest, int lv) {
	lhs_ = a;
	rhs_ = b;
	mask_ = a->mask() | b->mask();
	fail(comm == dest, "ScatterPlan::ScatterPlan() -> (comm == dest)");
	fail(a->getDest() != comm, "ScatterPlan::ScatterPlan() -- lhs");
	fail(b->getDest() != comm, "ScatterPlan::ScatterPlan() -- rhs");
	comm_ = comm;
	dest_ = dest;
	lv_ = lv; // in which loop
}

int ScatterPlan::getLoc() const {
	return comm_;
}

int ScatterPlan::getDest() const {
	return dest_;
}

int ScatterPlan::primaryKey() const {
	return -1;
}

TableStat ScatterPlan::stat(Conjunction *ps, int mask, Context *context) const {
	TableStat ls = lhs_->stat(emp, mask & lhs_->mask(), context);
	TableStat rs = rhs_->stat(emp, mask & rhs_->mask(), context);
	// the column size
	int bytes = 0;
	for (int i = 0; i < context->size(); i++)
		if (is_subset(1 << i, mask))
			bytes += context->getType(i)->bytes();
	// estimate table size
	double n = getConfig()->n;
	double m = getConfig()->m;
	int w = getConfig()->w;
	double est = n*(w-1)*(1-pow(1-1.0/w, ls.rows/n));
	double cost = ls.cost + rs.cost + est;
	return TableStat(bytes, ls.rows, cost);
}

int ScatterPlan::step() const {
	return step_ + 1;
}

void ScatterPlan::print(FILE *f) const {
	fprintf(f, "(ScatterPlan (Dest %d) ", dest_);
	lhs_->print(f);
	fprintf(f, " ");
	rhs_->print(f);
	fprintf(f, ")");
}

void ScatterPlan::prepare(IREnv *env, int loopLv) {
	fail(loopLv == 0, "ScatterPlan::prepare() -- outside a loop");
	lhs_->prepare(env, 0);
	rhs_->prepare(env, loopLv);
	step_ = rhs_->step();
}

void GroupPlan::print(FILE *f) const {
	fprintf(f, "(GroupPlan ");
	u_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, " ");
	preds_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void GroupPlan::prepare(IREnv *env, int loopLv) {
	p_->prepare(env, loopLv);
}

void ConstantPlan::print(FILE *f) const {
	fprintf(f, "(ConstantPlan (Func %s))", str_.c_str());
}

void ConstantPlan::prepare(IREnv *env, int loopLv) {
}

void AggregatePlan::print(FILE *f) const {
	fprintf(f, "(AggregatePlan %s ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void AggregatePlan::prepare(IREnv *env, int loopLv) {
	p_->prepare(env, loopLv);
}

void ComposePlan::print(FILE *f) const {
	fprintf(f, "(ComposePlan ");
	p1_->print(f);
	fprintf(f, " ");
	p2_->print(f);
	fprintf(f, ")");
}

void ComposePlan::prepare(IREnv *env, int loopLv) {
	p1_->prepare(env, loopLv);
	p2_->prepare(env, loopLv);
}

void IteratePlan::prepare(IREnv *env, int loopLv) {
	p_->prepare(env, loopLv + 1);
}

void ForRangePlan::print(FILE *f) const {
	fprintf(f, "(ForRangePlan %s %d %d ",
			name_->getString().c_str(), lo_, hi_);
	p_->print(f);
	fprintf(f, ")");
}

void FixedPointPlan::print(FILE *f) const {
	fprintf(f, "(FixedPointPlan %s ", tn_->getString().c_str());
	p_->print(f);
	fprintf(f, ")");
}

Collection *VertexTableAccess::selectedColumns(IREnv *env, Context *context,
		Conjunction *conj, int mask) {
	Type *t = context->getType(attr_);
	int id = env->getMM()->addVertexTable(tn_, t);
	if (getLoc() == getDest()) {
		context->bind(prim_, new VertexId(id));
		context->bind(attr_, new VertexAttr(id, t));
		return new VertexArray(id);
	}
	int ch = env->getCM()->allocate(new MsgChannel(t));
	env->add(0, new ScanArray(new VertexArray(id), Conj2IR(
			conj->trans(context, mask_), new SendMessage(
			ch, new VertexAttr(id, t), new VertexId(id)))));
	env->add(0, new Activate(ch));
	int tid = env->getMM()->allocate(new LocalVector(t));
	env->add(1, new VectorFromChannel(ch, tid));
	VecValue *val = new VecValue(tid, new PairType(t, t)); // key-value pair
	context->bind(attr_, new SelectIR(val, 0, t));
	context->bind(prim_, new SelectIR(val, 1, t));
	return new Vector(tid);
}

VertexArray *VertexTableAccess::evaluateExpr(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) {
	Type *t = context->getType(attr_);
	int id = env->getMM()->addVertexTable(tn_, t);
	context->bind(prim_, new VertexId(id));
	context->bind(attr_, new VertexAttr(id, t));
	ExprIR *expr = e->toIR(context);
	if (getLoc() == getDest()) {
		env->add(0, new MapArray(new VertexArray(id), tid, expr));
		return new VertexArray(tid);
	} else {
		fail(t != getVidType(), "VertexTableAcces::evalExpr() - type error");
		ExprIR *expr = e->toIR(context);
		Type *et = expr->type();
		int ch = env->getCM()->allocate(new Combiner(info));
		env->add(0, new ScanArray(new VertexArray(id), Conj2IR(
				conj->trans(context, mask_), new SendMessage(
				ch, new VertexAttr(id, t), expr))));
		env->add(0, new Activate(ch));
		env->add(1, new ArrayFromChannel(ch, tid));
		return new VertexArray(tid);			
	}
}

AggValue *VertexTableAccess::aggregate(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info) {
	Type *t = context->getType(attr_);
	int id = env->getMM()->addVertexTable(tn_, t);
	context->bind(prim_, new VertexId(id));
	context->bind(attr_, new VertexAttr(id, t));
	ExprIR *expr = e->toIR(context);
	int ch = env->getCM()->allocate(new Aggregator(info));
	env->add(0, new InitAgg(ch));
	env->add(0, new OmpForReduction(ch,
			new ScanArray(new VertexArray(id), Conj2IR(
			conj->trans(context, mask_), new SetAgg(ch, expr)))));
	env->add(0, new AllReduce(ch));
	return new AggValue(ch, info->t);
}

void EdgeTableAccess::vectorize(IREnv *env, Context *context) {
	Type *v = getVidType();
	Type *thd = context->getType(attr_);
	int id = env->getMM()->addEdgeTable(tn_, thd);
	Type *t = env->getMM()->getSchema(id)->type(); // second & third columns
	VecValue *x = new VecValue(id, new PairType(v, t));
	context->bind(fst_, new SelectIR(x, 0, v));
	SelectIR *y = new SelectIR(x, 1, t);
	context->bind(snd_, new SelectIR(y, 0, v));
	if (attr_ != -1)
		context->bind(attr_, new SelectIR(y, 1, thd));
}

Collection *EdgeTableAccess::selectedColumns(IREnv *env, Context *context,
		Conjunction *conj, int mask) {
	int id = env->getMM()->addEdgeTable(tn_, context->getType(attr_));
	vectorize(env, context);
	if (getLoc() == getDest())
		return new Vector(id);
	Constructor *c = new Constructor();
	Type *ct = c->type(); // channel's type
	Type *rt = new PairType(getVidType(), ct);
	int tid = env->getMM()->allocate(new LocalVector(ct));
	int ch = env->getCM()->allocate(new MsgChannel(ct));
	auto cs = conj->trans(context, mask_);
	for (int idx = 0, i = 0; i < context->size(); i++)
		if (i != snd_ && is_subset(1 << i, mask)) {
			c->add(context->trans(i));
			context->bind(i, new SelectIR(new SelectIR(new VecValue(tid, rt), 1, ct),
					idx++, context->getType(i)));
		}
	env->add(0, new ScanVector(new Vector(id), Conj2IR(
			cs, new SendMessage(ch, context->trans(snd_), c))));
	env->add(0, new Activate(ch));
	env->add(1, new VectorFromChannel(ch, tid));
	context->bind(snd_, new SelectIR(new VecValue(tid, rt), 0, getVidType()));
	return new Vector(tid, 1);
}

VertexArray *EdgeTableAccess::evaluateExpr(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) {
	int id = env->getMM()->addEdgeTable(tn_, context->getType(attr_));
	vectorize(env, context);
	ExprIR *expr = e->toIR(context);
	Type *et = expr->type();
	if (getLoc() == getDest()) {
		int ch = env->getCM()->allocate(new GroupBy(info));
		env->add(0, new ScanVector(new Vector(id), new SendMessage(
				ch, context->trans(getDest()), expr)));
		context->bind(getDest(), new VertexId(id));
		env->add(0, new ArrayFromChannel(ch, tid));
		return new VertexArray(tid);
	} else {
		int ch = env->getCM()->allocate(new Combiner(info));
		env->add(0, new ScanVector(new Vector(id), new SendMessage(
				ch, context->trans(getDest()), expr)));
		env->add(0, new Activate(ch));
		context->bind(getDest(), new VertexId(id));
		env->add(1, new ArrayFromChannel(ch, tid));
		return new VertexArray(tid);
	}
}

AggValue *EdgeTableAccess::aggregate(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info) {
	int id = env->getMM()->addEdgeTable(tn_, context->getType(attr_));
	vectorize(env, context);
	ExprIR *expr = e->toIR(context);
	Type *et = expr->type();
	int ch = env->getCM()->allocate(new Aggregator(info));
	env->add(0, new InitAgg(ch));
	env->add(0, new OmpForReduction(ch,
			new ScanVector(new Vector(id), new SetAgg(ch, expr))));
	env->add(0, new AllReduce(ch));
	return new AggValue(ch, info->t);
}

Collection *JoinPlan::selectedColumns(IREnv *env, Context *context,
		Conjunction *conj, int mask) {
	int mask_p = conj->exclude(lhs_->mask(), rhs_->mask());
	int mask_l = ((mask | mask_p) & lhs_->mask()) | (1 << comm_);
	int mask_r = ((mask | mask_p) & rhs_->mask()) | (1 << comm_);
	Collection *lc = lhs_->selectedColumns(env, context, conj, mask_l);
	Collection *rc = rhs_->selectedColumns(env, context, conj, mask_r);
	Constructor *cont = new Constructor();
	ExprIR *dest = context->trans(dest_);
	// case 1: local join
	if (getDest() == getLoc()) {
		// case 1.1.1: inner table is a vertex table
		if (lc->uniqueKey() && rc->uniqueKey())
			return new ZippedArray((ArrayIR*) lc, (ArrayIR*) rc);
		Constructor *c = new Constructor();
		Type *ct = c->type();
		Type *rt = new PairType(getVidType(), c->type());
		int ch = env->getCM()->allocate(new CreateVec(ct));
		int id = env->getMM()->allocate(new LocalVector(ct));
		auto cs = conj->trans(context, mask_);
		for (int idx = 0, i = 0; i < context->size(); i++)
			if ((mask & (1 << i)) && i != dest_) {
				c->add(context->trans(i));
				context->bind(i, new SelectIR(new SelectIR(new VecValue(id, rt), 1, ct), idx++,
						context->getType(i)));
			}
		context->bind(dest_, new SelectIR(new VecValue(id, rt), 0, getVidType()));
		// case 1.1.2: inner table is an edge table
		if (lc->uniqueKey() && !rc->uniqueKey()) {
			env->add(step_, new Join2((ArrayIR*) lc, (VectorIR*) rc, Conj2IR(
					cs, new SendMessage(ch, dest, c))));
			env->add(step_, new VectorFromChannel(ch, id));
			return new Vector(id, 1);
		// case 1.2: outer loop is an edge table
		} else {
			// case 1.2.1: inner loop is a vertex table
			if (rc->uniqueKey()) {
				env->add(step_, new Join2((ArrayIR*) rc, (VectorIR*) lc, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			// case 1.2.2: inner loop is an edge table
			} else {
				env->add(step_, new Join((VectorIR*) lc, (VectorIR*) rc, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			}
		}
	// case 2: remote join
	} else {
		Constructor *c = new Constructor();
		Type *ct = c->type();
		Type *rt = new PairType(getVidType(), c->type());
		int ch = env->getCM()->allocate(new MsgChannel(ct));
		int id = env->getMM()->allocate(new LocalVector(ct));
		auto cs = conj->trans(context, mask_);
		for (int idx = 0, i = 0; i < context->size(); i++)
			if ((mask & (1 << i)) && i != dest_) {
				c->add(context->trans(i));
				context->bind(i, new SelectIR(new SelectIR(new VecValue(id, rt), 1, ct), idx++,
						context->getType(i)));
			}
		context->bind(dest_, new SelectIR(new VecValue(id, rt), 0, getVidType()));
		// case 2.1: outer table is a vertex table
		if (lc->uniqueKey()) {
			// case 2.1.1: inner table is a vertex table
			if (rc->uniqueKey()) {
				ZippedArray *arr = new ZippedArray((ArrayIR*) lc, (ArrayIR*) rc);
				env->add(step_, new ScanArray(arr, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			}
			// case 2.1.2: inner table is an edge table
			else {
				env->add(step_, new Join2((ArrayIR*) lc, (VectorIR*) rc, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			}
		// case 2.2: outer table is an edge table
		} else {
			// case 2.2.1: inner table is a vertex table
			if (rc->uniqueKey()) {
				env->add(step_, new Join2((ArrayIR*) rc, (VectorIR*) lc, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			// case 2.2.2: inner table is an edge table
			} else {
				env->add(step_, new Join((VectorIR*) lc, (VectorIR*) rc, Conj2IR(
						cs, new SendMessage(ch, dest, c))));
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new VectorFromChannel(ch, id));
				return new Vector(id, 1);
			}
		}
	}
	prtError("should not reach the end of JoinPlan::selectedColumns()");
	return nullptr;
}

VertexArray *JoinPlan::evaluateExpr(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) {
	// mask_p: the mask of predicates that can be only evaluated on the current node
	int mask_p = conj->exclude(lhs_->mask(), rhs_->mask());
	int mask_l = (mask_p & lhs_->mask()) | (1 << comm_);
	int mask_r = (mask_p & rhs_->mask()) | (1 << comm_);
	Collection *lc = lhs_->selectedColumns(env, context, conj, mask_l);
	Collection *rc = rhs_->selectedColumns(env, context, conj, mask_r);
	ExprIR *dest = context->trans(dest_);
	ExprIR *expr = e->toIR(context);
	bool local = getLoc() == getDest();
	// case 1: outer table is a vertex table
	if (lc->uniqueKey()) {
		// case 1.1: inner table is a vertex table
		if (rc->uniqueKey()) {
			ZippedArray *za = new ZippedArray((ArrayIR*) lc, (ArrayIR*) rc);
			if (local) {
				env->add(step_, new MapArray(za, tid, expr));
				return new VertexArray(tid);
			} else {
				int ch = env->getCM()->allocate(new Combiner(info));
				env->add(step_, new ScanArray(za, Conj2IR(
						conj->trans(context, mask_), new SendMessage(ch, dest, expr))));
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new ArrayFromChannel(ch, tid));
				return new VertexArray(tid);
			}
		// case 1.2: inner table is an edge table
		} else {
			int ch = (local ? env->getCM()->allocate(new GroupBy(info))
							: env->getCM()->allocate(new Combiner(info)));
			env->add(step_, new Join2((ArrayIR*) lc, (VectorIR*) rc, Conj2IR(
					conj->trans(context, mask_), new SendMessage(ch, dest, expr))));
			if (!local) {
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new ArrayFromChannel(ch, tid));
			} else
				env->add(step_, new ArrayFromChannel(ch, tid));
			return new VertexArray(tid);
		}
	// case 2: outer table is an edge table
	} else {
		// case 2.1: inner table is a vertex table
		if (rc->uniqueKey()) {
			int ch = (local ? env->getCM()->allocate(new GroupBy(info))
							: env->getCM()->allocate(new Combiner(info)));
			env->add(step_, new Join2((ArrayIR*) rc, (VectorIR*) lc, Conj2IR(
					conj->trans(context, mask_), new SendMessage(ch, dest, expr))));
			if (!local) {
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new ArrayFromChannel(ch, tid));
			} else
				env->add(step_, new ArrayFromChannel(ch, tid));
			return new VertexArray(tid);
		} else {
			int ch = (local ? env->getCM()->allocate(new GroupBy(info))
							: env->getCM()->allocate(new Combiner(info)));
			env->add(step_, new Join((VectorIR*) rc, (VectorIR*) lc, Conj2IR(
					conj->trans(context, mask_), new SendMessage(ch, dest, expr))));
			if (!local) {
				env->add(step_, new Activate(ch));
				env->add(step_ + 1, new ArrayFromChannel(ch, tid));
			} else
				env->add(step_, new ArrayFromChannel(ch, tid));
			return new VertexArray(tid);
		}
	}
	prtError("should not reach the end of JoinPlan::evalExpr()");
	return nullptr;
}

AggValue *JoinPlan::aggregate(IREnv *env, Context *context,
		 Conjunction *conj, ExprAST *e, CombinerInfo *info) {
	int mask_p = conj->exclude(lhs_->mask(), rhs_->mask());
	int mask_l = (mask_p & lhs_->mask()) | (1 << comm_);
	int mask_r = (mask_p & rhs_->mask()) | (1 << comm_);
	Collection *lc = lhs_->selectedColumns(env, context, conj, mask_l);
	Collection *rc = rhs_->selectedColumns(env, context, conj, mask_r);
	ExprIR *expr = e->toIR(context);
	Type *t = expr->type();
	int ch = env->getCM()->allocate(new Aggregator(info));
	// case 1: outer table is a vertex table
	if (lc->uniqueKey()) {
		// case 1.1: inner table is a vertex table
		if (rc->uniqueKey()) {
			ZippedArray *za = new ZippedArray((ArrayIR*) lc, (ArrayIR*) rc);
			env->add(step_, new InitAgg(ch));
			env->add(step_, new OmpForReduction(ch,
					new ScanArray(za, Conj2IR(
					conj->trans(context, mask_), new SetAgg(ch, expr)))));
			env->add(step_, new AllReduce(ch));
			return new AggValue(ch, t);
		// case 1.2: inner table is an edge table
		} else {
			env->add(step_, new InitAgg(ch));
			env->add(step_, new Join2((ArrayIR*) lc, (VectorIR*) rc, Conj2IR(
					conj->trans(context, mask_), new SetAgg(ch, expr))));
			env->add(step_, new AllReduce(ch));
			return new AggValue(ch, t);
		}
	// case 2: outer table is an edge table
	} else {
		// case 2.1: inner table is a vertex table
		if (rc->uniqueKey()) {
			env->add(step_, new InitAgg(ch));
			env->add(step_, new Join2((ArrayIR*) rc, (VectorIR*) lc, Conj2IR(
					conj->trans(context, mask_), new SetAgg(ch, expr))));
			env->add(step_, new AllReduce(ch));
			return new AggValue(ch, t);
		// case 2.2: both tables are edge tables
		} else {
			env->add(step_, new InitAgg(ch));
			env->add(step_, //new OmpForReduction(ch,  // still has bug!!!!
					new Join((VectorIR*) rc, (VectorIR*) lc, Conj2IR(
					conj->trans(context, mask_), new SetAgg(ch, expr)))); //);
			env->add(step_, new AllReduce(ch));
			return new AggValue(ch, t);
		}
	}
	prtError("should not reach the end of JoinPlan::aggregate()");
	return nullptr;
}

Collection *RequestPlan::selectedColumns(IREnv *env, Context *context,
		Conjunction *conj, int mask) {
	Constructor *c = new Constructor();
	int ch = env->getCM()->allocate(new ReqChannel(c->type()));
	int mask_r = mask & resp_->mask() & ~(1 << comm_);
	Collection *rc = resp_->selectedColumns(env, context, conj, mask_r);
	for (int idx = 0, i = 0; i < context->size(); i++)
		if (is_subset(1 << i, mask_r)) {
			c->add(context->trans(i));
			context->bind(i, new SelectIR(new GetRespValue(ch, c->type()),
					idx++, context->getType(i)));
		}
	// handle the respond phase first
	fail(!rc->uniqueKey(), "RequestPlan::selectedColumns() -- unexpected error");
	if (typeid(*resp_) == typeid(VertexTableAccess) && typeid(*rc) == typeid(VertexArray))
		env->add(step_ + 1, new RespondIR(ch, ((VertexArray*)rc)->getId()));
	else {
		int t = env->getMM()->allocate(new LocalArray(c->type()));
		env->add(step_ + 1, new MapArray((ArrayIR*) rc, t, c));
		env->add(step_ + 1, new RespondIR(ch, t));
		env->add(step_ + 1, new ClearArray(t));
	}
	// join the respond values & request table
	int mask_l = mask_ & ~mask_r;
	Collection *lc = req_->selectedColumns(env, context, conj, mask_l);
	ExprIR *comm = context->trans(comm_);
	ExprIR *dest = context->trans(dest_);
	if (lc->uniqueKey()) {
		int tid = env->getMM()->allocate(new LocalArray(c->type()));
		for (int idx = 0, i = 0; i < context->size(); i++)
			if (is_subset(1 << i, mask_r))
				context->bind(i, new SelectIR(new VecValue(tid, c->type()),
						idx++, context->getType(i)));
		lc->inc();
		if (typeid(*lc) == typeid(VertexArray)) {
			env->add(step_, new RequestIR(ch, ((VertexArray*) lc)->getId()));
		} else {
			env->add(step_, new ScanArray((ArrayIR*) lc, new AddRequestIR(ch, comm)));
			env->add(step_, new Activate(ch));
		}
		env->add(step_ + 2, new ArrayFromChannel(ch, tid));
		return new ZippedArray((ArrayIR*) lc, new VertexArray(tid, 1));
	} else {
		int tid = env->getMM()->allocate(new LocalArray(c->type()));
		for (int idx = 0, i = 0; i < context->size(); i++)
			if (is_subset(1 << i, mask_r))
				context->bind(i, new SelectIR(new VecValue(tid, c->type()),
						idx++, context->getType(i)));
		lc->inc();
		env->add(step_, new ScanVector((VectorIR*) lc, new AddRequestIR(ch, comm)));
		env->add(step_, new Activate(ch));
		env->add(step_ + 2, new ArrayFromChannel(ch, tid));
		return new ZippedVector((VectorIR*) lc, new Vector(tid, 1));
	}
}

VertexArray *RequestPlan::evaluateExpr(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) {
	if (is_subset(e->mask(), resp_->mask())) {
		int ch = env->getCM()->allocate(new ReqChannel(e->type()));
		// handle the respond phase first
		Collection *rc = resp_->selectedColumns(env, context, conj, e->mask());
		fail(!rc->uniqueKey(), "RequestPlan::evalExpr() -- unexpected error");
		if (typeid(*resp_) == typeid(VertexTableAccess) && typeid(*rc) == typeid(VertexArray)) {
			env->add(step_ + 1, new RespondIR(ch, ((VertexArray*)rc)->getId()));
		} else {
			int t = env->getMM()->allocate(new LocalArray(e->type()));
			env->add(step_ + 1, new MapArray((ArrayIR*) rc, t, e->toIR(context)));
			env->add(step_ + 1, new RespondIR(ch, t));
			env->add(step_ + 1, new ClearArray(ch));
		}
		// handle the request phase later
		Collection *lc = req_->selectedColumns(env, context, conj, (1 << comm_) | (1 << dest_));
		ExprIR *comm = context->trans(comm_);
		ExprIR *dest = context->trans(dest_);
		if (lc->uniqueKey()) {
			lc->inc();
			if (typeid(*lc) == typeid(VertexArray)) {
				env->add(step_, new RequestIR(ch, ((VertexArray*) lc)->getId()));
			} else {
				env->add(step_, new ScanArray((ArrayIR*) lc, new AddRequestIR(ch, comm)));
				env->add(step_, new Activate(ch));
			}
			//env->add(step_ + 2, new MapArray((ArrayIR*) lc, tid,
			//		new GetRespValue(ch, e->type())));
			env->add(step_ + 2, new ArrayFromChannel(ch, tid));
			return new VertexArray(tid);
		} else {
			lc->inc();
			env->add(step_, new ScanVector((VectorIR*) lc, new AddRequestIR(ch, comm)));
			env->add(step_, new Activate(ch));
			int gb = env->getCM()->allocate(new GroupBy(info));
			env->add(step_ + 2, new ScanVector((VectorIR*) lc,
					new SendMessage(gb, dest, new GetRespValue(ch, e->type()))));
			env->add(step_ + 2, new ClearChannel(ch));
			env->add(step_ + 2, new ArrayFromChannel(gb, tid));
			return new VertexArray(tid);
		}
	} else {
		Constructor *c = new Constructor();
		int ch = env->getCM()->allocate(new ReqChannel(c->type()));
		int mask = e->mask() & ~(1 << comm_);
		for (int idx = 0, i = 0; i < context->size(); i++)
			if (is_subset(1 << i, mask)) {
				c->add(context->trans(i));
				context->bind(i, new SelectIR(new GetRespValue(ch, c->type()),
						idx++, context->getType(i)));
			}
		// handle the respond phase first
		Collection *rc = resp_->selectedColumns(env, context, conj, mask);
		fail(!rc->uniqueKey(), "RequestPlan::evalExpr() -- unexpected error");
		int t = env->getMM()->allocate(new LocalArray(c->type()));
		env->add(step_ + 1, new MapArray((ArrayIR*) rc, t, c));
		env->add(step_ + 1, new RespondIR(ch, t));
		env->add(step_ + 1, new ClearArray(t));
		// handle the request phase later
		int mask_l = (1 << comm_) | (1 << dest_) | (e->mask() & req_->mask());
		Collection *lc = req_->selectedColumns(env, context, conj, mask_l);
		ExprIR *comm = context->trans(comm_);
		ExprIR *dest = context->trans(dest_);
		if (lc->uniqueKey()) {
			if (typeid(*lc) == typeid(VertexArray)) {
				env->add(step_, new RequestIR(ch, ((VertexArray*) lc)->getId()));
			} else {
				env->add(step_, new ScanArray((ArrayIR*) lc, new AddRequestIR(ch, comm)));
				env->add(step_, new Activate(ch));
			}
			env->add(step_ + 2, new MapArray((ArrayIR*) lc, tid,
					e->toIR(context)));
			return new VertexArray(tid);
		} else {
			env->add(step_, new ScanVector((VectorIR*) lc, new AddRequestIR(ch, comm)));
			env->add(step_, new Activate(ch));
			int gb = env->getCM()->allocate(new GroupBy(info));
			env->add(step_ + 2, new ScanVector((VectorIR*) lc,
					new SendMessage(gb, dest, e->toIR(context))));
			env->add(step_ + 2, new ClearChannel(ch));
			env->add(step_ + 2, new ArrayFromChannel(gb, tid));
			return new VertexArray(tid);
		}
	}
	fail("RequestPlan::evalExpr() -- should not reach");
}

AggValue *RequestPlan::aggregate(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info) {
	fail("RequestPlan::aggregate()");
	return nullptr;
}

Collection *ScatterPlan::selectedColumns(IREnv *env, Context *context,
		Conjunction *conj, int mask) {
	// should not reach
	fail("ScatterPlan::selectedColumns()");
	return nullptr;
}

VertexArray *ScatterPlan::evaluateExpr(IREnv *env, Context *context,
		Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) {
	Collection *lc = lhs_->selectedColumns(env, context, conj, lhs_->mask());
	Collection *rc = rhs_->selectedColumns(env, context, conj, rhs_->mask());
	ExprIR *dest = context->trans(dest_);
	ExprIR *expr = e->toIR(context);
	fail( lc->uniqueKey(), "ScatterPlan::evalExpr() -- LHS");
	fail(!rc->uniqueKey(), "ScatterPlan::evalExpr() -- RHS");
	int ch = env->getCM()->allocate(new ScatterCombine(info));
	lc->inc();
	auto cs = conj->trans(context, mask_);
	//env->addInit(lv_, new Join2((ArrayIR*) rc, (VectorIR*) lc, Conj2IR(
	//		cs, new AddEdge(ch, ((ArrayIR*) rc)->getId(), dest))));
	//env->addInit(lv_, new Activate(ch));
	if (typeid(*rc) == typeid(VertexArray) && typeid(*expr) == typeid(VertexAttr) &&
			((VertexArray*) rc)->getId() == ((VertexAttr*) expr)->getId()) {
		env->add(step_, new SetMessage(ch, ((VertexArray*) rc)->getId()));
	} else {
		int id = env->getMM()->allocate(new LocalArray(e->type()));
		env->add(step_, new MapArray((ArrayIR*) rc, id, expr));
		env->add(step_, new SetMessage(ch, id));
		env->add(step_, new ClearArray(id));
	}
	env->add(step_ + 1, new ArrayFromChannel(ch, tid));
	return new VertexArray(tid);
}

AggValue *ScatterPlan::aggregate(IREnv *env, Context *context,
		 Conjunction *conj, ExprAST *e, CombinerInfo *info) {
	// should not reach
	fail("ScatterPlan::aggregate()");
	return nullptr;
}

// fuse optimization (program, initialization, jumping, termination condition)
static SeqSTM *fuse(SeqSTM *p, Superstep *I, Superstep *J, ExprIR *tc) {
	if (getConfig()->fuse && p->isReading()) {
		SeqSTM *ret = new SeqSTM(false, p->dupStep(), nullptr);
		SeqSTM *app = new SeqSTM(false, p->dupStep(), nullptr);
		CondSTM *c = new CondSTM(tc, (SeqSTM*) p->next(),
				new SeqSTM(false, new Superstep(), nullptr));
		SeqSTM *j = new SeqSTM(false, J, nullptr);
		j->setJump(c);
		app->append(j);
		p->next()->append(app);
		SeqSTM *i = new SeqSTM(false, I, nullptr);
		ret->append(i);
		ret->add(c);
		return ret;
	} else {
		CondSTM *c = new CondSTM(tc, p, new SeqSTM(false, new Superstep(), nullptr));
		SeqSTM *j = new SeqSTM(false, J, nullptr);
		j->setJump(c);
		p->append(j);
		return new SeqSTM(false, I, c);
	}
}

SeqSTM *GroupPlan::transform(IREnv *env) {
	env->reset(p_->step());
	Type *t = expr_->type();
	int ret = env->getMM()->addVertexTable(ret_, t);
	CombinerInfo *info = calcAgg(t, op_);
	VertexArray *c = p_->evaluateExpr(env, context_, preds_, expr_, info, ret);
	if (!preds_->all_marked()) {
		debug(this);
		debug(preds_->filter(p_->mask()));
		fail("GroupPlan::transform() -- unevaluated predicates");
	}
	return env->produceSTM();
}

SeqSTM *ConstantPlan::transform(IREnv *env) {
	env->reset(0);
	int id = env->getMM()->addGlobalValue(ret_, type_);
	VarIR *var = new VarIR(type_, id);
	env->add(0, new AssignIR(var, new BuiltinIR(str_, type_)));
	return env->produceSTM();
}

SeqSTM *AggregatePlan::transform(IREnv *env) {
	env->reset(p_->step());
	Type *t = expr_->type();
	CombinerInfo *info = calcAgg(t, op_);
	AggValue *av = p_->aggregate(env, context_, preds_, expr_, info);
	int id = env->getMM()->addGlobalValue(ret_, t);
	VarIR *var = new VarIR(t, id);
	env->add(p_->step(), new AssignIR(var, av));
	if (!preds_->all_marked()) {
		debug(this);
		debug(preds_->filter(p_->mask()));
		fail("AggregatePlan::transform() -- unevaluated predicates");
	}
	return env->produceSTM();
}

SeqSTM *ComposePlan::transform(IREnv *env) {
	SeqSTM *s1 = p1_->transform(env);
	SeqSTM *s2 = p2_->transform(env);
	s1->append(s2);
	return s1;
}

SeqSTM *ForRangePlan::transform(IREnv *env) {
	int id = env->getMM()->allocateVar(getIntType());
	VarIR *var = new VarIR(getIntType(), id);
	env->enterLoop();
	env->addInit(lv_, new AssignIR(var, new IntIR(lo_)));
	SeqSTM *b = p_->transform(env);
	Superstep *S = new Superstep(); // end of the loop body
	Superstep *E = new Superstep(); // exit of the loop
	S->add(new AssignIR(var, new BinaryIR(getIntType(),
			getOper(tok_add), var, new IntIR(1))));
	ExprIR *cmp = new BinaryIR(getBoolType(),
			getOper(tok_lt), var, new IntIR(hi_));
	SeqSTM *i = env->exitLoop();
	return fuse(b, i->head(), S, cmp);
}

SeqSTM *FixedPointPlan::transform(IREnv *env) {
	CombinerInfo *info = new CombinerInfo(getBoolType(),
			getOper(tok_and), new BoolIR(true));
	int agg = env->getCM()->allocate(new Aggregator(info)); // control the loop
	int dup = env->getMM()->allocate(new LocalArray(t_));
	int src = env->getMM()->addVertexTable(tn_, t_);
	env->enterLoop();
	env->addInit(lv_, new ArrayCopy(dup, src));
	env->addInit(lv_, new InitAgg(agg));
	SeqSTM *b = p_->transform(env);
	Superstep *P = new Superstep(); // beginning of the loop body
	Superstep *S = new Superstep(); // end of the loop body
	P->append(b->head());
	S->add(new ArrayCompare(agg, dup, src));  // TODO position seems wrong
	S->add(new AllReduce(agg));
	S->add(new ArrayCopy(dup, src));
	SeqSTM *loop = new SeqSTM(false, P, b->next());
	ExprIR *tc = new UnaryIR(getBoolType(), getOper(tok_not),
			new AggValue(agg, getBoolType()));
	SeqSTM *i = env->exitLoop();
	return fuse(loop, i->head(), S, tc);
}

using vt = const pair<string, Type*>;
Context::Context(const std::vector<TableExpr*> rels, TypeEnv *env) {
	// variables & relations
	for (TableExpr *tab : rels) {
		Schema *s = env->getSchema(tab->getName());
		int sz = tab->size();
		if (typeid(*s) == typeid(GlobalValue)) {
			glb_vars_.push_back(make_pair(tab->getName(),
					tab->getVar(0)));
		} else {
			for (int i = 0; i < sz; i++) {
				vars_.push_back(make_pair(
					tab->getVar(i)->getString(), // variable
					s->getType(i))); // type
			}
			rels_.push_back(tab);
		}
	}
	auto cmp = [](const vt &a, const vt &b) {
		return a.first < b.first;
	};
	sort(vars_.begin(), vars_.end(), cmp);
	vars_.erase(unique(vars_.begin(), vars_.end()), vars_.end());
	// mask
	int join_cols = 0;
	for (TableExpr *tab : rels_) {
		int sz = tab->size(), mask = 0;
		// set tab's mask
		for (int i = 0; i < sz; i++) {
			int p = lookup(tab->getVar(i)->getString());
			mask = mask | (1 << p);
		}
		tab->setMask(mask);
		// potential join columns
		for (int i = 0; i < min(sz, 2); i++) {
			int p = lookup(tab->getVar(i)->getString());
			fail(p == -1, "Context::Context()");
			if (getType(p)->equals(getVidType()))
				join_cols = join_cols | (1 << p);
		}
	}
	binds_.resize(vars_.size());
	/*
	fprintf(stderr, "join_columns: [");
	for (int i = 0; i < vars_.size(); i++)
		if (1 & (join_cols >> i))
			fprintf(stderr, " %s", vars_[i].first.c_str());
	fprintf(stderr, " ]\n");
	*/
}

int Context::lookup(const std::string &s) const {
	int lo = 0, hi = vars_.size();
	while (lo + 1 < hi) {
		int mid = (lo + hi) >> 1;
		if (strcmp(s.c_str(), vars_[mid].first.c_str()) < 0)
			hi = mid;
		else
			lo = mid;
	}
	int cmp = strcmp(s.c_str(), vars_[lo].first.c_str());
	return (cmp == 0 ? lo : -1);
}

int Context::size() const {
	return vars_.size();
}

Type *Context::getType(int index) const {
	if (index >= vars_.size())
		return getUnitType(); // not exist
	return vars_[index].second;
}

const string &Context::getVar(int index) const {
	return vars_[index].first;
}

const vector<TableExpr*> &Context::rels() const {
	return rels_;
}

bool Context::isPrimaryKey(Identifier *v, TypeEnv *env) const {
	int idx = lookup(v->getString());
	fail(idx == -1, "column not found");
	// v must be the primary key of all vertex tables (wrong!)
	for (TableExpr *tab : rels_)
		if (is_subset(1 << idx, tab->mask())) {
			Schema *s = env->getSchema(tab->getName());
			if (typeid(*s) == typeid(EdgeTable))
				return false;
			if (!tab->getVar(0)->equals(v))
				return false;
		}
	return true;
}

void Context::showVars(int mask, const char *msgs) const {
	fprintf(stderr, "(mask %d) %s:", mask, msgs);
	for (int i = 0; i < vars_.size(); i++)
		if (is_subset(1 << i, mask))
			fprintf(stderr, " %s", vars_[i].first.c_str());
	fprintf(stderr, "\n");
}

void Context::bind(int idx, ExprIR *expr) {
	if (0 <= idx && idx < binds_.size())
		binds_[idx] = expr; // just replace
}

ExprIR* Context::trans(int idx) const {
	return binds_[idx];
}

ExprIR* Context::trans(const std::string &s, CodeGen *cg) const {
	for (int i = 0; i < vars_.size(); i++)
		if (vars_[i].first == s)
			return binds_[i];
	for (int i = 0; i < glb_vars_.size(); i++)
		if (glb_vars_[i].second->equals(s.c_str())) {
			int id = cg->getMM()->addGlobalValue(
					glb_vars_[i].first, glb_vars_[i].second->type());
			return new VarIR(glb_vars_[i].second->type(), id);
		}
	sprintf(buf, "Context::trans() -- variable %s not found", s.c_str());
	fail(buf);
	return nullptr;
}

/*
bool Context::contains(int id) const {
	return used_.find(id) != used_.end();
}

void Context::add(int id) {
	vars_.push_back(id);
	used_.insert(id);
}

void Context::add(Table* t) {
	rels_.push_back(t);
	if (!contains(t->src))
		add(t->src);
	if (!contains(t->dst))
		add(t->dst);
	if (t->prop != -1 && !contains(t->prop))
		add(t->prop);
	int x = index(t->src);
	int y = index(t->dst);
	int z = (t->prop != -1 ? index(t->prop) : x);
	t->mask = (1 << x) | (1 << y) | (1 << z);
}

int Context::index(int id) const {
	return find(vars_.begin(), vars_.end(), id) - vars_.begin();
}

int Context::size() const {
	return vars_.size();
}

const vector<int> &Context::vars() const {
	return vars_;
}

const vector<Table*> &Context::rels() const {
	return rels_;
}
*/

void Context::dump(FILE *f) const {
	fprintf(f, "###### [ Context ] ######\n");
	fprintf(f, "Variables:");
	for (int i = 0; i < vars_.size(); i++)
		fprintf(f, " %s", vars_[i].first.c_str());
	fprintf(f, "\n");
	fprintf(f, "Relations:\n");
	for (auto rel : rels_) {
		fprintf(f, " ");
		rel->print(f);
		fprintf(f, "\n");
	}
	fprintf(f, "-------------------------\n");
}

SQLQuery::SQLQuery(Context *c, Conjunction *ps):context(c) {
	exprs = new ExprList();
	preds = new Conjunction();
	for (int i = 0; i < ps->size(); i++) {
		ps->get(i)->calcMask(c);
		preds->add(ps->get(i));
	}
}

void SQLQuery::addExpr(ExprAST *expr) {
	expr->calcMask(context);
	exprs->add(expr);
}

void SQLQuery::dump(FILE *f) const {
	context->dump(f);
	fprintf(f, "####### [ Query ] #######\n");
	for (int i = 0; i < exprs->size(); i++) {
		fprintf(f, "Expression %d: ", i);
		debug(exprs->get(i), f);
	}
	for (int i = 0; i < preds->size(); i++) {
		fprintf(f, "Predicate %d: [%d] ", i, preds->get(i)->mask());
		debug(preds->get(i), f);
	}
	//fprintf(f, "Mask: %d\n", mask);
	//fprintf(f, "Destination: %d\n", dest);
	fprintf(f, "-------------------------\n");
}

/*
static void print_vars(const vector<int> &vars, int mask) {
	printf("Variables :");
	for (int i = 0; i < vars.size(); i++)
		if (1 & (mask >> i))
			printf(" %d", vars[i]);
	printf("\n");
}

static void print_rels(const vector<Table*> rels) {
	printf("Relations :");
	for (auto rel : rels) {
		printf(" ");
		rel->dump(stdout);
	}
	printf("\n");
}

static void print_mask(int v, int len) {
	for (int i = 0; i < len; i++)
		printf("%d", 1 & (v >> i));
}
*/

static int calc_mask(int s, int m, int l) {
	int ret = 0, c = 1;
	while (m > 0) {
		if (m & 1) {
			if (s & 1)
				ret ^= c;
			s >>= 1;
		}
		m >>= 1;
		c <<= 1;
	}
	return ret;
}

static int calc_index(int s, int m) {
	for (int i = 0; i < m; i++)
		if (s == (1 << i))
			return i;
	return -1;
}

static ExecPlan *eval_one_table(TableExpr *t, Schema *s, int dest, Context *context) {
	if (typeid(*s) == typeid(VertexSet))
		return new VertexTableAccess(t, s, dest, context);
	if (typeid(*s) == typeid(VertexTable))
		return new VertexTableAccess(t, s, dest, context);
	if (typeid(*s) == typeid(EdgeTable))
		return new EdgeTableAccess(t, s, dest, context);
	prtError("eval_one_table");
	return nullptr;
}

static void update(Context *context, ExecPlan *sols[],
		double costs[], int mask, int st, ExecPlan *p) {
	TableStat stat = p->stat(emp, mask, context);
	if (stat.cost < costs[st]) {
		sols[st] = p;
		costs[st] = stat.cost;
	}
}

ExecPlan *solve(SQLQuery *query, TypeEnv *env) {
	// whether to turn on the optimization
	Config *conf = getConfig();
	bool opt_switch = conf->opt && !strcmp(conf->target, "pregel-channel");
	LoopInfo *loop = env->getLoopInfo();
	// definitions
	Context *context = query->context;
	const vector<TableExpr*> &rels = context->rels();
	const int m = context->size();
	const int n = rels.size();
	const int s = 1 << n, mask = s - 1;
	// all states (sols[m*s] is the return plan)
	ExecPlan **sols = new ExecPlan*[m * s + 1];
	for (int st = 0; st <= m * s; st++)
		sols[st] = nullptr;
	// test whether two tables have a common variable
	int *adj = new int[n];
	memset(adj, 0, sizeof(int) * n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (i != j && rels[i]->mask() & rels[j]->mask())
				adj[i] ^= 1 << j;
	// test whether a subset of relations is valid
	bool *valid = new bool[s];
	memset(valid, 0, s);
	for (int i = 0; i < n; i++)
		valid[1 << i] = true;
	for (int i = 0; i < s; i++)
		for (int j = 0; j < n; j++)
			valid[i] |= (1 & (i >> j)) &&
					valid[i ^ (1 << j)] && (i & adj[j]);
	// test the occured variables of a subset of relations
	int *vmask = new int[s];
	memset(vmask, 0, sizeof(int) * s);
	for (int i = 0; i < n; i++) {
		const int t = 1 << i;
		vmask[t] = rels[i]->mask();
		for (int j = 1; j < t; j++)
			vmask[t ^ j] = vmask[t] | vmask[j];
	}
	// the size of the subset (number of 1s in the binary representation)
	int *ones = new int[s];
	memset(ones, 0, sizeof(int) * s);
	for (int i = 1; i < s; i++)
		ones[i] = ones[i >> 1] + (i & 1);
	// whether a table is a vertex table
	bool *is_vt = new bool[n];
	int *prim = new int[n]; // primary key
	memset(is_vt, false, sizeof(bool) * n);
	for (int i = 0; i < n; i++) {
		Schema *s = env->getSchema(rels[i]->getName());
		is_vt[i] = typeid(*s) == typeid(VertexTable);
		prim[i] = context->lookup(rels[i]->getVar(0)->getString());
	}
	// the primary key of a subset of tables (-1 if not exist)
	int *pri = new int[s];
	memset(pri, -1, sizeof(int) * s);
	for (int i = 0; i < n; i++)
		pri[1 << i] = (is_vt[i] ? prim[i] : -1);
	for (int i = 1; i < s; i++) // i: a set of tables
		if (pri[i] != -1)       // i has a primary key
			for (int j = 0; j < n; j++) // j: a vertex table
				if (is_vt[j] && !is_subset(1 << j, i)) {
					if (is_subset(1 << pri[i], vmask[1 << j]))
						pri[i ^ (1 << j)] = prim[j];
					if (is_subset(1 << prim[j], vmask[i]))
						pri[i ^ (1 << j)] = pri[i];
				}
	delete[] is_vt;
	delete[] prim;
	// the cost (costs[m*s] is the cost of the return plan)
	double *costs = new double[m * s + 1];
	for (int i = 0; i <= m * s; i++)
		costs[i] = DBL_MAX;
	// dynamic programming
	// base case: a single table
	for (int i = 0; i < n; i++) { // table
		Schema *s = env->getSchema(rels[i]->getName());
		for (int j = 0; j < m; j++)  // partitioned by which column
			if (is_subset(1 << j, rels[i]->mask())) {
				int st = (j << n) ^ (1 << i);
				sols[st] = eval_one_table(rels[i], s, j, context);
				costs[st] = sols[st]->stat(emp, rels[i]->mask(), context).cost;
			}
	}
	int emask = 0, pmask = query->preds->mask();
	for (int i = 0; i < query->exprs->size(); i++)
		emask = emask | query->exprs->get(i)->mask();
	loop->calcMask(context);
	int lv = loop->calcLevel(pmask);
	// emask = (1 << m) - 1;
	for (int i = 1; i < s; i++) // iterate over all subsets of relations
		if (valid[i] && ones[i] > 1) { // valid subset and size > 1
			int subset = (1 << ones[i]) - 1;
			for (int j = 1; j < subset; j++) {
				int mx = calc_mask(j, i, ones[i]);
				int my = i ^ mx;
				// v: the only common column of two subsets of relations
				int v = calc_index(vmask[mx] & vmask[my], m);
				if (valid[mx] && valid[my] && v != -1) {
					ExecPlan *px = sols[(v << n) ^ mx];
					ExecPlan *py = sols[(v << n) ^ my];
					fail (px == nullptr || py == nullptr,
							"unsolved sub-problem");
					// case 1: join on column v
					for (int k = 0; k < m; k++)
						if (is_subset(1 << k, vmask[i])) {
							ExecPlan *r = new JoinPlan(px, py, v, k);
							update(context, sols, costs, vmask[i], (k << n) ^ i, r);
						}
					// case 2: request-respond paradigm
					if (opt_switch)
					for (int k = 0; k < m; k++)
						if (pri[mx] == k) // TODO
						if (is_subset(1 << k, vmask[mx]) && k != v && pri[my] == v) {
							ExecPlan *pk = sols[(k << n) ^ mx];
							if (pk != nullptr) {
								ExecPlan *r = new RequestPlan(pk, py, v, k);
								update(context, sols, costs, vmask[i], (k << n) ^ i, r);
							}
						}
					// case 3: scatter-combine
					if (opt_switch)
					if (i == mask && query->has_group && loop->insideLoop()) {
						for (int k = 0; k < m; k++)
							if (is_subset(1 << k, vmask[mx]) && k != v && pri[mx] == -1
									&& ones[mx] == 1 && pri[my] == v && lv != -1) {
								ExecPlan *pk = sols[(v << n) ^ mx];
								ExecPlan *r = new ScatterPlan(pk, py, v, k, lv);
								update(context, sols, costs, vmask[i], (k << n) ^ i, r);
							}
					}
				}
			}
		}
	// destination
	double cost = DBL_MAX;
	ExecPlan *ret = nullptr;
	if (query->is_agg) {
		for (int c = 0; c < m; c++) {
			int st = (c << n) ^ mask;
			if (costs[st] < cost) {
				cost = costs[st];
				ret = sols[st];
			}
		}
	} else {
		int d = query->dest;
		fail(sols[(d << n) ^ mask] == nullptr, "no return value");
		cost = costs[(d << n) ^ mask]; 
		ret = sols[(d << n) ^ mask];
	}
	delete[] pri;
	delete[] adj;
	delete[] valid;
	delete[] vmask;
	delete[] ones;
	delete[] sols;
	delete[] costs;
	return ret;
}
