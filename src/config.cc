#include <cstdlib>
#include <cstring>

#include "config.h"
#include "errorinfo.h"

void Config::check() {
	if (n > m)
		prtError("invalid parameter (n should not be greater than m)");
}

static Config *conf = new Config();
Config *getConfig() {
	return conf;
}
