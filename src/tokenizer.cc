#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <vector>

#include "tokenizer.h"
#include "errorinfo.h"

using namespace std;

static int line = 0;

static char buff[256];
static int bptr;
static int no_tok;

static char br_stack[256], br_top;

static FILE *fin = stdin;
void setInputStream(FILE *f) {
	if (f == nullptr) 
		prtError("input file does not exist");
	fin = f;
}

static struct { int type, cnt; } indent;
static void getLine() {
	line += 1;
	no_tok = (fgets(buff, 256, fin) == nullptr);
	setline(line, buff);
	if (!no_tok) {
		bptr = 0;
		int l = strlen(buff);
		while (buff[bptr] && isspace(buff[bptr]))
			bptr++;
	}
}

static Token createToken(int row, int col, int type, char *val = nullptr) {
	return Token(row, col + 1, type, val);
}

static void copyString(char *dst, char *src, int len) {
	memcpy(dst, src, len);
	dst[len] = 0;
}

typedef pair<const char *, int> TokRec;
static vector<TokRec> names;

static bool cmp(const TokRec &a, const TokRec &b) {
	return strcmp(a.first, b.first) < 0;
}

void initTokenizer() {
	names.clear();
	names.push_back(make_pair("SELECT", tok_select));
	names.push_back(make_pair("FROM", tok_from));
	names.push_back(make_pair("WHERE", tok_where));
	names.push_back(make_pair("JOIN", tok_join));
	names.push_back(make_pair("AND", tok_and));
	names.push_back(make_pair("OR", tok_or));
	names.push_back(make_pair("NOT", tok_not));
	names.push_back(make_pair("IN", tok_in));
	names.push_back(make_pair("GROUP", tok_group));
	names.push_back(make_pair("BY", tok_by));
	names.push_back(make_pair("SET", tok_set));
	names.push_back(make_pair("WHILE", tok_while));
	names.push_back(make_pair("DO", tok_do));
	names.push_back(make_pair("UNTIL", tok_until));
	names.push_back(make_pair("FIX", tok_fix));
	names.push_back(make_pair("END", tok_end));
	names.push_back(make_pair("FOR", tok_for));
	names.push_back(make_pair("TO", tok_to));
	names.push_back(make_pair("IF", tok_if));
	names.push_back(make_pair("THEN", tok_then));
	names.push_back(make_pair("ELSE", tok_else));
	names.push_back(make_pair("INPUT", tok_input));
	names.push_back(make_pair("VERTEX_TABLE", tok_vt));
	names.push_back(make_pair("EDGE_TABLE", tok_et));
	names.push_back(make_pair("GLOBAL_VALUE", tok_gv));
	names.push_back(make_pair("IN_DEGREE", tok_id));
	names.push_back(make_pair("OUT_DEGREE", tok_od));
	names.push_back(make_pair("COMBINER", tok_combiner));
	names.push_back(make_pair("IDENTITY", tok_identity));
	names.push_back(make_pair("TRUE", tok_bval));
	names.push_back(make_pair("FALSE", tok_bval));
	names.push_back(make_pair("INF", tok_inf));
	names.push_back(make_pair("VOID", tok_void));
	names.push_back(make_pair("VID", tok_vid));
	names.push_back(make_pair("INT", tok_int));
	names.push_back(make_pair("BOOL", tok_bool));
	names.push_back(make_pair("FLOAT", tok_float));
	names.push_back(make_pair("MAX", tok_max));
	names.push_back(make_pair("MIN", tok_min));
	names.push_back(make_pair("SUM", tok_sum));
	names.push_back(make_pair("COUNT", tok_cnt));
	names.push_back(make_pair("AVERAGE", tok_avg));
	sort(names.begin(), names.end(), cmp);
}

int strcasecmp(const char *a, const char *b) {
	for (;; a++, b++) {
		int d = tolower((unsigned char)*a) - tolower((unsigned char)*b);
		if (d != 0 || !*a)
			return d;
	}
	return 0;
}

static Token lookupName(char *buf, int col) {
	static char upper[64]; 
	int lo = -1, hi = names.size(), mid, p = 0;
	for (; buf[p]; p++)
		upper[p] = toupper(buf[p]);
	upper[p] = 0;

	while (lo + 1 < hi) {
		mid = (lo + hi) >> 1;
		int c = strcmp(upper, names[mid].first);
		if (c == 0)
			return createToken(line, col, 
				names[mid].second, strdup(buf));
		if (c < 0)
			hi = mid;
		else
			lo = mid;
	}
	if (islower(*buf))
		return createToken(line, col, tok_name, strdup(buf));
	else
		return createToken(line, col, tok_tname, strdup(buf));
}

static Token gettok() {
	static char buf_[32];
	while (!no_tok && !buff[bptr])
		getLine();
	if (no_tok)
		return createToken(line, 0, tok_eof);

	while (true) {
		char ch = buff[bptr];
		int col = bptr++;
		if (ch == 0) {
			getLine();
			return gettok(); // reach EOL
		}
		if (isspace(ch))
			continue;
		if (isalpha(ch)) {
			while (isalnum(buff[bptr]) || buff[bptr] == '_'
					|| buff[bptr] == '\'') bptr++;
			copyString(buf_, buff + col, bptr - col);
			return lookupName(buf_, col);
		}
		if (isdigit(ch)) {
			while (isdigit(buff[bptr]))
				bptr++;
			if (buff[bptr] == '.') {
				bptr++;
				while (isdigit(buff[bptr])) bptr++;
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_fval, strdup(buf_));
			} else {
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_ival, strdup(buf_));
			}
		}
		switch (ch) {
		case '(':
			br_stack[++br_top] = '(';
			return createToken(line, col, tok_lparen);
		case ')':
			if (br_stack[br_top] != '(')
				prtError(Pos(line, col), "mismatched parenthesis");
			br_top -= 1;
			return createToken(line, col, tok_rparen);
		case '<':
			if (!strncmp(buff + col, "<=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_le);
			} else
			if (!strncmp(buff + col, "<>", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ne);
			} else
				return createToken(line, col, tok_lt);
		case '>':
			if (!strncmp(buff + col, ">=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ge);
			} else
				return createToken(line, col, tok_gt);
		case '+':
			return createToken(line, col, tok_add);
		case '-':
			return createToken(line, col, tok_sub);
		case '*':
			return createToken(line, col, tok_mul);
		case '/':
			if (!strncmp(buff + col, "//", 2)) {
				getLine();
				return gettok();
			}
			return createToken(line, col, tok_div);
		case '%':
			return createToken(line, col, tok_mod);
		case '=':
			return createToken(line, col, tok_eq);
		case '^':
			return createToken(line, col, tok_xor);
		case ',':
			return createToken(line, col, tok_comma);
		case ':':
			return createToken(line, col, tok_colon);
		case ';':
			return createToken(line, col, tok_semicolon);
		case '.':
			return createToken(line, col, tok_dot);
		}
	}
	return createToken(line, 0, tok_eof);
}

static vector<Token> store;
static int ckp = 0; // checkpoint
static int cur = -1; // current position
Token CurTok;

void lexerSetCP() {
	ckp = store.size() - 1;
}

void lexerRollBack() {
	cur = ckp;
	CurTok = store[cur];
}

void lexerClear() {
	ckp = cur = 0;
	store.clear();
}

Token getNextToken() {
	cur++;
	if (cur == store.size())
		store.push_back(gettok());
	//printf("%d %s\n", cur, getTokenStr(store[cur]));
	return CurTok = store[cur];
}

static const char *token_strs[] = {
	"", "EOF",
	"_I", "_F", "_B", // basic data types
	"name", "tname",
	"(", ")", ".", ",", ":", ";",
	"SELECT", "FROM", "WHERE", "JOIN", "AND", "OR",
	"NOT", "IN", "GROUP", "BY", "SET", "WHILE","DO",
	"UNTIL", "FIX", "END", "FOR", "TO", "IF", "THEN",
	"ELSE", "INPUT", "void", "vid", "int", "bool", "float",
	"VERTEX_TABLE", "EDGE_TABLE", "GLOBAL_VALUE",
	"IN_DEGREE", "OUT_DEGREE", "COMBINER", "IDENTITY", "INF",
	"+", "-", "*", "/", "%", "^", "=", "<>", "<", "<=", ">", ">=",
	"MAXIMUM", "MINIMUM", "SUM", "COUNT", "AVERAGE",
	"_", ""  // others
};

const char *getTokenStr(Token tok) {
	if ((tok.type >= tok_ival && tok.type <= tok_tname))
		return tok.val;
	return token_strs[tok.type];
}

const char *getTokenStr(int type) {
	return token_strs[type];
}

const char *getCppTokenStr(Token tok) {
	return getCppTokenStr(tok.type);
}

const char *getCppTokenStr(int type) {
	switch (type) {
		case tok_and: return "&&";
		case tok_or : return "||";
		case tok_eq: return "==";
		case tok_ne: return "!=";
		default: {
			fail(type < tok_add || type > tok_ge,
					"getCppTokenStr(): unexpected operator");
			return token_strs[type];
		}
	}
}