#ifndef INTERMEDIATE_H
#define INTERMEDIATE_H

#include <map>
#include <vector>

#include "codegen.h"
#include "printable.h"

// from ast.h
class TableName;
class Operator;
class Program;
class Header;
// from types.h
class Type;
// from this file
class ConstIR;

// for aggregator
struct CombinerInfo {
	Type *t; // TODO: looks redundant
	Operator *op;
	ConstIR *e;

	CombinerInfo(Type *t, Operator *op, ConstIR *e):
			t(t), op(op), e(e) {}

	std::string getOp1() const; // binary op  (e.g., sum -> "+")
	std::string getOp2() const; // assignment (e.g., sum -> "+=")
	std::string getOp3() const; // MPI op     (e.g., sum -> "MPI_SUM")
	std::string getIdentity() const;
};

// abstract class
class ChannelInfo {
protected:
	Type *type_;

public:
	ChannelInfo(Type *t):type_(t) {}
	virtual ~ChannelInfo() {}

	virtual Type *type() const final;
	virtual CombinerInfo *getCombiner() const = 0;
	virtual void genDef(CodeGen *cg, const std::string &cn) const = 0;
	virtual void genLoad(CodeGen *cg, const std::string &cn) const {}
};

class MsgChannel : public ChannelInfo {
public:
	MsgChannel(Type *t):ChannelInfo(t) {}
	MsgChannel(Type *t, CombinerInfo *info):ChannelInfo(t) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

// no message passing!
class Combiner : public ChannelInfo {
private:
	CombinerInfo *comb_;

public:
	Combiner(CombinerInfo *info):ChannelInfo(info->t), comb_(info) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

class ScatterCombine : public ChannelInfo {
private:
	CombinerInfo *comb_;

public:
	ScatterCombine(CombinerInfo *info):ChannelInfo(info->t), comb_(info) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
	void genLoad(CodeGen *cg, const std::string &cn) const override;
};

class GroupBy : public ChannelInfo {
private:
	CombinerInfo *comb_;

public:
	GroupBy(CombinerInfo *info):ChannelInfo(info->t), comb_(info) {
		prtError("GroupBy -- not supported yet!!");
	}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

class CreateVec : public ChannelInfo {
public:
	CreateVec(Type *t):ChannelInfo(t) {
		//prtError("CreateVec -- not supported yet!!");
	}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

class ReqChannel : public ChannelInfo {
public:
	ReqChannel(Type *t):ChannelInfo(t) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

class Aggregator : public ChannelInfo {
private:
	CombinerInfo *comb_;

public:
	Aggregator(Type *t, Operator *op, ConstIR *e):
			ChannelInfo(t), comb_(new CombinerInfo(t, op, e)) {}
	Aggregator(CombinerInfo *info):
			ChannelInfo(info->t), comb_(info) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};

/*
class PeerToPeer : public ChannelInfo {
public:
	PeerToPeer(Type *t):ChannelInfo(t) {}

	CombinerInfo *getCombiner() const override;
	void genDef(CodeGen *cg, const std::string &cn) const override;
};*/

class ChannelManager {
private:
	int cid_; // unique id for channels
	int pid_; // unique id for peer-to-peer channel

	// channels and their information
	std::map<int, ChannelInfo*> channels_;
	std::map<int, int> c2p_;

public:
	ChannelManager():cid_(0), pid_(0) {}

	int allocate(ChannelInfo *info);
	int allocate(Type *t);
	void remove(int ch);
	ChannelInfo *getInfo(int ch);
	CombinerInfo *getCombiner(int ch);
	std::string getName(int ch) const;

	// code generation
	void loadChannels(CodeGen *cg) const; // load
	void listChannels(CodeGen *cg) const;
	void listConstructors(CodeGen *cg) const;
};

// abstract class, the type of an object in memory
class MSchema {
protected:
	Type *type_;

public:
	MSchema(Type *t):type_(t) {}

	virtual Type *type() const final { return type_; }
	virtual void genDef(CodeGen *cg, const std::string &n) const = 0;
};

class LocalArray : public MSchema {
public:
	LocalArray(Type *t):MSchema(t) {}

	void genDef(CodeGen *cg, const std::string &n) const override;
};

class LocalVector : public MSchema {
public:
	LocalVector(Type *t):MSchema(t) {}

	void genDef(CodeGen *cg, const std::string &n) const override;
};

class LocalValue : public MSchema {
public:
	LocalValue(Type *t):MSchema(t) {}

	void genDef(CodeGen *cg, const std::string &n) const override;
};

class MemoryManager {
private:
	int cid_; // table id (to identify different tables & local values)
	int vid_; // variable id (exclude the vertex/edge tables)

	// type of the array/iterator
	std::map<std::string, int> name2id_;
	std::map<int, std::string> id2name_;
	std::map<int, MSchema*> schema_;
	std::vector<Header*> headers_;

	Type *val_; // edge type

public:
	MemoryManager():cid_(0), val_(getUnitType()) {}

	int allocate(MSchema *s);
	int allocateVar(Type *t);
	int addVertexTable(TableName *tn_, Type *t);
	int addEdgeTable(TableName *tn_, Type *t);
	int addGlobalValue(TableName *tn_, Type *t);
	MSchema *getSchema(int id);
	void setIO(const std::vector<Header*> &headers);
	Type* getEdgeType() { return val_; }
	// for code generation
	std::string getName(int id) const;
	//void addP2Pchannel(CodeGen *cg);
	void defineTables(CodeGen *cg);
	void parseLine(CodeGen *cg);
	void initTables(CodeGen *cg);
	void dumpTables(CodeGen *cg);
};

/*
class VariableManager {
private:
	int cid_;

	std::map<int, Type*> types_;

public:
	VariableManager() {}

	int allocateVar(Type *t);
	// for code generation
	void defineVariables(CodeGen *cg);
	std::string getName(int id) const;
};
*/

// abstract class
class IR : public Printable {
public:
	IR() {}
	virtual ~IR() {}
};

class StmtIR : public IR {
public:
	StmtIR() {}
	virtual ~StmtIR() {}

	virtual void genChannelBased(CodeGen *cg) {}
};

// abstract class
class ExprIR : public IR {
protected:
	Type *type_; // the inferenced type

public:
	ExprIR(Type *t):type_(t) {}
	virtual ~ExprIR() {}

	// precedence of the outmost operation
	virtual int prec() const { return 100; }
	virtual Type* type() const { return type_; }
	virtual void genChannelBased(CodeGen *cn, Text *text) {}
};

class VarIR : public ExprIR {
private:
	int id_;

public:
	VarIR(Type *t, int id):ExprIR(t), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

// global value (different from normal variable)
class GValue : public ExprIR {
private:
	int id_; // this id is in the memory manager
};

class ConstIR : public ExprIR {
public:
	ConstIR(Type *t):ExprIR(t) {}
	virtual ~ConstIR() {}

	virtual std::string toString() const = 0;
};

enum class FLAGS : int { MIN = -1, NORMAL = 0, MAX = 1 };

class IntIR : public ConstIR {
private:
	int val_;
	FLAGS flag_;

public:
	IntIR(int val, FLAGS flag = FLAGS::NORMAL):
			ConstIR(getIntType()), val_(val), flag_(flag) {}

	std::string toString() const override;
	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class FloatIR : public ConstIR {
private:
	double val_;
	FLAGS flag_;

public:
	FloatIR(double val, FLAGS flag = FLAGS::NORMAL):
			ConstIR(getFloatType()), val_(val), flag_(flag) {}

	std::string toString() const override;
	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class BoolIR : public ConstIR {
private:
	bool val_;

public:
	BoolIR(bool val):ConstIR(getBoolType()), val_(val) {}

	std::string toString() const override;
	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class MakePair : public ExprIR {
protected:
	ExprIR *e1_, *e2_;

public:
	MakePair(Type *t, ExprIR *e1, ExprIR *e2):
		ExprIR(t), e1_(e1), e2_(e2) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class BinaryIR : public ExprIR {
protected:
	Operator *op_;
	ExprIR *e1_, *e2_;
	
public:
	BinaryIR(Type *t, Operator *op, ExprIR *e1, ExprIR *e2):
			ExprIR(t), op_(op), e1_(e1), e2_(e2) {}

	int prec() const override;
	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class IfExprIR : public ExprIR {
protected:
	ExprIR *cond_, *e1_, *e2_;

public:
	IfExprIR(Type *t, ExprIR *cond, ExprIR *e1, ExprIR *e2):
			ExprIR(t), cond_(cond), e1_(e1), e2_(e2) {}
			
	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class UnaryIR : public ExprIR {
protected:
	Operator *op_;
	ExprIR *expr_;

public:
	UnaryIR(Type *t, Operator *op, ExprIR *expr):
			ExprIR(t), op_(op), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class AggValue : public ExprIR {
private:
	int ch_;

public:
	AggValue(int ch, Type *t): ExprIR(t), ch_(ch) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class IndexIR : public ExprIR {
private:
	int key_;

public:
	IndexIR(int key):ExprIR(getIntType()), key_(key) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class VertexId : public ExprIR {
private:
	int id_;

public:
	VertexId(int id):ExprIR(getVidType()), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class VertexAttr : public ExprIR {
private:
	int id_;

public:
	VertexAttr(int id, Type *t):ExprIR(t), id_(id) {}

	int getId() const { return id_; }

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class VecValue : public ExprIR {
private:
	int id_;

public:
	VecValue(int id, Type *t):ExprIR(t), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class Constructor : public ExprIR {
private:
	std::vector<ExprIR*> list_;

public:
	Constructor():ExprIR(new TupleType()) {}

	void add(ExprIR *expr);
	int size() const;

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class SelectIR : public ExprIR {
private:
	ExprIR *expr_;
	int idx_;

public:
	SelectIR(ExprIR *expr, int idx, Type *t):
			ExprIR(t), expr_(expr), idx_(idx) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

class BuiltinIR : public ExprIR {
private:
	std::string str_;

public:
	BuiltinIR(std::string str, Type *t):
			ExprIR(t), str_(str) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

// abstract class
class Collection : public Printable {
public:
	Collection() = default;
	virtual ~Collection() {}

	virtual int getId() const = 0;
	virtual void setLoopVar(CodeGen *cg,
			const std::string &var) const = 0;
	virtual void inc() = 0;
	virtual void clear(CodeGen *cg) = 0;
	virtual bool uniqueKey() const = 0;
};

// abstract class
class ArrayIR : public Collection {
public:
	ArrayIR() = default;
	virtual ~ArrayIR() {}

	bool uniqueKey() const override;
};

class VertexArray : public ArrayIR {
private:
	int id_;
	int cr_; // when to clear the array

public:
	VertexArray(int id, int cr = 1000):id_(id),cr_(cr) {}

	int getId() const override;
	void print(FILE *f) const override;
	void setLoopVar(CodeGen *cg, const std::string &var) const override;
	void inc() override;
	void clear(CodeGen *cg) override;
};

class ZippedArray : public ArrayIR {
private:
	ArrayIR *v1_, *v2_;

public:
	ZippedArray(ArrayIR *v1, ArrayIR *v2):
			v1_(v1), v2_(v2) {}

	// return arbitrary!!
	int getId() const override;
	void print(FILE *f) const override;
	void setLoopVar(CodeGen *cg, const std::string &var) const override;
	void inc() override;
	void clear(CodeGen *cg) override;
};

class VectorIR : public Collection {
public:
	VectorIR() = default;
	virtual ~VectorIR() {}
	bool uniqueKey() const override;
};

// key-value pair
class Vector : public VectorIR {
private:
	int id_;
	int cr_;

public:
	Vector(int id, int cr = 1000):id_(id),cr_(cr) {}

	int getId() const override;
	void print(FILE *f) const override;
	void setLoopVar(CodeGen *cg, const std::string &var) const override;
	void inc() override;
	void clear(CodeGen *cg) override;
};

class ZippedVector : public VectorIR {
private:
	VectorIR *v1_, *v2_;

public:
	ZippedVector(VectorIR *v1, VectorIR *v2):
			v1_(v1), v2_(v2) {}

	// return arbitrary!!
	int getId() const override;
	void print(FILE *f) const override;
	void setLoopVar(CodeGen *cg, const std::string &var) const override;
	void inc() override;
	void clear(CodeGen *cg) override;
};

class ArrayCopy : public StmtIR {
private:
	int dst_, src_; // unique ids of the arrays

public:
	ArrayCopy(int dst, int src):dst_(dst), src_(src) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class ArrayCompare : public StmtIR {
private:
	int lhs_, rhs_; // unique ids for arrays
	int ch_; // channel id for the aggregator

public:
	ArrayCompare(int ch, int lhs, int rhs):
			ch_(ch), lhs_(lhs), rhs_(rhs) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class ClearArray : public StmtIR {
private:
	int id_; // unique id of the array

public:
	ClearArray(int id):id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class InitAgg : public StmtIR {
private:
	int ch_;
	ExprIR *expr_;

public:
	InitAgg(int ch, ExprIR *expr = nullptr):ch_(ch), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class SetAgg : public StmtIR {
private:
	int ch_;
	ExprIR *expr_;

public:
	SetAgg(int ch, ExprIR *expr):ch_(ch), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class AllReduce : public StmtIR {
private:
	int ch_;

public:
	AllReduce(int ch):ch_(ch) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class AssignIR : public StmtIR {
private:
	VarIR *lval_;
	ExprIR *expr_;

public:
	AssignIR(VarIR *v, ExprIR *e):lval_(v), expr_(e) {};

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class CondIR : public StmtIR {
private:
	ExprIR *cond_;
	StmtIR *body_;

public:
	CondIR(ExprIR *cond, StmtIR *body):cond_(cond), body_(body) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class SendMessage : public StmtIR {
private:
	int ch_;
	ExprIR *dest_, *expr_;

public:
	SendMessage(int ch, ExprIR *dest, ExprIR *expr):
			ch_(ch), dest_(dest), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class AddEdge : public StmtIR {
private:
	int ch_, id_;
	ExprIR *expr_;

public:
	AddEdge(int ch, int id, ExprIR *expr):ch_(ch), id_(id), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class RequestIR : public StmtIR {
private:
	int ch_, id_;

public:
	RequestIR(int ch, int id):
			ch_(ch), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class AddRequestIR : public StmtIR {
private:
	int ch_;
	ExprIR *dest_;

public:
	AddRequestIR(int ch, ExprIR *dest):
			ch_(ch), dest_(dest) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class RespondIR : public StmtIR {
private:
	int ch_;
	int id_; // vertex array

public:
	RespondIR(int ch, int id):ch_(ch), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

// only used in the scatter-combine channel
class SetMessage : public StmtIR {
private:
	int ch_;
	int id_; // vertex array

public:
	SetMessage(int ch, int id):ch_(ch), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class GetRespValue : public ExprIR {
private:
	int ch_;

public:
	GetRespValue(int ch, Type *t):ExprIR(t), ch_(ch) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg, Text *text) override;
};

/* old version..
class RespondIR : public StmtIR {
private:
	int ch_;
	ExprIR *expr_;

public:
	RespondIR(int ch, ExprIR *expr):
			ch_(ch), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};
*/

class Activate : public StmtIR {
public:
	int ch_;

public:
	Activate(int ch):ch_(ch) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class OmpForReduction : public StmtIR {
private:
	int ch_; // aggregator
	StmtIR *p_; // for ..

public:
	OmpForReduction(int ch, StmtIR *p):ch_(ch), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

// loop over the local vertices
class ScanArray : public StmtIR {
private:
	ArrayIR *c_;
	StmtIR *p_;

public:
	ScanArray(ArrayIR *c, StmtIR *p):c_(c), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class MapArray : public StmtIR {
private:
	ArrayIR *c_;
	int id_; // id of the generated array
	ExprIR *expr_; // a function

public:
	MapArray(ArrayIR *c, int id, ExprIR *expr):
			c_(c), id_(id), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class MapVector : public StmtIR {
private:
	VectorIR *c_;
	int id_; // id of the generated array
	ExprIR *expr_; // a function

public:
	MapVector(VectorIR *c, int id, ExprIR *expr):
			c_(c), id_(id), expr_(expr) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

// scan an array of key-value pairs
class ScanVector : public StmtIR {
private:
	VectorIR *c_;
	StmtIR *p_;

public:
	ScanVector(VectorIR *c, StmtIR *p):c_(c), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class ArrayFromChannel : public StmtIR {
private:
	int ch_; // the channel id
	int id_; // the unique id for the array

	// depending on the channel type, this class may have different
	// implementations (e.g., through a loop or channel's interface)
public:
	ArrayFromChannel(int ch, int id):ch_(ch), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class VectorFromChannel : public StmtIR {
private:
	int ch_; // the channel id
	int id_; // the unique id for the array

	// depending on the channel type, this class may have different
	// implementations (e.g., through a loop or channel's interface)
public:
	VectorFromChannel(int ch, int id):ch_(ch), id_(id) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class ClearChannel : public StmtIR {
private:
	int ch_;

public:
	ClearChannel(int ch):ch_(ch) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class Join : public StmtIR {
private:
	VectorIR *l_, *r_;
	StmtIR *p_;

public:
	Join(VectorIR *l, VectorIR *r, StmtIR *p):
			l_(l), r_(r), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

// join vertex and edge table
class Join2 : public StmtIR {
private:
	ArrayIR *arr_;
	VectorIR *vec_;
	StmtIR *p_;

public:
	Join2(ArrayIR *arr, VectorIR *vec, StmtIR *p):
			arr_(arr), vec_(vec), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class JoinVectors : public StmtIR {
private:
	Vector *v1_, *v2_;
	StmtIR *p_;

public:
	JoinVectors(Vector *v1, Vector *v2, StmtIR *p):
			v1_(v1), v2_(v2), p_(p) {}

	void print(FILE *f) const override;
	void genChannelBased(CodeGen *cg) override;
};

class Superstep {
private:
	std::vector<StmtIR*> vec_;

public:
	Superstep() {}

	void add(StmtIR *ir);
	void append(Superstep *s);
	void dump(FILE *f) const;
	void genChannelBased(CodeGen *cg);
};

class SeqSTM;
class STM : public Printable {
protected:
	int step_;
public:
	STM() : step_(0) {}
	virtual ~STM() {}

	virtual int step() const final { return step_; }
	virtual bool isReading() const = 0;
	virtual void append(SeqSTM *stm) = 0;
	virtual void add(STM *stm) = 0;
	virtual void dump(FILE *f, int step) const = 0;
	virtual void setStep(CodeGen *cg, int step) = 0;
	virtual void genChannelBased(CodeGen *cg) = 0; // code generation

	void print(FILE *f) const override;
};

class SeqSTM : public STM {
protected:
	bool reading_;
	Superstep *head_;
	STM *next_, *jump_;

public:
	SeqSTM(bool reading, Superstep *head, STM *next, STM *jump = nullptr) :
			reading_(reading), head_(head), next_(next), jump_(jump) {}

	Superstep* head();
	Superstep* dupStep();
	STM* next();
	STM* jump();
	void setJump(STM *jump);

	bool isReading() const override;
	void append(SeqSTM *stm) override;
	void add(STM *stm) override;
	void dump(FILE *f, int step) const override;
	void setStep(CodeGen *cg, int step) override;
	void genChannelBased(CodeGen *cg) override;
};

class CondSTM : public STM {
protected:
	ExprIR *cond_;
	SeqSTM *loop_;
	SeqSTM *exit_;

public:
	CondSTM(ExprIR *cond, SeqSTM *loop, SeqSTM *exit):
			cond_(cond), loop_(loop), exit_(exit) {}

	bool isReading() const override {
		return false;
	}
	void append(SeqSTM *stm) override;
	void add(STM *stm) override;
	void dump(FILE *f, int step) const override;
	void setStep(CodeGen *cg, int step) override;
	void genChannelBased(CodeGen *cg) override;
};

class IREnv {
private:
	ChannelManager *cm_;
	MemoryManager *mm_;
	// VariableManager *vm_;

	std::vector<Superstep*> steps_, inits_;

public:
	IREnv();

	ChannelManager *getCM() const;
	MemoryManager *getMM() const;

	void reset(int numStep);
	void addInit(int lv, StmtIR *ir);
	void add(int step, StmtIR *ir);
	void enterLoop();
	SeqSTM *exitLoop();
	SeqSTM *produceSTM(bool debug = false);
};

StmtIR *Conj2IR(const std::vector<ExprIR*> &conds, StmtIR *stmt);

IREnv *getIREnv();
SeqSTM *transform(Program *prog, bool debug = true);

#endif
