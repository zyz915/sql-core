#ifndef AST_H
#define AST_H

#include <cstring>
#include <memory>
#include <string>
#include <vector>
#include <set>

#include "errorinfo.h"
#include "intermediate.h"
#include "printable.h"
#include "query.h"
#include "tokenizer.h"
#include "types.h"

class Identifier;
// from query.h
class SQLQuery;
class Context;
class Plan;

class ExprAST : public Pos, public Printable {
protected:
	Type *type_;
	int mask_ = 0;

public:
	ExprAST() = delete;
	ExprAST(Pos pos):Pos(pos), type_(nullptr) {}
	ExprAST(Pos pos, Type *t):Pos(pos), type_(t) {}
	
	virtual ~ExprAST() {}
	// test whether two expressions are equal
	virtual bool equals(const ExprAST *other) const = 0;
	// infer a type for the expression
	virtual Type *infer(TypeEnv *env) = 0;
	// return the currently inferred type of the expression
	virtual Type *type() const final;
	// calculate a mask
	virtual void calcMask(Context *context) = 0;
	// return the mask
	virtual int mask() const final { return mask_; }
	// translate to ExprIR
	virtual ExprIR* toIR(Context *context) = 0;
};

struct Operator : Pos {
	int type;
	Operator(Pos pos, int type):
			Pos(pos), type(type) {}
};

/* Identifier */
class Identifier : public ExprAST {
private:
	std::string name_;

public:
	Identifier(Pos pos, const char *str):
			ExprAST(pos), name_(str) {}
	// used in intermediate.cc
	Identifier(const std::string &str, Type *t):
			ExprAST(Pos(), t), name_(str) {}

	const std::string &getString() const;
	bool equals(const char *name) const;
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	void calcMask(Context *context) override;
	ExprIR *toIR(Context *context) override;
};

class TableName : public Printable, public Pos {
private:
	std::string name_;
public:
	TableName(Pos pos, const char *str):
			Pos(pos), name_(str) {}
	TableName(const char *str):
			Pos(), name_(str) {}

	const std::string &getString() const;
	bool equals(const char *name) const;
	bool equals(const TableName *other) const;
	// override
	void print(FILE *f) const override;
};

/* ConstAST values */
class ConstAST : public ExprAST {
public:
	ConstAST(Pos pos):ExprAST(pos) {}
	ConstAST(Pos pos, Type *t):ExprAST(pos, t) {}
	// override
	void calcMask(Context *context) override;
};

class IntExpr : public ConstAST {
private:
	int val_;
public:
	IntExpr(int val):ConstAST(Pos(), getIntType()), val_(val) {}
	IntExpr(Token tok):ConstAST(tok),
			val_(atoi(tok.val)) {}
	int getVal() const { return val_; }
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

class FloatExpr : public ConstAST {
private:
	double val_;
public:
	FloatExpr(double val):ConstAST(Pos(), getFloatType()), val_(val) {}
	FloatExpr(Token tok):ConstAST(tok),
			val_(atof(tok.val)) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

class BoolExpr : public ConstAST {
private:
	bool val_;
public:
	BoolExpr(bool val):ConstAST(Pos(), getBoolType()), val_(val)  {}
	BoolExpr(Token tok):ConstAST(tok) {
		val_ = !strcmp(tok.val, "true") || !strcmp(tok.val, "TRUE");
	}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

class InfiniteExpr : public ConstAST {
public:
	InfiniteExpr(Type *t):ConstAST(Pos(), t) {}
	InfiniteExpr(Pos pos):ConstAST(pos) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

/* Expressions */
class ExprList : public Printable {
protected:
	std::vector<ExprAST*> list_;

public:
	ExprList() {}

	void add(ExprAST *expr);
	int size() const;
	ExprAST *get(int index);
	ExprAST *operator[](int index);
	// override
	void print(FILE *f) const override;
};

class IfExpr : public ExprAST {
public:
	ExprAST *cond_, *e1_, *e2_;

public:
	IfExpr(Pos pos, ExprAST *cond, ExprAST *e1, ExprAST *e2):
			ExprAST(pos), cond_(cond), e1_(e1), e2_(e2) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	void calcMask(Context *context) override;
	ExprIR *toIR(Context *context) override;
};

class UnaryExpr : public ExprAST {
protected:
	ExprAST *expr_;

	UnaryExpr(Pos pos, ExprAST *expr):
			ExprAST(pos), expr_(expr) {}
public:
	virtual ~UnaryExpr() {}

	ExprAST *getExpr() const;
	// override
	void calcMask(Context *context) override;
};

class BinaryExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_;

public:
	BinaryExpr(Pos pos, Operator *op, ExprAST *e1, ExprAST *e2):
			ExprAST(pos), op_(op), e1_(e1), e2_(e2) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	void calcMask(Context *context) override;
	ExprIR *toIR(Context *context) override;
};

class NegativeExpr : public UnaryExpr {
public:
	NegativeExpr(Pos pos, ExprAST *expr):UnaryExpr(pos, expr) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

class NotExpr : public UnaryExpr {
public:
	NotExpr(Pos pos, ExprAST *expr):UnaryExpr(pos, expr) {}

	ExprAST *getExpr() { return expr_; }
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *infer(TypeEnv *env) override;
	ExprIR *toIR(Context *context) override;
};

class Header {
public:
	bool type; // false->input, true->output
	Token tok;
	Schema *schema;
	TableName *tn;
	//int ch; // channel id for input

	Header(bool t, Token tok, Schema *s, TableName *n):
			type(t), tok(tok), schema(s), tn(n) {} //, ch(0) {}
};

class Body : public Printable {
public:
	virtual ~Body() {}
	virtual void typeCheck(TypeEnv *env) = 0;
	virtual ControlPlan *transform(TypeEnv *env) = 0;
	virtual void modified(std::set<std::string> &s) = 0;
};

class TableExpr : public Printable, public Pos {
private:
	TableName *tn_;
	std::vector<Identifier*> vars_;
	int mask_ = -1;

public:
	TableExpr(TableName *tn):Pos(*tn), tn_(tn) {}

	void add(Identifier *name);
	TableName *getName() const;
	Identifier *getVar(int index) const;
	int size() const;
	// mask
	void setMask(int mask);
	int mask() const;
	// override
	void print(FILE *f) const override;
};

class Conjunction : public Printable {
private:
	std::vector<ExprAST*> list_;
	std::vector<bool> marked_;

public:
	Conjunction() {}

	int size() const;
	int mask() const;
	void add(ExprAST *expr);
	ExprAST *get(int index);
	int count(int mask) const;
	// remove the predicates include variables in m1 & m2; return the mask
	int exclude(int m1, int m2) const;
	int include(int mask) const;
	// evaluate the predicates that only include variables in mask;
	std::vector<ExprIR*> trans(Context *context, int mask);
	bool all_marked() const;
	Conjunction *mark(int mask); // not used
	Conjunction *filter(int mask); // not used

	void print(FILE *f) const override;
};

class QueryBlock : public Body, public Pos {
protected:
	Conjunction *preds_;
	std::vector<TableExpr*> tables_;
	Identifier *gb_; // group by
	TableName *ret_;

public:
	QueryBlock(Pos p, TableName *ret);

	void addPred(ExprAST *expr);
	void addTable(TableExpr *tab);
	void setGroupBy(Identifier *name);
	// override
	void typeCheck(TypeEnv *env) override;
	void print(FILE *f) const override;
	void modified(std::set<std::string> &s) override;
};

class VertexTableQuery : public QueryBlock {
private:
	Identifier *u_;
	Operator *op_; // aggregator (could be empty)
	ExprAST *expr_;

public:
	// no aggregator
	VertexTableQuery(Pos p, TableName *ret, Identifier *u, Operator *op, ExprAST *e):
			QueryBlock(p, ret), u_(u), op_(op), expr_(e) {}
	// aggregator (group by)
	VertexTableQuery(Pos p, TableName *ret, Identifier *u, ExprAST *e):
			QueryBlock(p, ret), u_(u), op_(nullptr), expr_(e) {}

	void typeCheck(TypeEnv *env) override;
	void print(FILE *f) const override;
	ControlPlan *transform(TypeEnv *env) override;
};

class GlobalValueQuery : public QueryBlock {
private:
	Operator *op_; // aggregator
	ExprAST *expr_;

public:
	GlobalValueQuery(Pos p, TableName *ret, Operator *op, ExprAST *e):
			QueryBlock(p, ret), op_(op), expr_(e) {}

	void typeCheck(TypeEnv *env) override;
	void print(FILE *f) const override;
	ControlPlan *transform(TypeEnv *env) override;
};

class Compose : public Body {
protected:
	Body *head_, *tail_;
public:
	Compose(Body *head, Body *tail):
			head_(head), tail_(tail) {}
	// override
	void print(FILE *f) const override;
	void typeCheck(TypeEnv *env) override;
	ControlPlan *transform(TypeEnv *env) override;
	void modified(std::set<std::string> &s) override;
};

class Iteration : public Body {
protected:
	Body *prog_;
	std::set<std::string> used_;

public:
	Iteration(Body *prog): prog_(prog) {}

	void print(FILE *f) const override = 0;
	void typeCheck(TypeEnv *env) override;
	void modified(std::set<std::string> &s) override;
};

class ForRange : public Iteration {
protected:
	Identifier *name_;
	IntExpr *lo_, *hi_; // range = [lo, hi)

public:
	ForRange(Body *prog, Identifier *name, IntExpr *lo, IntExpr *hi):
		Iteration(prog), name_(name), lo_(lo), hi_(hi) {}
	// override
	void print(FILE *f) const override;
	void typeCheck(TypeEnv *env) override;
	ControlPlan *transform(TypeEnv *env) override;
};

// actually the fixed-point computation
class DoUntil : public Iteration {
protected:
	TableName *tn_;

public:
	DoUntil(Body *prog, TableName *tn):
			Iteration(prog), tn_(tn) {}
	// override
	void print(FILE *f) const override;
	ControlPlan *transform(TypeEnv *env) override;
};

class Program : public Printable {
public:
	std::vector<Header*> headers;
	Body *body;

	Program() {}

public:
	ControlPlan *transform(TypeEnv *env); 
	// override
	void print(FILE *f) const;
};

Operator *getOper(int tok);

#endif
