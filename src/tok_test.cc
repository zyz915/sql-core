#include "tokenizer.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

extern Token CurTok;

static char buff[256];
static int shift;
static int debug;

static void putToken(char *buff, Token tok) {
	if (debug) {
		fprintf(stderr, "%2d (%d.%d) %s\n", tok.type,
				tok.row, tok.col, getTokenStr(tok));
		return;
	}
	char *dst = buff + tok.col - 1;
	const char *src = getTokenStr(tok);
	memcpy(dst, src, strlen(src));
}

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	initTokenizer();
	debug = (argc > 2 && !strcmp(argv[2], "-d"));
	int currline = 1;
	memset(buff, ' ', sizeof(buff));
	getNextToken();
	while (true) {
		while (CurTok.row != currline) {
			shift = 0;
			int end = 255;
			while (end > 1 && buff[end - 1] == ' ')
				end--;
			buff[end] = 0;
			if (!debug)
				printf("%s\n", buff);
			memset(buff, ' ', sizeof(buff));
			currline += 1;
		}
		putToken(buff, CurTok);
		if (CurTok.type == tok_eof)
			break;
		getNextToken();
	}
	return 0;
}
