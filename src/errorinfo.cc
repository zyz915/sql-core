#include <cstdio>
#include <cstdlib>
#include <map>
#include <string>

#include "errorinfo.h"

using namespace std;

static map<int, string> prog;
void setline(int line, const char *buff) {
	prog.insert(make_pair(line, string(buff)));
}

void locate(Pos pos) {
	auto v = prog.find(pos.row);
	if (v == prog.end()) return;
	const char *s = v->second.c_str();
	int l = v->second.length();
	if (pos.col < 1 || pos.col > l) return;
	for (int i = 0; s[i] && s[i] != '\n'; i++)
		fputc(s[i], stderr);
	fputc('\n', stderr);
	for (int i = 0; i < pos.col - 1; i++)
		fputc(s[i] == '\t' ? '\t' : ' ', stderr);
	fprintf(stderr, "^\n");
	fflush(stderr);
}

void prtError(const char *str) {
	fprintf(stderr, "error: %s\n", str);
	exit(0);
}

void prtError(Pos pos, const char *str) {
	fprintf(stderr, "line %d.%d error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(0);
}

void prtParseError(Pos pos, const char *str) {
	fprintf(stderr, "line %d.%d parse error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(0);
}

void prtTypeError(Pos pos, const char *str) {
	fprintf(stderr, "line %d.%d type error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(0);
}

void prtErrorRV(Pos pos, const char *name) {
	fprintf(stderr, "line %d.%d error: variable '%s' redefined\n", pos.row, pos.col, name);
	locate(pos);
	exit(0);
}

void prtErrorUV(Pos pos, const char *name) {
	fprintf(stderr, "line %d.%d error: variable '%s' undefined\n", pos.row, pos.col, name);
	locate(pos);
	exit(0);
}

void prtErrorIM(Pos pos, const char *name) {
	fprintf(stderr, "line %d.%d error: variable '%s' is immutable\n", pos.row, pos.col, name);
	fprintf(stderr, "> use 'let mut %s = ..' to enable variable mutation\n", name);
	locate(pos);
	exit(0);
}

void prtWarning(const char *str) {
	fprintf(stderr, "warning: %s\n", str);
}

void prtWarning(Pos pos, const char *str) {
	fprintf(stderr, "line %d.%d warning: %s\n", pos.row, pos.col, str);
	locate(pos);
}

void fail(bool val, const char *str) {
	if (val) prtError(str);
}

void fail(const char *str) {
	fprintf(stderr, "should not reach: %s\n", str);
	exit(0);
}
