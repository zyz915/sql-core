#ifndef ERRORINFO_H
#define ERRORINFO_H

struct Pos {
	int row;
	int col;

	Pos():row(0),col(0) {}
	Pos(const Pos &pos):row(pos.row),col(pos.col) {}
	Pos(int row, int col):row(row),col(col) {}
};

void prtError(const char *msg);
void prtError(Pos pos, const char *msg);
void prtParseError(Pos pos, const char *msg);
void prtTypeError(Pos pos, const char *msg);
void prtErrorRV(Pos pos, const char *name); // redefined variable
void prtErrorUV(Pos pos, const char *name); // undefined variable

void prtWarning(const char *msg);
void prtWarning(Pos pos, const char *msg);

// report error when the predicate holds
void fail(bool predicate, const char *msg);
void fail(const char *msg);
void fail(Pos pos, const char *msg);

void setline(int line, const char *buff);
void locate(Pos pos);

#endif
