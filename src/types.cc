#include <cstdio>
#include <typeindex>

#include "ast.h"
#include "codegen.h"
#include "errorinfo.h"
#include "query.h"
#include "types.h"

using namespace std;

/**
 * initialize static in-class members;
 * access to the unique class instance
 */
IntType *IntType::itype = new IntType();
VidType *VidType::vtype = new VidType();
BoolType *BoolType::btype = new BoolType();
FloatType *FloatType::ftype = new FloatType();
UnitType *UnitType::utype = new UnitType();

Type *getIntType() {
	return IntType::itype;
}

Type *getVidType() {
	return VidType::vtype;
}

BoolType *getBoolType() {
	return BoolType::btype;
}

FloatType *getFloatType() {
	return FloatType::ftype;
}

UnitType *getUnitType() {
	return UnitType::utype;
}

void VidType::print(FILE *f) const {
	fprintf(f, "vid");
}

void IntType::print(FILE *f) const {
	fprintf(f, "int");
}

void BoolType::print(FILE *f) const {
	fprintf(f, "bool");
}

void FloatType::print(FILE *f) const {
	fprintf(f, "float");
}

void UnitType::print(FILE *f) const {
	fprintf(f, "unit");
}

void PairType::print(FILE *f) const {
	fprintf(f, "(PairType ");
	l_->print(f);
	fprintf(f, " ");
	r_->print(f);
	fprintf(f, ")");
}

void TupleType::print(FILE *f) const {
	fprintf(f, "(Compound");
	for (Type *t : list_) {
		fprintf(f, " ");
		t->print(f);
	}
	fprintf(f, ")");
}

bool Type::equals(Type *t) const {
	return typeid(*this) == typeid(*t);
}

bool PairType::equals(Type *t) const {
	if (typeid(*this) != typeid(*t))
		return false;
	PairType *pt = static_cast<PairType*>(t);
	return l_->equals(pt->l_) && r_->equals(pt->r_);
}

bool TupleType::equals(Type *t) const {
	if (typeid(*this) != typeid(*t))
		return false;
	TupleType *ct = static_cast<TupleType*>(t);
	if (list_.size() != ct->list_.size())
		return false;
	for (int i = 0; i < list_.size(); i++)
		if (!list_[i]->equals(ct->list_[i]))
			return false;
	return true;
}

int VidType::bytes() const {
	//prtError("VidType::bytes()");
	return 4;
}

int IntType::bytes() const {
	return 4;
}

int BoolType::bytes() const {
	return 1;
}

int FloatType::bytes() const {
	return 8;
}

int UnitType::bytes() const {
	return 0;
}

int PairType::bytes() const {
	return l_->bytes() + r_->bytes();
}

int TupleType::bytes() const {
	int ret = 0;
	for (Type *t : list_)
		ret += t->bytes();
	return ret;
}

string VidType::toString(CodeGen *cg) const {
	return cg->key()->toString(cg);
}

string IntType::toString(CodeGen *cg) const {
	return string("int");
}

string FloatType::toString(CodeGen *cg) const {
	return string("double");
}

string UnitType::toString(CodeGen *cg) const {
	return string("Empty");
}

string BoolType::toString(CodeGen *cg) const {
	return string("bool");
}

string PairType::toString(CodeGen *cg) const {
	return string("pair<") +
		l_->toString(cg) + ", " +
		r_->toString(cg) + "> ";
}

string TupleType::toString(CodeGen *cg) const {
	switch (list_.size()) {
		case 0: return getUnitType()->toString(cg);
		case 1: return list_[0]->toString(cg);
		case 2: return string("pair<") +
				list_[0]->toString(cg) + ", " +
				list_[1]->toString(cg) + "> ";
		default: {
			string s = "Tuple";
			for (Type *t : list_) {
				s += "_";
				s += t->toString(cg);
			}
			return s;
		}
	}
}

string VidType::toMPIType(CodeGen *cg) const {
	return cg->key()->toMPIType(cg);
}

string IntType::toMPIType(CodeGen *cg) const {
	return string("MPI_INT");
}

string FloatType::toMPIType(CodeGen *cg) const {
	return string("MPI_DOUBLE");
}

string UnitType::toMPIType(CodeGen *cg) const {
	return string("");
}

string BoolType::toMPIType(CodeGen *cg) const {
	return string("MPI_CHAR");
}

string PairType::toMPIType(CodeGen *cg) const {
	return string("");
}

string TupleType::toMPIType(CodeGen *cg) const {
	return string("");
}

string PairType::access(int idx) const {
	return (idx == 0 ? ".first" : ".second");
}

string TupleType::access(int idx) const {
	char buf[10];
	fail(idx >= list_.size(),
			"TupleType::access() -- index out of range");
	switch (list_.size()) {
		case 0: return "";
		case 1: return "";
		case 2: return (idx == 0 ? ".first" : ".second");
		default: {
			sprintf(buf, ".i%d", idx);
			return string(buf);
		}
	}
}

void TupleType::add(Type *t) {
	list_.push_back(t);
}

int TupleType::size() const {
	return list_.size();
}

bool Schema::equals(Schema *other) const {
	return (typeid(other) == typeid(*this) && type_ == other->type_);
}

void Schema::dump(FILE *f) const {
	fprintf(f, "(Schema ");
	type_->print(f);
	fprintf(f, ")\n");
}

int LoopInfo::push(const set<string> &s) {
	mod_.push_back(s);
	return mod_.size() - 1;
}

void LoopInfo::pop() {
	mod_.pop_back();
}

bool LoopInfo::insideLoop() {
	return !mod_.empty();
}

void LoopInfo::calcMask(Context *context) {
	mask_.resize(mod_.size());
	auto rels = context->rels();
	for (int lv = 0; lv < mod_.size(); lv++) {
		mask_[lv] = 0;
		for (int i = 0; i < rels.size(); i++) {
			string s = rels[i]->getName()->getString();
			if (mod_[lv].find(s) == mod_[lv].end())
				mask_[lv] = mask_[lv] | rels[i]->mask();
		}
	}
}

int LoopInfo::calcLevel(int mask) {
	for (int lv = 0; lv < mask_.size(); lv++)
		if (!(mask & ~mask_[lv]))
			return lv;
	return -1;
}

TypeEnv::TypeEnv() {
	addTable(new TableName("VertexSet"), new VertexSet());
}

void TypeEnv::Scope::add(Identifier *name, Type *t) {
	types_.insert(make_pair(name->getString(), t));
}

Type *TypeEnv::Scope::get(Identifier *name) const {
	auto v = types_.find(name->getString());
	return (v == types_.end() ? nullptr : v->second);
}

bool TypeEnv::Scope::exists(Identifier *name) const {
	return types_.find(name->getString()) != types_.end();
}

void TypeEnv::addTable(TableName *tn, Schema *s) {
	tables_.insert(make_pair(tn->getString(), s));
}

bool TypeEnv::tableExists(TableName *tn) const {
	return tables_.find(tn->getString()) != tables_.end();
}

Schema *TypeEnv::getSchema(TableName *tn) const {
	return tables_.find(tn->getString())->second;
}

LoopInfo *TypeEnv::getLoopInfo() {
	if (loopInfo_ == nullptr)
		loopInfo_ = new LoopInfo();
	return loopInfo_;
}

Type *TypeEnv::checkArithmicOp(Type *t1, Type *t2, Pos pos) const {
	if (t1->equals(t2))
		return t1;
	else {
		if (t1->equals(getFloatType()) &&
			t2->equals(getIntType()))
			return getFloatType();
		if (t2->equals(getFloatType()) &&
			t1->equals(getIntType()))
			return getFloatType();
		fprintf(stderr, " left operant : ");
		debug(t1);
		fprintf(stderr, "right operant : ");
		debug(t2);
		prtTypeError(pos, "unmatched types");
	}
	return nullptr;
}

void TypeEnv::addConstraint(Type *t1, Type *t2, Pos pos) const {
	if (!realType(t1)->equals(realType(t2))) {
		fprintf(stderr, "expected type : ");
		debug(t1);
		fprintf(stderr, "  actual type : ");
		debug(t2);
		prtTypeError(pos, "unmatched types");
	}
}

bool TypeEnv::exists(Identifier *name) const {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++)
		if ((*it)->exists(name))
			return true;
	return false;
}

void TypeEnv::addBinding(Identifier *name, Type *t) {
	Type *exist = stack_.back()->get(name);
	if (exist != nullptr && !exist->equals(t))
		prtTypeError(*name, "unmatched types");
	stack_.back()->add(name, t);
}

Type *TypeEnv::realType(Type *t) const {
	if (t != nullptr && t->equals(VidType::vtype))
		return IntType::itype;
	return t;
}

Type *TypeEnv::getType(Identifier *name) const {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++) {
		Type *t = (*it)->get(name);
		if (t != nullptr)
			return t;
	}
	return nullptr;
}

void TypeEnv::pushScope() {
	stack_.push_back(new Scope());
}

void TypeEnv::popScope() {
	stack_.pop_back();
}

static TypeEnv *env = new TypeEnv();
TypeEnv *getTypeEnv() {
	return env;
}

void typeCheck(Program *prog, bool debug) {
	prog->body->typeCheck(getTypeEnv());
}

void debug(const Type *t, FILE *f) {
	t->print(f);
	fprintf(f, "\n");
	fflush(f);
}
