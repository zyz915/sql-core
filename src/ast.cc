#include "ast.h"
#include "config.h"
#include "errorinfo.h"
#include "query.h"
#include "types.h"

using namespace std;

static char buf[128];

static inline bool is_subset(int a, int b) {
	return !(a & ~b);
}

Type *ExprAST::type() const {
	return type_;
}

bool Identifier::equals(const char *str) const {
	return !strcmp(name_.c_str(), str);
}

const string &Identifier::getString() const {
	return name_;
}

const string &TableName::getString() const {
	return name_;
}

bool TableName::equals(const char *str) const {
	return !strcmp(name_.c_str(), str);
}

bool TableName::equals(const TableName *other) const {
	return name_ == other->name_;
}

void ExprList::add(ExprAST *expr) {
	list_.push_back(expr);
}

int ExprList::size() const {
	return list_.size();
}

ExprAST *ExprList::get(int index) {
	return list_[index];
}

ExprAST *ExprList::operator[](int index) {
	return list_[index];
}

ExprAST *UnaryExpr::getExpr() const {
	return expr_;
}

void TableExpr::add(Identifier *name) {
	vars_.push_back(name);
}

void TableExpr::setMask(int mask) {
	mask_ = mask;
}

int TableExpr::mask() const {
	return mask_;
}

TableName *TableExpr::getName() const {
	return tn_;
}

Identifier *TableExpr::getVar(int index) const {
	return vars_[index];
}

int TableExpr::size() const {
	return vars_.size();
}

int Conjunction::size() const {
	return list_.size();
}

int Conjunction::mask() const {
	int mask = 0;
	for (int i = 0; i < list_.size(); i++)
		if (!marked_[i])
			mask = mask | list_[i]->mask();
	return mask;
}

void Conjunction::add(ExprAST *expr) {
	list_.push_back(expr);
	marked_.push_back(false);
}

ExprAST *Conjunction::get(int index) {
	return list_[index];
}

int Conjunction::count(int mask) const {
	int ret = 0;
	for (ExprAST *e : list_)
		ret += is_subset(e->mask(), mask);
	return ret;
}

int Conjunction::exclude(int m1, int m2) const {
	int ret = 0;
	for (int i = 0; i < list_.size(); i++)
		if (!marked_[i] && (!is_subset(list_[i]->mask(), m1) &&
				!is_subset(list_[i]->mask(), m2)))
			ret = ret | list_[i]->mask();
	return ret;
}

int Conjunction::include(int mask) const {
	int ret = 0;
	for (int i = 0; i < list_.size(); i++)
		if (!marked_[i] && is_subset(list_[i]->mask(), mask))
			ret = ret | list_[i]->mask();
	return ret;
}

vector<ExprIR*> Conjunction::trans(Context *context, int mask) {
	vector<ExprIR*> ret;
	for (int i = 0; i < list_.size(); i++)
		if (!marked_[i] && is_subset(list_[i]->mask(), mask)) {
			ret.push_back(list_[i]->toIR(context));
			marked_[i] = true; // problematic?
		}
	return ret;
}

bool Conjunction::all_marked() const {
	for (int i = 0; i < list_.size(); i++)
		if (!marked_[i]) return false;
	return true;
}

Conjunction *Conjunction::mark(int mask) {
	for (int i = 0; i < list_.size(); i++)
		if (is_subset(list_[i]->mask(), mask))
			marked_[i] = true;
	return this;
}

Conjunction *Conjunction::filter(int mask) {
	Conjunction *ret = new Conjunction();
	for (ExprAST *e : list_)
		if (is_subset(e->mask(), mask))
			ret->add(e);
	return ret;
}

void QueryBlock::addPred(ExprAST *expr) {
	preds_->add(expr);
}

void QueryBlock::addTable(TableExpr *tab) {
	tables_.push_back(tab);
}

void QueryBlock::setGroupBy(Identifier *name) {
	gb_ = name;
}

QueryBlock::QueryBlock(Pos p, TableName *ret):
		Pos(p), ret_(ret), gb_(nullptr) {
	preds_ = new Conjunction();
}

/* implement the interface in class AST:
 * 	 void print(FILE *f);
 */
void Identifier::print(FILE *f) const {
	fprintf(f, "(Name %s)", name_.c_str());
}

void TableName::print(FILE *f) const {
	fprintf(f, "(TableName %s)", name_.c_str());
}

void IntExpr::print(FILE *f) const {
	fprintf(f, "(Int %d)", val_);
}

void FloatExpr::print(FILE *f) const {
	fprintf(f, "(Float %.2f)", val_);
}

void BoolExpr::print(FILE *f) const {
	fprintf(f, "(Bool %s)", (val_ ? "true" : "false"));
}

void InfiniteExpr::print(FILE *f) const {
	fprintf(f, "\"inf\"");
}

void ExprList::print(FILE *f) const {
	fprintf(f, "(ExprList");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void Conjunction::print(FILE *f) const {
	fprintf(f, "(Conjunction");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void IfExpr::print(FILE *f) const {
	fprintf(f, "(IfExpr ");
	cond_->print(f);
	fprintf(f, " ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void BinaryExpr::print(FILE *f) const {
	fprintf(f, "(BinaryExpr \"%s\" ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void NegativeExpr::print(FILE *f) const {
	fprintf(f, "(NegativeExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void NotExpr::print(FILE *f) const {
	fprintf(f, "(NotExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void TableExpr::print(FILE *f) const {
	fprintf(f, "(TableExpr ");
	tn_->print(f);
	for (const auto var : vars_) {
		fprintf(f, " ");
		var->print(f);
	}
	fprintf(f, " %d)", mask_);
}

void QueryBlock::print(FILE *f) const {
	for (auto tab : tables_) {
		fprintf(f, " ");
		tab->print(f);
	}
	fprintf(f, " ");
	preds_->print(f);
	if (gb_ != nullptr) {
		fprintf(f, " \"GROUP BY\" ");
		gb_->print(f);
	}
}

void VertexTableQuery::print(FILE *f) const {
	fprintf(f, "(VertexTableQuery");
	QueryBlock::print(f);
	fprintf(f, ")");
}

void GlobalValueQuery::print(FILE *f) const {
	fprintf(f, "(GlobalValueQuery");
	QueryBlock::print(f);
	fprintf(f, ")");
}

void Compose::print(FILE *f) const {
	fprintf(f, "(Compose ");
	head_->print(f);
	fprintf(f, " ");
	tail_->print(f);
	fprintf(f, ")");
}

void ForRange::print(FILE *f) const {
	fprintf(f, "(ForRange ");
	name_->print(f);
	fprintf(f, " %d %d ", lo_->getVal(), hi_->getVal());
	prog_->print(f);
	fprintf(f, ")");
}

void DoUntil::print(FILE *f) const {
	fprintf(f, "(DoUntil ");
	prog_->print(f);
	fprintf(f, " ");
	tn_->print(f);
	fprintf(f, ")");
}

void Program::print(FILE *f) const {
	fprintf(f, "(Program ");
	body->print(f);
	fprintf(f, ")");
}

/* implement the interface in class ExprAST:
 *   bool equals(const ExprAST *other) const;
 */
bool Identifier::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const Identifier*>(other);
	return name_ == it->name_;
}

bool IntExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const IntExpr*>(other);
	return val_ == it->val_;
}

bool FloatExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const FloatExpr*>(other);
	return val_ == it->val_;
}

bool BoolExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const BoolExpr*>(other);
	return val_ == it->val_;
}

bool InfiniteExpr::equals(const ExprAST *other) const {
	return (typeid(*this) != typeid(*other));
}

bool IfExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const IfExpr*>(other);
	return (cond_->equals(it->cond_) &&
			e1_->equals(it->e1_) &&
			e2_->equals(it->e2_));
}

bool BinaryExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const BinaryExpr*>(other);
	return (op_->type == it->op_->type &&
			e1_->equals(it->e1_) &&
			e2_->equals(it->e2_));
}

bool NegativeExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const NegativeExpr*>(other);
	return (expr_->equals(it->expr_));
}

bool NotExpr::equals(const ExprAST *other) const {
	if (typeid(*this) != typeid(*other)) return false;
	auto *it = static_cast<const NotExpr*>(other);
	return (expr_->equals(it->expr_));
}

/* implement the interface in class ExprAST:
 *   Type *infer(TypeEnv *env);
 */
Type *Identifier::infer(TypeEnv *env) {
	if (!env->exists(this))
		prtErrorUV(*this, this->name_.c_str());
	type_ = env->getType(this);
	return type_;
}

Type *IntExpr::infer(TypeEnv *env) {
	return (type_ = getIntType());
}

Type *FloatExpr::infer(TypeEnv *env) {
	return (type_ = getFloatType());
}

Type *BoolExpr::infer(TypeEnv *env) {
	return (type_ = getBoolType());
}

Type *InfiniteExpr::infer(TypeEnv *env) {
	return type_ = getIntType();
}

Type *IfExpr::infer(TypeEnv *env) {
	Type *ct = cond_->infer(env);
	env->addConstraint(getBoolType(), ct, *this);
	Type *t1 = e1_->infer(env);
	Type *t2 = e2_->infer(env);
	env->addConstraint(t1, t2, *e2_);
	return (type_ = t1);
}

Type *BinaryExpr::infer(TypeEnv *env) {
	Type *t1 = e1_->infer(env);
	Type *t2 = e2_->infer(env);
	int ty = op_->type;
	if (ty >= tok_add && ty <= tok_div)
		return (type_ = env->checkArithmicOp(t1, t2, *this));
	if (ty == tok_mod) {
		env->addConstraint(getIntType(), env->realType(t1), *e1_);
		env->addConstraint(getIntType(), env->realType(t2), *e2_);
		return (type_ = getIntType());
	}
	env->addConstraint(env->realType(t1), env->realType(t2), *this);
	if (ty >= tok_eq && ty <= tok_ge)
		return (type_ = getBoolType());
	if (ty == tok_and || ty == tok_or)
		return (type_ = getBoolType());
	if (ty == tok_xor)
		return (type_ = getIntType());
	prtError(*op_, "invalid binary operator");
	return (type_ = getIntType());
}

Type *NegativeExpr::infer(TypeEnv *env) {
	return type_ = expr_->infer(env);
}

Type *NotExpr::infer(TypeEnv *env) {
	env->addConstraint(getBoolType(),
			expr_->infer(env), *this);
	return (type_ = getBoolType());
}

/* implement the interface in class ExprAST:
 *   void calcMask(Context *context);
 */
void Identifier::calcMask(Context *context) {
	int idx = context->lookup(name_);
	if (idx != -1)
		mask_ = 1 << idx;
}

void ConstAST::calcMask(Context *context) {
	mask_ = 0;
}

void IfExpr::calcMask(Context *context) {
	e1_->calcMask(context);
	e2_->calcMask(context);
	cond_->calcMask(context);
	mask_ = e1_->mask() | e2_->mask() | cond_->mask();
}

void UnaryExpr::calcMask(Context *context) {
	expr_->calcMask(context);
	mask_ = expr_->mask();
}

void BinaryExpr::calcMask(Context *context) {
	e1_->calcMask(context);
	e2_->calcMask(context);
	mask_ = e1_->mask() | e2_->mask();
}

void QueryBlock::typeCheck(TypeEnv *env) {
	Schema *r = env->getSchema(ret_);
	for (auto tab : tables_) {
		TableName *tn = tab->getName();
		Schema *s = env->getSchema(tn);
		int cols = s->numCols();
		for (int i = 0; i < cols; i++) {
			Identifier *name = tab->getVar(i);
			Type *st = s->getType(i);
			env->addBinding(name, st);
			name->infer(env);
		}
	}
	for (int i = 0; i < preds_->size(); i++) {
		Type *t = preds_->get(i)->infer(env);
		env->addConstraint(getBoolType(), t, *preds_->get(i));
	}
}

void VertexTableQuery::typeCheck(TypeEnv *env) {
	env->pushScope();
	QueryBlock::typeCheck(env);
	Schema *s = env->getSchema(ret_);
	env->addConstraint(s->getType(0),
			u_->infer(env), *u_);
	env->addConstraint(s->getType(1),
			expr_->infer(env), *expr_);
	env->popScope();
}

void GlobalValueQuery::typeCheck(TypeEnv *env) {
	env->pushScope();
	QueryBlock::typeCheck(env);
	Schema *s = env->getSchema(ret_);
	env->addConstraint(s->getType(0), expr_->infer(env), *expr_);
	env->popScope();
}

void Compose::typeCheck(TypeEnv *env) {
	head_->typeCheck(env);
	tail_->typeCheck(env);
}

void Iteration::typeCheck(TypeEnv *env) {
	env->pushScope();
	prog_->typeCheck(env);
	env->popScope();
}

void ForRange::typeCheck(TypeEnv *env) {
	env->pushScope();
	env->addBinding(name_, getIntType());
	prog_->typeCheck(env);
	env->popScope();
}

void QueryBlock::modified(set<string> &s) {
	s.insert(ret_->getString());
}

void Compose::modified(set<string> &s) {
	head_->modified(s);
	tail_->modified(s);
}

void Iteration::modified(set<string> &s) {
	prog_->modified(used_);
	s.insert(used_.begin(), used_.end());
}

/*
 * Implement the interface of ExprAST:
 *   ExprIR *toIR(Context *context) override;
 */
ExprIR *Identifier::toIR(Context *context) {
	return context->trans(name_, getCodeGen());
}

ExprIR *IntExpr::toIR(Context *context) {
	return new IntIR(val_);
}

ExprIR *FloatExpr::toIR(Context *context) {
	return new FloatIR(val_);
}

ExprIR *BoolExpr::toIR(Context *context) {
	return new BoolIR(val_);
}

ExprIR *InfiniteExpr::toIR(Context *context) {
	return new IntIR(0, FLAGS::MAX);
}

ExprIR *IfExpr::toIR(Context *context) {
	return new IfExprIR(type_, cond_->toIR(context),
			e1_->toIR(context), e2_->toIR(context));
}

ExprIR *BinaryExpr::toIR(Context *context) {
	return new BinaryIR(type_, op_, e1_->toIR(context),
			e2_->toIR(context));
}

ExprIR *NegativeExpr::toIR(Context *context) {
	return new UnaryIR(type_, new Operator(*this, tok_sub),
			expr_->toIR(context));
}

ExprIR *NotExpr::toIR(Context *context) {
	return new UnaryIR(type_, new Operator(*this, tok_not),
			expr_->toIR(context));
}

ControlPlan *VertexTableQuery::transform(TypeEnv *env) {
	Context *context = new Context(tables_, env);
	SQLQuery *query = new SQLQuery(context, preds_);
	//query->addExpr(u_);
	query->addExpr(expr_);
	query->is_agg = 0;
	query->has_group = (gb_ != nullptr);
	query->dest = context->lookup(u_->getString());
	ExecPlan *pl = solve(query, env);
	if (!context->isPrimaryKey(u_, env) && gb_ == nullptr)
		prtError(*this, "missing GROUP BY");
	if (gb_ != nullptr && !gb_->equals(u_))
		prtError(*gb_, "GROUP BY on invalid column");
	return new GroupPlan(context, u_, op_, expr_, preds_, pl, ret_);
}

ControlPlan *GlobalValueQuery::transform(TypeEnv *env) {
	Context *context = new Context(tables_, env);
	if (context->size() == 1) {
		if (op_->type == tok_cnt && preds_->size() == 0) {
			TableExpr *tab = context->rels().front();
			Schema *s = getTypeEnv()->getSchema(tab->getName());
			Type *t = expr_->type();
			if (typeid(*s) == typeid(VertexSet)) // return |V|
				return new ConstantPlan("total_numv()", t, ret_);
			if (typeid(*s) == typeid(VertexTable)) // return |V|
				return new ConstantPlan("total_numv()", t, ret_);
			if (typeid(*s) == typeid(EdgeTable)) // return |V|
				return new ConstantPlan("total_nume()", t, ret_);
		}

	}

	SQLQuery *query = new SQLQuery(context, preds_);
	query->addExpr(expr_);
	query->is_agg = 1;
	query->has_group = false;
	query->dest = -1; // not specified
	ExecPlan *pl = solve(query, env);
	return new AggregatePlan(context, op_, expr_, preds_, pl, ret_);
}

ControlPlan *Compose::transform(TypeEnv *env) {
	ControlPlan *p1 = head_->transform(env);
	ControlPlan *p2 = tail_->transform(env);
	return new ComposePlan(p1, p2);
}

ControlPlan *ForRange::transform(TypeEnv *env) {
	int lv = env->getLoopInfo()->push(used_);
	ControlPlan *p = prog_->transform(env);
	env->getLoopInfo()->pop();
	return new ForRangePlan(p, name_, lo_->getVal(),
			hi_->getVal(), lv);
}

ControlPlan *DoUntil::transform(TypeEnv *env) {
	int lv = env->getLoopInfo()->push(used_);
	ControlPlan *p = prog_->transform(env);
	env->getLoopInfo()->pop();
	Schema *s = env->getSchema(tn_);
	return new FixedPointPlan(s->type(), p, tn_, lv);
}

ControlPlan *Program::transform(TypeEnv *env) {
	set<string> s;
	body->modified(s);
	return body->transform(env);
}

Operator *getOper(int tok) {
	return new Operator(Pos(), tok);
}
