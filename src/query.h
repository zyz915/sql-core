#ifndef QUERY_H_
#define QUERY_H_

#include <cstdio>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "intermediate.h"
#include "printable.h"
#include "types.h"

// from ast.h
class ExprAST;
class Identifier;
class TableName;
class TableExpr;
class Operator;
class ExprList;
class Conjunction;
class AggExpr;
// this file
class Context;

class VertexSet : public Schema {
public:
	VertexSet();

	double numRows() const override;
	int numCols() const override;
	Type *getType(int index) const override;
};

class VertexTable : public Schema {
public:
	VertexTable(Type *t):Schema(t) {}

	double numRows() const override;
	int numCols() const override;
	Type *getType(int index) const override;
	//void dump(FILE *f = stderr) const override;
};

class EdgeTable : public Schema {
public:
	EdgeTable(Type *t):Schema(t) {}

	double numRows() const override;
	int numCols() const override;
	Type *getType(int index) const override;
	//void dump(FILE *f = stderr) const override;
};

class GlobalValue : public Schema {
public:
	GlobalValue(Type *t):Schema(t) {}

	double numRows() const override;
	int numCols() const override;
	Type *getType(int index) const override;
	//void dump(FILE *f = stderr) const override;
};

struct TableStat {
	int bytes;    // size of the columns
	double rows;  // estimated number of rows
	double real_rows; // number of rows without filtering?
	double cost = 0; // communication cost for generating the table

	TableStat():bytes(0), rows(0), cost(0) {}
	TableStat(int b, double r, double c = 0):
			bytes(b), rows(r), cost(c) {}

	double size() const {
		return rows * bytes;
	}
};

// abstract class
class Plan : public Printable {
public:
	Plan() {}
	virtual ~Plan() {}

	// necessary computations before further selectedColumnstion
	virtual void prepare(IREnv *env, int loopLv) = 0;
};

// abstract class
class ExecPlan : public Plan {
protected:
	int mask_, step_;

public:
	ExecPlan():mask_(0), step_(0) {}
	ExecPlan(int mask):mask_(mask) {}

	// which variables are contained
	virtual int mask() const final { return mask_; }
	// where the table is evaluated
	virtual int getLoc() const = 0;
	// where the table is sent to
	virtual int getDest() const = 0;
	// the primary key of the table (-1 if not exist)
	virtual int primaryKey() const = 0;
	// estimate the size of the table
	virtual TableStat stat(Conjunction *ps, int mask, Context *context) const = 0;
	// when the table is evaluated and sent to the destination
	virtual int step() const;
	// generate all the columns
	virtual Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) = 0;
	// evaluate the expression (instead of all the columns) on the sender-side
	virtual VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) = 0;
	// send the result to the aggregator
	virtual AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) = 0;
};

// abstract class
class ControlPlan : public Plan {
public:
	ControlPlan():Plan() {}

	// generate the state transition machine
	virtual SeqSTM *transform(IREnv *env) = 0;
};

class VertexTableAccess : public ExecPlan {
private:
	TableName *tn_;
	int prim_;  // ID of the primary key
	int attr_;  // ID of the vertex attribute;
	int dest_;  // which column representation
	double rows_; // size of the table

public:
	VertexTableAccess() = delete;
	VertexTableAccess(TableExpr *t, Schema *s, int dest, Context *c);

	// override
	int getLoc() const override;
	int getDest() const override;
	int primaryKey() const override;
	TableStat stat(Conjunction *ps, int mask, Context *context) const override;
	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) override;
	VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) override;
	AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) override;
};

class EdgeTableAccess : public ExecPlan {
private:
	TableName *tn_;
	int fst_;  // the first column
	int snd_;  // the second column
	int attr_; // the edge attribute (-1 if not used)
	int dest_; // which column representation
	double rows_; // size of the table

public:
	EdgeTableAccess() = delete;
	EdgeTableAccess(TableExpr *t, Schema *s, int dest, Context *c);

	int getLoc() const override;
	int getDest() const override;
	int primaryKey() const override;
	TableStat stat(Conjunction *ps, int mask, Context *context) const override;
	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	void vectorize(IREnv *env, Context *context);
	Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) override;
	VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) override;
	AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) override;
};

class JoinPlan : public ExecPlan {
private:
	ExecPlan *lhs_, *rhs_;
	int comm_, dest_;

public:
	JoinPlan(ExecPlan *a, ExecPlan *b, int comm, int dest);

	int getLoc() const override;
	int getDest() const override;
	int primaryKey() const override;
	TableStat stat(Conjunction *ps, int mask, Context *context) const override;
	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) override;
	VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) override;
	AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) override;
};

class RequestPlan : public ExecPlan {
private:
	ExecPlan *req_, *resp_;
	int comm_, dest_;

public:
	RequestPlan(ExecPlan *a, ExecPlan *b, int comm, int dest);

	int getLoc() const override;
	int getDest() const override;
	int primaryKey() const override;
	TableStat stat(Conjunction *ps, int mask, Context *context) const override;
	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	int step() const override;
	Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) override;
	VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) override;
	AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) override;
};

class ScatterPlan : public ExecPlan {
private:
	ExecPlan *lhs_, *rhs_;
	int comm_, dest_, lv_;

public:
	ScatterPlan(ExecPlan *a, ExecPlan *b, int comm, int dest, int lv);

	int getLoc() const override;
	int getDest() const override;
	int primaryKey() const override;
	TableStat stat(Conjunction *ps, int mask, Context *context) const override;
	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	int step() const override;
	Collection *selectedColumns(IREnv *env, Context *context,
			Conjunction *conj, int mask) override;
	VertexArray *evaluateExpr(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info, int tid) override;
	AggValue *aggregate(IREnv *env, Context *context,
			Conjunction *conj, ExprAST *e, CombinerInfo *info) override;
};

class GroupPlan : public ControlPlan {
private:
	Context *context_;
	Identifier *u_;
	Operator *op_;
	ExprAST *expr_;
	Conjunction *preds_;
	ExecPlan *p_; // the join result of the query
	TableName *ret_;

public:
	GroupPlan(Context *context, Identifier *u, Operator *op, ExprAST *expr,
			Conjunction *preds, ExecPlan *r, TableName *ret):
			context_(context), u_(u), op_(op), expr_(expr),
			preds_(preds), p_(r), ret_(ret) {}

	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	SeqSTM *transform(IREnv *env) override;
};

/*
class NonGroupPlan : public ControlPlan {
private:
	Context *context_;
	Identifier *u_;
	ExprAST *expr_;
	ExprList *preds_;
	ExecPlan *p_; // the join result of the query
	TableName *ret_;

public:
	NonGroupPlan(Context *context, Identifier *u, ExprAST *expr,
			ExprList *preds, ExecPlan *r, TableName *ret):
			context_(context), u_(u), expr_(expr),
			preds_(preds), p_(r), ret_(ret) {}

	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	SeqSTM *transform(IREnv *env) override;
};
*/

class ConstantPlan : public ControlPlan {
private:
	std::string str_;
	Type *type_;
	TableName *ret_;

public:
	ConstantPlan(std::string str, Type *t, TableName *ret):
			type_(t), str_(str), ret_(ret) {}

	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	SeqSTM *transform(IREnv *env) override;
};

class AggregatePlan : public ControlPlan {
private:
	Context *context_;
	Operator *op_;
	ExprAST *expr_;
	ExecPlan *p_;
	Conjunction *preds_;
	TableName *ret_;

public:
	AggregatePlan(Context *context, Operator *op, ExprAST *expr,
			Conjunction *preds, ExecPlan *r, TableName *ret):
			context_(context), op_(op), expr_(expr),
			preds_(preds), p_(r), ret_(ret) {}

	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	SeqSTM *transform(IREnv *env) override;
};

class ComposePlan : public ControlPlan {
private:
	ControlPlan *p1_, *p2_;

public:
	ComposePlan(ControlPlan *p1, ControlPlan *p2):
			p1_(p1), p2_(p2) {}

	void print(FILE *f) const override;
	void prepare(IREnv *env, int loopLv) override;
	SeqSTM *transform(IREnv *env) override;
};

// abstract class
class IteratePlan : public ControlPlan {
protected:
	ControlPlan *p_;
	int lv_;

public:
	IteratePlan(ControlPlan *p, int lv):p_(p), lv_(lv) {}

	void prepare(IREnv *env, int loopLv) override;
};

class ForRangePlan : public IteratePlan {
private:
	Identifier *name_;
	int lo_, hi_; // range
	int lv_;

public:
	ForRangePlan(ControlPlan *p, Identifier *name, int lo, int hi, int lv):
			IteratePlan(p, lv), name_(name), lo_(lo), hi_(hi) {}

	void print(FILE *f) const override;
	SeqSTM *transform(IREnv *env) override;
};

class FixedPointPlan : public IteratePlan {
private:
	Type *t_;
	TableName *tn_;

public:
	FixedPointPlan(Type *t, ControlPlan *p, TableName *tn, int lv):
			IteratePlan(p, lv), t_(t), tn_(tn) {}

	void print(FILE *f) const override;
	SeqSTM *transform(IREnv *env) override;
};

class Context {
private:
	std::unordered_set<int> used_;  // to check the existance
	std::vector<std::pair<std::string, Type*> > vars_; // all variables
	std::vector<TableExpr*> rels_;
	std::vector<ExprIR*> binds_;

	std::vector<std::pair<TableName*, Identifier*> > glb_vars_;

public:
	Context(const std::vector<TableExpr*> rels, TypeEnv *env);

	int lookup(const std::string &s) const; // return index
	int size() const;
	void dump(FILE *f = stderr) const;
	Type *getType(int index) const;
	const std::string &getVar(int index) const;
	bool isPrimaryKey(Identifier *v, TypeEnv *env) const;
	void showVars(int mask, const char *s = "") const;

	// for generating IR
	void bind(int idx, ExprIR *expr);
	ExprIR* trans(int idx) const;
	ExprIR* trans(const std::string &s, CodeGen *cg) const;

	const std::vector<TableExpr*> &rels() const;
};

struct SQLQuery {
	Operator *func;  // the final reduce function
	ExprList *exprs;
	Conjunction *preds;

	Context *context;
	int mask;     // a subset of potential join variables

	// query type
	int is_agg = 0;    // is the result a global value?
	int has_group = 0; // does the query use group-by?
	int dest;  // the destination to send the result

	SQLQuery(Context *context, Conjunction *cj);
	~SQLQuery() {}

	void addExpr(ExprAST *expr);
	void dump(FILE *f = stderr) const;
};

ExecPlan *solve(SQLQuery *query, TypeEnv *env);

#endif /* QUERY_H_ */