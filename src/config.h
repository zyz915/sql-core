#ifndef CONFIG_H
#define CONFIG_H

struct Config {
	/* number of spaces for indent (0 -> tab) */
	int spaces = 4;
	/* target language*/
	const char *language = "c++";
	/* target system */
	const char *target = "pregel-channel";
	/* class name for worker */
	const char *worker_name = "XWorker";
	/* turn on the optimization */
	int opt = 1;
	/* turn on fusion optimization */
	int fuse = 1;
	/* parameters (n #vertices, m #edges, w #workers) */
	double n = 1.0, m = 10.0;
	int w = 16;
	/* whether to disable the edge table */
	bool disableEdgeTable = false;

	Config() {}

	void check();
};

Config* getConfig();

#endif
