#include <vector>
#include <stack>

#include "ast.h"
#include "errorinfo.h"
#include "parser.h"
#include "tokenizer.h"
#include "types.h"
#include "query.h"

using namespace std;

extern Token CurTok;

static char err[256]; // buffer for error message
static void eatToken(int type) {
	if (CurTok.type != type) {
		Token tmp(1, 1, type);
		sprintf(err, "expect '%s' but '%s' is found",
				getTokenStr(tmp), getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	getNextToken();
}

static void eatSemiColon() {
	if (CurTok.type == tok_semicolon)
		getNextToken();
}

static Identifier *eatNameToken() {
	if (CurTok.type != tok_name) {
		sprintf(err, "expect identifier but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	Identifier *ret = new Identifier(CurTok, CurTok.val);
	getNextToken();
	return ret;
}

static TableName *eatTableName() {
	if (CurTok.type != tok_tname) {
		sprintf(err, "expect table name but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	TableName *ret = new TableName(CurTok, CurTok.val);
	getNextToken();
	return ret;
}

static Type *eatTypeToken() {
	Type *t = nullptr;
	switch (CurTok.type) {
	case tok_vid:
		t = getVidType();
		break;
	case tok_int:
		t = getIntType();
		break;
	case tok_bool:
		t = getBoolType();
		break;
	case tok_float:
		t = getFloatType();
		break;
	default:
		prtError(CurTok, "unknown type");
	}
	getNextToken();
	return t;
}

static int pre[128]; // precedence table
static void initPrecedence() {
	pre[tok_or ] = 4;
	pre[tok_and] = 5;
	pre[tok_xor] = 6;
	pre[tok_eq ] = 8;
	pre[tok_ne ] = 8;
	pre[tok_le ] = 9;
	pre[tok_lt ] = 9;
	pre[tok_ge ] = 9;
	pre[tok_gt ] = 9;
	pre[tok_add] = 11;
	pre[tok_sub] = 11;
	pre[tok_mul] = 12;
	pre[tok_div] = 12;
	pre[tok_mod] = 12;
	pre[tok_not] = 13;
	// these are not binary operator
	pre[0] = 0; // empty
	pre[tok_comma] = 1;
}

static bool isBinOp(int type) {
	return (type >= tok_add && type <= tok_ge)
		|| (type >= tok_and && type <= tok_or);
}

static bool isAggOp(int op) {
	return (op >= tok_max && op <= tok_avg);
}

// pre-definitions
static ExprAST *parseExpr(bool stop_before_and = false);
static Body *parseBody();
static Type *parseType();

IfExpr *parseIfExpr() {
	Pos pos = CurTok;
	eatToken(tok_if);
	eatToken(tok_lparen);
	ExprAST *cond = parseExpr();
	eatToken(tok_rparen);
	eatToken(tok_then);
	ExprAST *e1 = parseExpr();
	eatToken(tok_else);
	ExprAST *e2 = parseExpr();
	return new IfExpr(pos, cond, e1, e2);
}

// Primary :: name | int | float | bool
//          | 'inf' | ifexp | aggregator
//          | -exp | !exp | '(' exp ')'
//          | ...
static ExprAST *parsePrimary() {
	Pos pos = CurTok;
	ExprAST *ret = nullptr;
	TableName *fn = nullptr;
	Identifier *name = nullptr, *rf = nullptr;
	Operator *op = nullptr;
	switch (CurTok.type) {
	case tok_if:
		ret = parseIfExpr();
		break;
	case tok_ival:
		ret = new IntExpr(CurTok);
		getNextToken();
		break;
	case tok_fval:
		ret = new FloatExpr(CurTok);
		getNextToken();
		break;
	case tok_bool:
		ret = new BoolExpr(CurTok);
		getNextToken();
		break;
	case tok_name: // function or name or e.W
		ret = eatNameToken();
		break;
	case tok_inf:
		eatToken(tok_inf);
		ret = new InfiniteExpr(pos);
		break;
	case tok_lparen: // (exp) or pair
		eatToken(tok_lparen);
		ret = parseExpr();
		eatToken(tok_rparen);
		break;
	case tok_sub:
		eatToken(tok_sub);
		ret = new NegativeExpr(pos, parsePrimary());
		break;
	case tok_not:
		eatToken(tok_not);
		ret = new NotExpr(pos, parsePrimary());
		break;
	case tok_bval:
		ret = new BoolExpr(CurTok);
		getNextToken();
		break;
	default:
		prtError(pos, "unrecognized expression.");
	}
	return ret;
}

static void reduce(stack<ExprAST*> &exps, stack<Operator*> &ops) {
	Operator *op = ops.top();
	ops.pop();
	ExprAST *e2 = exps.top();
	exps.pop();
	ExprAST *e1 = exps.top();
	exps.pop();
	exps.push(new BinaryExpr(*op, op, e1, e2));
}
// Expr :: primary { op primary }*
// operations are "+" "-" "*" "/" "%" etc.
static ExprAST *parseExpr(bool stop_before_and) {
	stack<ExprAST*> exps;
	stack<Operator*> ops;
	exps.push(parsePrimary());
	int type = CurTok.type;
	while (isBinOp(type) && (!stop_before_and || exps.size() - ops.size() > 1 || type != tok_and)) {
		while (!ops.empty() && pre[type] <= pre[ops.top()->type])
			reduce(exps, ops); 
		ops.push(new Operator(CurTok, type));
		getNextToken();
		exps.push(parsePrimary());
		type = CurTok.type;
	}
	while (!ops.empty())
		reduce(exps, ops);
	return exps.top();
}

static TableExpr *parseTable() {
	TableName *tn = eatTableName();
	Schema *s = getTypeEnv()->getSchema(tn);
	if (s == nullptr)
		prtError(*tn, "table not defined.");
	int cols = s->numCols();
	eatToken(tok_lparen);
	TableExpr *tab = new TableExpr(tn);
	for (int i = 0; i < cols; i++) {
		Identifier *id = eatNameToken();
		tab->add(id);
		if (i != cols - 1)
			eatToken(tok_comma);
	}
	eatToken(tok_rparen);
	return tab;
}

static Schema *parseSchema() {
	Type *t = getUnitType();
	int ty = CurTok.type;
	if (ty != tok_vt && ty != tok_et && ty != tok_gv && ty != tok_od) {
		sprintf(err, "expect table schema but %s is found.",
				getTokenStr(CurTok.type));
		prtParseError(CurTok, err);
	}
	getNextToken();
	eatToken(tok_lparen);
	if (CurTok.type == tok_void) {
		eatToken(tok_void);
		eatToken(tok_rparen);
	} else {
		t = eatTypeToken();
		eatToken(tok_rparen);
	}
	switch (ty) {
		case tok_vt:
		case tok_od:
			return new VertexTable(t);
		case tok_et:
			return new EdgeTable(t);
		case tok_gv:
			return new GlobalValue(t);
	}
	return nullptr;
}

void Aggregation() {
	/*
	case tok_max:
	case tok_min:
	case tok_sum:
	case tok_cnt:
	case tok_avg:
		op = new Operator(CurTok, CurTok.type);
		getNextToken();
		eatToken(tok_lparen);
		ret = new AggExpr(op, parseExpr());
		eatToken(tok_rparen);
		break;
	*/
}

static QueryBlock *parseQuery() {
	Pos pos = CurTok;
	eatToken(tok_set);
	TableName *tn = eatTableName();
	if (CurTok.type == tok_eq)
		prtError(CurTok, "expect table schema.");
	eatToken(tok_colon);
	Schema *s = parseSchema();
	TypeEnv *env = getTypeEnv();
	if (!env->tableExists(tn))
		env->addTable(tn, s);
	else if (s->equals(env->getSchema(tn))) {
		fprintf(stderr, "expected type: ");
		s->dump(stderr);
		fprintf(stderr, "  actual type: ");
		env->getSchema(tn)->dump(stderr);
		fprintf(stderr, "\n");
		prtError(*tn, "unmatched types.");
	}
	eatToken(tok_eq);
	eatToken(tok_select);
	QueryBlock *qb;
	if (typeid(*s) == typeid(VertexTable)) {
		Identifier *u = eatNameToken();
		eatToken(tok_comma);
		if (isAggOp(CurTok.type)) {
			Operator *op = new Operator(CurTok, CurTok.type);
			getNextToken();
			eatToken(tok_lparen);
			ExprAST *e = parseExpr();
			eatToken(tok_rparen);
			qb = new VertexTableQuery(pos, tn, u, op, e);
		} else {
			ExprAST *e = parseExpr();
			qb = new VertexTableQuery(pos, tn, u, e);
		}
	} else
	if (typeid(*s) == typeid(GlobalValue)) {
		if (!isAggOp(CurTok.type)) {
			sprintf(err, "expect function but %s is found", getTokenStr(CurTok));
			prtError(CurTok, err);
		}
		Operator *op = new Operator(CurTok, CurTok.type);
		getNextToken();
		eatToken(tok_lparen);
		ExprAST *e = parseExpr();
		eatToken(tok_rparen);
		qb = new GlobalValueQuery(pos, tn, op, e);
	}
	eatToken(tok_from);
	TableExpr *tb = parseTable();
	qb->addTable(tb);
	while (CurTok.type == tok_join) {
		eatToken(tok_join);
		tb = parseTable();
		qb->addTable(tb);
	}
	if (CurTok.type == tok_where) {
		eatToken(tok_where);
		ExprAST *c = parseExpr(true);
		qb->addPred(c);
		while (CurTok.type == tok_and) {
			eatToken(tok_and);
			c = parseExpr(true);
			qb->addPred(c);
		}
	}
	if (CurTok.type == tok_group) {
		eatToken(tok_group);
		eatToken(tok_by);
		Identifier *name = eatNameToken();
		qb->setGroupBy(name);
	}
	return qb;
}

// Range :: 'for' var 'in' Int 'to' Int 'do' < Program > 'end'
static ForRange *parseForRange() {
	eatToken(tok_for);
	Identifier *name = eatNameToken();
	eatToken(tok_in);
	IntExpr *lo = new IntExpr(CurTok);
	getNextToken();
	eatToken(tok_to);
	IntExpr *hi = new IntExpr(CurTok);
	getNextToken();
	eatToken(tok_do);
	Body *body = parseBody();
	eatToken(tok_end);
	return new ForRange(body, name, lo, hi);
}

// DoUntil :: 'do' < Program > 'until' 'fix' ( fields )
static DoUntil *parseDoUntil() {
	eatToken(tok_do);
	Body *body = parseBody();
	eatToken(tok_until);
	DoUntil *ret = nullptr;
	int ty = CurTok.type;
	if (ty == tok_fix) {
		eatToken(tok_fix);
		eatToken(tok_lparen);
		ret = new DoUntil(body, eatTableName());
		eatToken(tok_rparen);
		return ret;
	}
	prtParseError(CurTok, "invalid termination condition");
	return nullptr;
}

static Body *parseBody() {
	vector <Body*> progs;
	bool brk = false;
	while (!brk && CurTok.type != tok_eof) {
		switch (CurTok.type) {
		case tok_do:
			progs.push_back(parseDoUntil());
			break;
		case tok_for:
			progs.push_back(parseForRange());
			break;
		case tok_set:
			progs.push_back(parseQuery());
			break;
		default:
			brk = true;
		}
	}
	if (progs.size() == 0)
		prtParseError(CurTok, "no palgol program");
	// convert array to list
	Body *ret = progs.back();
	for (int i = (int) progs.size() - 2; i >= 0; i--)
		ret = new Compose(progs[i], ret);
	eatSemiColon();
	return ret;
}

static void parseInputOutput(Program *prog) {
	while (CurTok.type == tok_input) {
		eatToken(tok_input);
		Token tok = CurTok;
		Schema *s = parseSchema();
		TableName *tn = eatTableName();
		eatSemiColon();
		getTypeEnv()->addTable(tn, s);
		prog->headers.push_back(new Header(false, tok, s, tn));
	}
}

Program *parse() {
	Program *prog = new Program();
	initTokenizer();
	initPrecedence();
	getNextToken();
	parseInputOutput(prog);
	prog->body = parseBody();
	return prog;
}

int getPrec(int op) {
	return pre[op];
}
