#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "parser.h"
#include "printable.h"
#include "types.h"

#include <cstdio>
#include <cstring>

int usage(const char *prog) {
	fprintf(stderr, "usage: %s [options] <source file>\n", prog);
	fprintf(stderr, "  where the options are:\n");
	fprintf(stderr, "    -o=<string>     output file (a C++ program), or print to stdout if not specified\n");
	fprintf(stderr, "    -spaces=<int>   number of spaces for indentation (in C++ code), default: 4\n");
	fprintf(stderr, "    -use-tab        use tab instead of spaces for indentation (in C++ code)\n");
	fprintf(stderr, "    -opt=on/off     whether or not to detect graph-specific optimizations, default: on\n");
	fprintf(stderr, "    -fuse=on/off    whether or not to enable the fuse optimization, default: on\n");
	fprintf(stderr, "    -debug=on/off   debugging information, default: off\n");
	fprintf(stderr, "    -n=<double>     estimated number of vertices, default: 1.0\n");
	fprintf(stderr, "    -m=<double>     estimated number of edges, default: 10.0\n");
	fprintf(stderr, "    -w=<int>        number of workers to use, default: 32\n");
	fprintf(stderr, "\n");
	return 0;
}

int main(int argc, char **argv)
{
	const char *inpfile = "";
	const char *outfile = "";
	bool debug = false;

	Config *conf = getConfig();
	int p = 1;
	while (p <= argc) {
		if (p == argc) {
			fprintf(stderr, "error: no input file\n");
			return usage(argv[0]);
		}
		const char *flag = argv[p++];
		if (flag[0] != '-') {
			inpfile = flag;
			break;
		}
		if (!strcmp(flag, "-h") || !strcmp(flag, "--help"))
			return usage(argv[0]);
		if (!strncmp(flag, "-o=", 3)) {
			outfile = strdup(flag + 3);
			continue;
		}
		if (!strcmp(flag, "-use-tab")) {
			conf->spaces = 0;
			continue;
		}
		if (!strncmp(flag, "-spaces=", 8)) {
			conf->spaces = atoi(flag + 8);
			continue;
		}
		if (!strncmp(flag, "-target=", 8)) {
			conf->target = strdup(flag + 8);
			continue;
		}
		if (!strncmp(flag, "-debug=", 7)) {
			if (!strncmp(flag + 7, "on", 2))
				debug = true;
			continue;
		}
		if (!strncmp(flag, "-fuse=", 6)) {
			if (!strncmp(flag + 6, "off", 3))
				conf->fuse = 0;
			continue;
		}
		if (!strncmp(flag, "-opt=", 5)) {
			if (!strncmp(flag + 5, "off", 3))
				conf->opt = 0;
			continue;
		}
		if (!strncmp(flag, "-n=", 3)) {
			conf->n = atof(flag + 3);
			continue;
		}
		if (!strncmp(flag, "-m=", 3)) {
			conf->m = atof(flag + 3);
			continue;
		}
		if (!strncmp(flag, "-w=", 3)) {
			conf->w = atoi(flag + 3);
			continue;
		}
		fprintf(stderr, "unrecognized option '%s'\n", flag);
		return usage(argv[0]);
	}
	conf->check();

	FILE *prt = (outfile[0] ? fopen(outfile, "w") : stdout);
	setInputStream(fopen(inpfile, "r"));
	Program *prog = parse();
	typeCheck(prog, debug);
	SeqSTM *stm = transform(prog, debug);
	genChannelBased(prog, stm, prt, debug);
	return 0;
}
