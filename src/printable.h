#ifndef PRINTABLE_H_
#define PRINTABLE_H_

class Printable {
public:
	Printable() {}
	virtual ~Printable() {}

	virtual void print(FILE *f) const = 0;
};

void debug(Printable *obj, FILE *f = stderr);

#endif /* PRINTABLE */