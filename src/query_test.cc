#include "ast.h"
#include "parser.h"
#include "printable.h"
#include "types.h"

#include <cstdio>

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	Program *prog = parse();
	typeCheck(prog, false);
	SeqSTM *stm = transform(prog, true);
	debug(stm, stderr);
	return 0;
}
