#include <algorithm>
#include <cstdio>
#include <cstdlib>

#include "ast.h"
#include "config.h"
#include "errorinfo.h"
#include "intermediate.h"
#include "parser.h"
#include "printable.h"
#include "query.h"
#include "types.h"

using namespace std;

static char buf[128];
static string float_to_string(double v) {
	const double eps = 1e-8;
	int x, sgn = (v < -eps ? -1 : v > eps);
	char *c = buf + 31, *s = c;
	if (v < 0) v = -v;
	v += eps;
	x = (int) v;
	v -= x;
	*c = 0;
	do {
		*--c = '0' + (x % 10);
		x /= 10;
	} while (x > 0);
	if (sgn == -1)
		*--c = '-';
	double thr = 1e-7;
	*s++ = '.';
	for (int i = 0; i < 6; i++) {
		v *= 10;
		x = (int) v;
		v -= x;
		*s++ = '0' + x;
		thr *= 10;
		if (v < thr) break;
	}
	*s = 0;
	return string(c);
}

static string int_to_string(int v) {
	char *p = buf + 20;
	int sgn = v < 0 ? 1 : v > 0;
	if (v < 0) v = -v;
	*p = 0;
	do {
		*--p = '0' + v % 10;
		v /= 10;
	} while (v > 0);
	if (sgn == -1)
		*--p = '-';
	return string(p);
}

string CombinerInfo::getOp1() const {
	int ty = op->type;
	switch (ty) {
		case tok_cnt: return "+";
		case tok_sum: return "+";
		case tok_and: return "&&";
		case tok_or:  return "||";
		default: {
			prtError("unsupported reduction in OpenMP");
			return "";
		}
	}
	return "";
}

string CombinerInfo::getOp2() const {
	int ty = op->type;
	switch (ty) {
		case tok_cnt: return "+=";
		case tok_sum: return "+=";
		case tok_and: return "&=";
		case tok_or:  return "|=";
		default: {
			prtError("unsupported reduction in C++");
			return "";
		}
	}
	return "";
}

string CombinerInfo::getOp3() const {
	int ty = op->type;
	switch (ty) {
		case tok_cnt: return "MPI_SUM";
		case tok_sum: return "MPI_SUM";
		case tok_and: return "MPI_LAND";
		case tok_or:  return "MPI_LOR";
		default: {
			prtError("unsupported reduction in MPI");
			return "";
		}
	}
	return "";
}

string CombinerInfo::getIdentity() const {
	int ty = op->type;
	switch (ty) {
		case tok_cnt: return "0";
		case tok_sum: return "0";
		case tok_and: return "false";
		case tok_or:  return "true";
		default: {
			prtError("unsupported combiner identity");
			return "";
		}
	}
	return "";
}

Type *ChannelInfo::type() const {
	return type_;
}

CombinerInfo *MsgChannel::getCombiner() const {
	return nullptr;
}

CombinerInfo *Combiner::getCombiner() const {
	return comb_;
}

CombinerInfo *ScatterCombine::getCombiner() const {
	return comb_;
}

CombinerInfo *GroupBy::getCombiner() const {
	return comb_;
}

CombinerInfo *CreateVec::getCombiner() const {
	return nullptr;
}

CombinerInfo *ReqChannel::getCombiner() const {
	return nullptr;
}

CombinerInfo *Aggregator::getCombiner() const {
	return comb_;
}

/*
CombinerInfo *PeerToPeer::getCombiner() const {
	return nullptr;
}*/

void MsgChannel::genDef(CodeGen *cg, const string &cn) const {
	string s = "DirectMessage<" + cg->key()->toString(cg) +
			", " + type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void Combiner::genDef(CodeGen *cg, const string &cn) const {
	string s = "PushCombine<" + cg->key()->toString(cg) +
			", " + type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void ScatterCombine::genDef(CodeGen *cg, const string &cn) const {
	string s = "ScatterCombine<" + cg->key()->toString(cg) +
			", " + type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void ScatterCombine::genLoad(CodeGen *cg, const string &cn) const {
	string s = cn + ".load(es);";
	cg->add(new Text(s));
}

void GroupBy::genDef(CodeGen *cg, const string &cn) const {
	string s = "CombinedValues<" + cg->key()->toString(cg) +
			", " + type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void ReqChannel::genDef(CodeGen *cg, const string &cn) const {
	string s = "RequestRespond<" + cg->key()->toString(cg) +
			", " +  type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void CreateVec::genDef(CodeGen *cg, const string &cn) const {
	string s = "VectorCreator<" + cg->key()->toString(cg) +
			", " +  type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s));
}

void Aggregator::genDef(CodeGen *cg, const string &cn) const {
	//string s = "Aggregator<" + type_->toString(cg) + "> " + cn + ";";
	string s = type_->toString(cg) + " " + cn + "; // aggregator";
	cg->add(new Text(s)); 
}

/*
void PeerToPeer::genDef(CodeGen *cg, const string &cn) const {
	string s = "PeerToPeer<" + type_->toString(cg) + "> " + cn + ";";
	cg->add(new Text(s)); 
}*/

int ChannelManager::allocate(ChannelInfo *info) {
	int ret = ++cid_;
	channels_.insert(make_pair(ret, info));
	//if (typeid(*info) == typeid(PeerToPeer))
	//	c2p_.insert(make_pair(ret, ++pid_));
	return ret;
}

int ChannelManager::allocate(Type *t) {
	return allocate(new MsgChannel(t));
}

void ChannelManager::remove(int ch) {
	channels_.erase(ch);
}

ChannelInfo* ChannelManager::getInfo(int ch) {
	auto it = channels_.find(ch);
	if (it == channels_.end()) {
		sprintf(buf, "channel %d not found", ch);
		prtError(buf);
		return nullptr;
	} else
		return it->second;
}

CombinerInfo* ChannelManager::getCombiner(int ch) {
	auto it = channels_.find(ch);
	if (it == channels_.end()) {
		sprintf(buf, "channel %d not found", ch);
		prtError(buf);
		return nullptr;
	} else {
		return it->second->getCombiner();
	}
}

string ChannelManager::getName(int ch) const {
	if (c2p_.find(ch) == c2p_.end())
		sprintf(buf, "ch%d", ch);
	else {
		auto it = c2p_.find(ch);
		sprintf(buf, "p%d", it->second);
	}
	return string(buf);
}

void ChannelManager::loadChannels(CodeGen *cg) const {
	char buf[64];
	Type *key = cg->key();
	for (auto channel : channels_) {
		int id = channel.first;  // channed id;
		ChannelInfo *ty = channel.second; // channel info;
		string cn = getName(id);
		ty->genLoad(cg, cn);
	}
}

void ChannelManager::listChannels(CodeGen *cg) const {
	char buf[64];
	Type *key = cg->key();
	for (auto channel : channels_) {
		int id = channel.first;  // channed id;
		ChannelInfo *ty = channel.second; // channel info;
		string cn = getName(id);
		ty->genDef(cg, cn);
	}
}

void ChannelManager::listConstructors(CodeGen *cg) const {
	Text *text = new Text();
	int cnt = 0;
	for (auto channel : channels_) {
		// aggregator is no longer a channel
		if (typeid(*channel.second) == typeid(Aggregator))
			continue;
		int ch = channel.first;
		CombinerInfo *info = channel.second->getCombiner();
		string var = getName(ch);
		if (info == nullptr) { // combiner not specified
			text->append(var + "(this), ");
		} else {
			int ty = info->op->type;
			string func;
			string identity = info->e->toString();
			switch (ty) {
				case tok_sum: {
					func = "c_sum";
					break;
				}
				case tok_cnt: {
					func = "c_sum";
					break;
				}
				case tok_max: {
					func = "c_max";
					break;
				}
				case tok_min: {
					func = "c_min";
					break;
				}
				case tok_and: {
					func = "c_land";
					break;
				}
				case tok_or: {
					func = "c_lor";
					break;
				}
				default:
					func = "c_rep"; // replace
					break;
			}
			// for pretty printing
			if (text->length() > 25) {
				cg->add(text);
				text = new Text();
			}
			text->append(var + "(this, make_combiner(" + func +
					", " + identity + ")), ");
		}
	}
	cg->add(text);
}

void LocalArray::genDef(CodeGen *cg, const string &n) const {
	if (!type_->equals(getUnitType()))
		cg->add(new Text("vector<" + type_->toString(cg) + "> " + n + ";"));
}

void LocalVector::genDef(CodeGen *cg, const string &n) const {
	cg->add(new Text("vector<pair<" + cg->key()->toString(cg) +
			", " + type_->toString(cg) + "> > " + n + ";"));
}

void LocalValue::genDef(CodeGen *cg, const string &n) const {
	cg->add(new Text(type_->toString(cg) + " " + n + ";"));
}

int MemoryManager::allocate(MSchema *s) {
	int ret = ++cid_;
	schema_.insert(make_pair(ret, s));
	return ret;
}

int MemoryManager::allocateVar(Type *t) {
	fail(t == nullptr, "MemoryManager::allocateVar()");
	int ret = allocate(new LocalValue(t));
	sprintf(buf, "t%d", ++vid_);
	name2id_.insert(make_pair(string(buf), ret));
	id2name_.insert(make_pair(ret, string(buf)));
	return ret;
}

int MemoryManager::addVertexTable(TableName *tn_, Type *t) {
	const string &str = tn_->getString();
	auto it = name2id_.find(str);
	if (it == name2id_.end()) {
		fail(t == nullptr, tn_->getString().c_str());
		int ret = allocate(new LocalArray(t));
		name2id_.insert(make_pair(str, ret));
		id2name_.insert(make_pair(ret, str));
		return ret;
	} else
		return it->second;
}

int MemoryManager::addEdgeTable(TableName *tn_, Type *t) {
	const string &str = tn_->getString();
	auto it = name2id_.find(str);
	TupleType *tt = new TupleType();
	tt->add(getVidType());
	if (!t->equals(getUnitType()))
		tt->add(t);
	if (it == name2id_.end()) {
		fail(t == nullptr, tn_->getString().c_str());
		int ret = allocate(new LocalVector(tt));
		name2id_.insert(make_pair(str, ret));
		id2name_.insert(make_pair(ret, str));
		return ret;
	} else
		return it->second;
}

int MemoryManager::addGlobalValue(TableName *tn_, Type *t) {
	const string &str = tn_->getString();
	auto it = name2id_.find(str);
	if (it == name2id_.end()) {
		fail(t == nullptr, tn_->getString().c_str());
		int ret = allocate(new LocalValue(t));
		name2id_.insert(make_pair(str, ret));
		id2name_.insert(make_pair(ret, str));
		return ret;
	} else
		return it->second;
}

MSchema *MemoryManager::getSchema(int id) {
	auto it = schema_.find(id);
	if (it == schema_.end()) {
		sprintf(buf, "variable %d not found", id);
		prtError(buf);
		return nullptr;
	} else
		return it->second;
}

string MemoryManager::getName(int id) const {
	auto it = id2name_.find(id);
	if (it != id2name_.end()) {
		string ret = it->second;
		replace(ret.begin(), ret.end(), '\'', '_');
		return ret;
	}
	return string("var") + int_to_string(id);
}

void MemoryManager::setIO(const std::vector<Header*> &headers) {
	headers_ = headers;
	for (Header *hd : headers)
		if (hd->tok.type == tok_et)
			val_ = hd->schema->type();
}

void MemoryManager::defineTables(CodeGen *cg) {
	Type *key = cg->key();
	for (auto rec : schema_) {
		int id = rec.first;
		MSchema *s = rec.second;
		string cn = getName(id);
		s->genDef(cg, cn);
	}
	// a special edge table
	for (Header *hd : headers_) {
		string name = hd->tn->getString();
		Schema *s = hd->schema;
		if (typeid(*s) == typeid(EdgeTable)) {
			Type *t = s->type();
			cg->add(new Text("Edge<" + cg->key()->toString(cg)
					+ ", " + t->toString(cg) + "> edge;"));
		}
	}
}

/*
void MemoryManager::addP2Pchannel(CodeGen *cg) {
	for (auto hd : headers_) {
		int id = name2id_[hd->tn->getString()];
		Type *t = getSchema(id)->type();
		Type *pt = new PairType(cg->key(), t);
		hd->ch = cg->getCM()->allocate(new PeerToPeer(pt));
	}
}

void MemoryManager::parseLine(CodeGen *cg) {
	// temporary variables
	map<string, string> tmp;
	int ts = 0;
	for (auto hd : headers_) {
		int id = name2id_[hd->tn->getString()];
		Type *t = getSchema(id)->type();
		if (tmp.find(t->toString(cg)) == tmp.end()) {
			sprintf(buf, "t%d", ts++);
			tmp.insert(make_pair(t->toString(cg), string(buf)));
		}
		Schema *s = hd->schema;
	}
	// definitions
	map<string, vector<string> > vars;
	vars[cg->key()->toString(cg)].push_back(string("key"));
	cg->add(new Text("Parser parser(line);"));
	for (auto t : tmp)
		vars[t.first].push_back(t.second);
	for (auto defs : vars) {
		vector<string> &vs = defs.second;
		Text *text = new Text(defs.first + " " + vs[0]);
		for (int i = 1; i < vs.size(); i++)
			text->append(", " + vs[i]);
		text->append(";");
		cg->add(text);
	}
	// parsing
	cg->add(new Text("parser.parse(key);"));
	cg->add(new Text("int h = hash(key);"));
	cg->add(new Text("int num = parser.getInt();"));
	cg->add(new Text("for (int i = 0; i < num; i++) {"));
	cg->add(new Increase());
	for (auto hd : headers_) {
		int id = name2id_[hd->tn->getString()];
		Type *t = getSchema(id)->type();
		string cn = cg->getCM()->getName(hd->ch);
		string v = tmp[t->toString(cg)];
		cg->add(new Text("parser.parse(" + v + ");"));
		cg->add(new Text(cn + ".send_message(h, " +
				"make_pair(key, " + v + "));"));
	}
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text("return key;"));
}*/

void MemoryManager::dumpTables(CodeGen *cg) {
	return;
}

void MemoryManager::initTables(CodeGen *cg) {
	// load input tables (only the edge table)
	set<int> init;
	for (auto hd : headers_) {
		string name = hd->tn->getString();
		int id = name2id_[name];
		switch (hd->tok.type) {
			case tok_et: {
				// a special channel for edge table
				if (!getConfig()->disableEdgeTable) {
					cg->add(new Text("edge.load(es);"));
					cg->add(new Text("edge.collect(" + name + ");"));
					init.insert(id);
				}
				break;
			}
			case tok_od: {
				cg->add(new Text("set_out_degree(" + name + ");"));
				init.insert(id);
				break;
			}
			case tok_id: {
				cg->add(new Text("set_in_degree(" + name + ");"));
				init.insert(id);
				break;
			}
		}
	}
	// resize
	for (auto it : schema_) {
		int id = it.first;
		MSchema *s = it.second;
		if (typeid(*s) == typeid(LocalArray) &&
				!s->type()->equals(getUnitType()) &&
				init.find(id) == init.end()) {
			string cn = getName(id);
			// TODO: cheating ..
			if (cn == "Deg") {
				cg->add(new Text("set_out_degree(Degree);"));
				continue;
			}
			cg->add(new Text(cn + ".resize(numv());"));
		}
	}
}

string IntIR::toString() const {
	switch (flag_) {
		case FLAGS::MIN:
			return "INT_MIN";
		case FLAGS::MAX:
			return "INT_MAX";
		case FLAGS::NORMAL:
			return int_to_string(val_);
	}
	return "";
}

string FloatIR::toString() const {
	switch (flag_) {
		case FLAGS::MIN:
			return "DBL_MAX";
		case FLAGS::MAX:
			return "DBL_MAX";
		case FLAGS::NORMAL:
			return float_to_string(val_);
	}
	return "";
}

void Constructor::add(ExprIR *expr) {
	list_.push_back(expr);
	((TupleType*) type_)->add(expr->type());
}

int Constructor::size() const {
	return list_.size();
}

string BoolIR::toString() const {
	return (val_ ? string("true") : string("false"));
}

int BinaryIR::prec() const {
	return getPrec(op_->type);
}

void VarIR::print(FILE *f) const {
	fprintf(f, "(VarIR %d)", id_);
}

void IntIR::print(FILE *f) const {
	switch (flag_) {
		case FLAGS::NORMAL: {
			fprintf(f, "(IntIR %d)", val_);
			break;
		}
		case FLAGS::MAX: {
			fprintf(f, "(IntIR inf)");
			break;
		}
		case FLAGS::MIN: {
			fprintf(f, "(IntIR -inf)");
			break;
		}
	}
}

void FloatIR::print(FILE *f) const {
	switch (flag_) {
		case FLAGS::NORMAL: {
			fprintf(f, "(FloatIR %f)", val_);
			break;
		}
		case FLAGS::MAX: {
			fprintf(f, "(FloatIR inf)");
			break;
		}
		case FLAGS::MIN: {
			fprintf(f, "(FloatIR -inf)");
			break;
		}
	}
}

void BoolIR::print(FILE *f) const {
	fprintf(f, "(BoolIR %s)", (val_ ? "true" : "false"));
}

void MakePair::print(FILE *f) const {
	fprintf(f, "(MakePair ");
	e1_->print(f);
	fprintf(f, ", ");
	e2_->print(f);
	fprintf(f, ")");
}

void BinaryIR::print(FILE *f) const {
	fprintf(f, "(BinaryIR %s ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void IfExprIR::print(FILE *f) const {
	fprintf(f, "(IfExprIR ");
	cond_->print(f);
	fprintf(f, " ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void UnaryIR::print(FILE *f) const {
	fprintf(f, "(UnaryIR %s ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void AggValue::print(FILE *f) const {
	fprintf(f, "(AggValue %d)", ch_);
}

void VertexId::print(FILE *f) const {
	fprintf(f, "(VertexId)");
}

void VertexAttr::print(FILE *f) const {
	fprintf(f, "(VertexAttr %d)", id_);
}

void VecValue::print(FILE *f) const {
	fprintf(f, "(VecValue %d)", id_);
}

void Constructor::print(FILE *f) const {
	fprintf(f, "(Constructor ");
	list_[0]->print(f);
	for (int i = 1; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void SelectIR::print(FILE *f) const {
	fprintf(f, "(SelectIR ");
	expr_->print(f);
	fprintf(f, " %d)", idx_);
}

void BuiltinIR::print(FILE *f) const {
	fprintf(f, "(BuiltinIR %s)", str_.c_str());
}

void GetRespValue::print(FILE *f) const {
	fprintf(f, "(GetRespValue %d)", ch_);
}

void ArrayCopy::print(FILE *f) const {
	fprintf(f, "(ArrayCopy %d %d)", dst_, src_);
}

void ArrayCompare::print(FILE *f) const {
	fprintf(f, "(ArrayCompare (Aggregator %d) %d %d)", ch_, lhs_, rhs_);
}

void ClearArray::print(FILE *f) const {
	fprintf(f, "(ClearArray %d)", id_);
}

void InitAgg::print(FILE *f) const {
	fprintf(f, "(SetAgg (Aggregator %d) ", ch_);
	if (expr_ != nullptr) {
		expr_->print(f);
	} else {
		fprintf(f, "default");
	}
	fprintf(f, ")");
}

void SetAgg::print(FILE *f) const {
	fprintf(f, "(SetAgg (Aggregator %d) ", ch_);
	expr_->print(f);
	fprintf(f, ")");
}

void AllReduce::print(FILE *f) const {
	fprintf(f, "(AllReduce (Aggregator %d))", ch_);
}

void AssignIR::print(FILE *f) const {
	fprintf(f, "(AssignIR ");
	lval_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void CondIR::print(FILE *f) const {
	fprintf(f, "(CondIR ");
	cond_->print(f);
	fprintf(f, " ");
	body_->print(f);
	fprintf(f, ")");
}

void Vector::print(FILE *f) const {
	fprintf(f, "(Vector %d)", id_);
}

void ZippedVector::print(FILE *f) const {
	fprintf(f, "(ZippedVector ");
	v1_->print(f);
	fprintf(f, " ");
	v2_->print(f);
	fprintf(f, ")");
}

void SendMessage::print(FILE *f) const {
	fprintf(f, "(SendMessage %d ", ch_);
	dest_->print(f);
	fprintf(f, ", ");
	expr_->print(f);
	fprintf(f, ")");
}

void AddEdge::print(FILE *f) const {
	fprintf(f, "(SendMessage %d ", ch_);
	expr_->print(f);
	fprintf(f, ")");
}

void AddRequestIR::print(FILE *f) const {
	fprintf(f, "(AddRequestIR %d ", ch_);
	dest_->print(f);
	fprintf(f, ")");
}

void RequestIR::print(FILE *f) const {
	fprintf(f, "(RequestIR %d (VertexArray %d))", ch_, id_);
}

void RespondIR::print(FILE *f) const {
	fprintf(f, "(RespondIR %d %d)", ch_, id_);
}

void SetMessage::print(FILE *f) const {
	fprintf(f, "(SetMessage %d %d)", ch_, id_);
}

void Activate::print(FILE *f) const {
	fprintf(f, "(Activate %d)", ch_);
}

void OmpForReduction::print(FILE *f) const {
	fprintf(f, "(Reduction (Aggregator %d) ", ch_);
	p_->print(f);
	fprintf(f, ")");
}

void VertexArray::print(FILE *f) const {
	fprintf(f, "(VertexArray %d)", id_);
}

void ZippedArray::print(FILE *f) const {
	fprintf(f, "(ZippedArray ");
	v1_->print(f);
	fprintf(f, " ");
	v2_->print(f);
	fprintf(f, ")");
}

void ScanArray::print(FILE *f) const {
	fprintf(f, "(ScanArray ");
	c_->print(f);
	fprintf(f, ")");
	p_->print(f);
	fprintf(f, ")");
}

void MapArray::print(FILE *f) const {
	fprintf(f, "(MapArray ");
	c_->print(f);
	fprintf(f, " (VertexArray %d) ", id_);
	expr_->print(f);
	fprintf(f, ")");
}

void ScanVector::print(FILE *f) const {
	fprintf(f, "(ScanVector ");
	c_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void Join::print(FILE *f) const {
	fprintf(f, "(Join ");
	l_->print(f);
	fprintf(f, " ");
	r_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void Join2::print(FILE *f) const {
	fprintf(f, "(Join ");
	arr_->print(f);
	fprintf(f, " ");
	vec_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void JoinVectors::print(FILE *f) const {
	fprintf(f, "(JoinVectors ");
	v1_->print(f);
	fprintf(f, " ");
	v2_->print(f);
	fprintf(f, " ");
	p_->print(f);
	fprintf(f, ")");
}

void ArrayFromChannel::print(FILE *f) const {
	fprintf(f, "(ArrayFromChannel %d (VertexArray %d))", ch_, id_);
}

void VectorFromChannel::print(FILE *f) const {
	fprintf(f, "(VectorFromChannel %d (Vector %d))", ch_, id_);
}

void ClearChannel::print(FILE *f) const {
	fprintf(f, "(ClearChannel %d)", ch_);
}

void VarIR::genChannelBased(CodeGen *cg, Text *text) {
	text->append(cg->getMM()->getName(id_));
}

void IntIR::genChannelBased(CodeGen *cn, Text *text) {
	switch (flag_) {
		case FLAGS::MIN: {
			text->append("INT_MIN");
			break;
		}
		case FLAGS::MAX: {
			text->append("INT_MAX");
			break;
		}
		default:
			text->append(int_to_string(val_));
	}
}

void FloatIR::genChannelBased(CodeGen *cn, Text *text) {
	switch (flag_) {
		case FLAGS::MIN: {
			text->append("DBL_MIN");
			break;
		}
		case FLAGS::MAX: {
			text->append("DBL_MAX");
			break;
		}
		default:
			text->append(float_to_string(val_));
	}
}

void BoolIR::genChannelBased(CodeGen *cg, Text *text) {
	text->append(val_ ? "true" : "false");
}

void MakePair::genChannelBased(CodeGen *cg, Text *text) {
	text->append("make_pair(");
	e1_->genChannelBased(cg, text);
	text->append(", ");
	e2_->genChannelBased(cg, text);
	text->append(")");
}

void BinaryIR::genChannelBased(CodeGen *cg, Text *text) {
	int lp = e1_->prec();
	int rp = e2_->prec();
	int me = getPrec(op_->type);
	if (lp < me) {
		text->append("(");
		e1_->genChannelBased(cg, text);
		text->append(")");
	} else
		e1_->genChannelBased(cg, text);
	sprintf(buf, " %s ", getCppTokenStr(op_->type));
	text->append(string(buf));
	if (rp <= me) {
		text->append("(");
		e2_->genChannelBased(cg, text);
		text->append(")");
	} else
		e2_->genChannelBased(cg, text);
}

void IfExprIR::genChannelBased(CodeGen *cg, Text *text) {
	text->append("(");
	cond_->genChannelBased(cg, text);
	text->append(" ? ");
	e1_->genChannelBased(cg, text);
	text->append(" : ");
	e2_->genChannelBased(cg, text);
	text->append(")");
}

void UnaryIR::genChannelBased(CodeGen *cg, Text *text) {
	switch (op_->type) {
		case tok_sub: {
			text->append("-");
			break;
		}
		case tok_not: {
			text->append("!");
			break;
		}
		default:
			fail("UnaryIR::code generation");
	}
	int prec = expr_->prec();
	if (getPrec(op_->type) >= prec) {
		text->append("(");
		expr_->genChannelBased(cg, text);
		text->append(")");
	} else
		expr_->genChannelBased(cg, text);
}

void AggValue::genChannelBased(CodeGen *cg, Text *text) {
	text->append(cg->getCM()->getName(ch_));
}

void VertexId::genChannelBased(CodeGen *cg, Text *text) {
	string lv = cg->getLoopVar(id_);
	sprintf(buf, "get_id(%s)", lv.c_str());
	text->append(buf);
}

void VertexAttr::genChannelBased(CodeGen *cg, Text *text) {
	string va = cg->getMM()->getName(id_);
	string lv = cg->getLoopVar(id_);
	sprintf(buf, "%s[%s]", va.c_str(), lv.c_str());
	text->append(buf);
}

void VecValue::genChannelBased(CodeGen *cg, Text *text) {
	string va = cg->getMM()->getName(id_);
	string lv = cg->getLoopVar(id_);
	sprintf(buf, "%s[%s]", va.c_str(), lv.c_str());
	text->append(buf);
}

void Constructor::genChannelBased(CodeGen *cg, Text *text) {
	switch (list_.size()) {
		case 0: {
			text->append("null");
			return;
		}
		case 1: {
			list_[0]->genChannelBased(cg, text);
			return;
		}
		case 2: {
			text->append("make_pair");
			break;
		}
		default: {
			text->append(type_->toString(cg));
		}
	}
	text->append("(");
	list_[0]->genChannelBased(cg, text);
	for (int i = 1; i < list_.size(); i++) {
		text->append(", ");
		list_[i]->genChannelBased(cg, text);
	}
	text->append(")");	
}

void SelectIR::genChannelBased(CodeGen *cg, Text *text) {
	Type *t = expr_->type();
	if (typeid(*t) != typeid(TupleType) && typeid(*t) != typeid(PairType))
		fail("SelectIR::gen() -- type error?!");
	CompoundType *ct = (CompoundType*) expr_->type();
	expr_->genChannelBased(cg, text);
	text->append(ct->access(idx_));
}

void BuiltinIR::genChannelBased(CodeGen *cg, Text *text) {
	text->append(str_);
}

void GetRespValue::genChannelBased(CodeGen *cg, Text *text) {
	string cn = cg->getCM()->getName(ch_);
	text->append(cn + ".get_value(" + cg->currLoopVar() + ")");
}

void ArrayCopy::genChannelBased(CodeGen *cg) {
	string d = cg->getMM()->getName(dst_);
	string s = cg->getMM()->getName(src_);
	cg->add(new Text(d + " = " + s + ";"));
}

void ArrayCompare::genChannelBased(CodeGen *cg) {
	string lhs = cg->getMM()->getName(lhs_);
	string rhs = cg->getMM()->getName(rhs_);
	string agg = cg->getCM()->getName(ch_);
	cg->add(new Text(agg + " = (" + lhs + " == " + rhs + ");"));
}

void ClearArray::genChannelBased(CodeGen *cg) {
	return; // disabled
	string s = cg->getMM()->getName(id_);
	cg->add(new Text(s + ".clear();"));
}

void InitAgg::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	CombinerInfo *comb = cg->getCM()->getCombiner(ch_);
	Text *text = new Text(cn + " = ");
	if (expr_ == nullptr) {
		text->append(comb->getIdentity());
	} else {
		expr_->genChannelBased(cg, text);
	}
	text->append(";");
	cg->add(text);
}

void SetAgg::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	CombinerInfo *comb = cg->getCM()->getCombiner(ch_);
	Text *text = new Text();
	text->append(cn + " " + comb->getOp2() + " ");
	expr_->genChannelBased(cg, text);
	text->append(";");
	cg->add(text);
}

void AllReduce::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	CombinerInfo *comb = cg->getCM()->getCombiner(ch_);
	string mpi = comb->t->toMPIType(cg);
	string op = comb->getOp3();
	Text *text = new Text("MPI_Allreduce(MPI_IN_PLACE, &" + cn + ", 1, "
			+ mpi + ", " + op + ", MPI_COMM_WORLD);");
	cg->add(text);
}

void AssignIR::genChannelBased(CodeGen *cg) {
	Text *text = new Text();
	lval_->genChannelBased(cg, text);
	text->append(" = ");
	expr_->genChannelBased(cg, text);
	text->append(";");
	cg->add(text);
}

void CondIR::genChannelBased(CodeGen *cg) {
	Text *text = new Text("if (");
 	cond_->genChannelBased(cg, text);
 	text->append(")");
 	cg->add(text);
 	cg->add(new Increase());
 	body_->genChannelBased(cg);
 	cg->add(new Decrease());
}

void SendMessage::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	Text *text = new Text();
	ChannelInfo *info = cg->getCM()->getInfo(ch_);
	if (typeid(*info) == typeid(MsgChannel))
		text->append(cn + ".send_message(");
	else
		text->append(cn + ".add_message(");
	dest_->genChannelBased(cg, text);
	text->append(", ");
	expr_->genChannelBased(cg, text);
	text->append(");");
	cg->add(text);
}

void AddEdge::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	string vi = cg->getLoopVar(id_);
	Text *text = new Text();
	text->append(cn + ".add_edge(" + vi + ", ");
	expr_->genChannelBased(cg, text);
	text->append(");");
	cg->add(text);
}

void AddRequestIR::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	Text *text = new Text();
	text->append(cn + ".add_request(");
	dest_->genChannelBased(cg, text);
	text->append(");");
	cg->add(text);
}

void RequestIR::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	Text *text = new Text();
	text->append(cn + ".add_requests(");
	text->append(cg->getMM()->getName(id_));
	text->append(");");
	cg->add(text);
}

void RespondIR::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	string vn = cg->getMM()->getName(id_);
	cg->add(new Text(cn + ".respond(" + vn + ");"));
}

// TODO: class name!!!!
void SetMessage::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	string vn = cg->getMM()->getName(id_);
	cg->add(new Text(cn + ".scatter(" + vn + ");"));
}

void Activate::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	cg->add(new Text(cn + ".activate();"));
}

void OmpForReduction::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	CombinerInfo *comb = cg->getCM()->getCombiner(ch_);
	Text *text = new Text("#pragma omp parallel for reduction ("
			+ comb->getOp1() + ":" + cn + ")");
	cg->add(text);
	p_->genChannelBased(cg);
}

void ScanArray::genChannelBased(CodeGen *cg) {
	string lv = cg->pushLoopVar();
	c_->setLoopVar(cg, lv);
	sprintf(buf, "for (int %s = 0; %s < numv(); %s++)",
			lv.c_str(), lv.c_str(), lv.c_str());
	cg->add(new Text(buf));
	cg->add(new Increase());
	p_->genChannelBased(cg);
	cg->add(new Decrease());
	c_->clear(cg);
}

void MapArray::genChannelBased(CodeGen *cg) {
	string lv = cg->pushLoopVar();
	string va = cg->getMM()->getName(id_); // name of the vertex attribute
	c_->setLoopVar(cg, lv);
	// cg->add(new Text(va + ".resize(numv());"));
	sprintf(buf, "for (int %s = 0; %s < numv(); %s++)",
			lv.c_str(), lv.c_str(), lv.c_str());
	cg->add(new Text(buf));
	cg->add(new Increase());
	Text *text = new Text(va + "[" + lv + "] = ");
	expr_->genChannelBased(cg, text);
	text->append(";");	
	cg->add(text);
	cg->add(new Decrease());
	c_->clear(cg);
}

void ScanVector::genChannelBased(CodeGen *cg) {
	string it = cg->pushLoopVar();
	string va = cg->getMM()->getName(c_->getId()); // name of the vector
	c_->setLoopVar(cg, it);
	sprintf(buf, "for (int %s = 0; %s < %s.size(); %s++)",
			it.c_str(), it.c_str(), va.c_str(), it.c_str());
	cg->add(new Text(buf));
	cg->add(new Increase());
	p_->genChannelBased(cg);
	cg->add(new Decrease());
	c_->clear(cg);
}

void ArrayFromChannel::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	string va = cg->getMM()->getName(id_);
	cg->add(new Text(cn + ".collect(" + va + ");"));
}

void VectorFromChannel::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	string va = cg->getMM()->getName(id_);
	cg->add(new Text(cn + ".collect(" + va + ");"));
}

void ClearChannel::genChannelBased(CodeGen *cg) {
	string cn = cg->getCM()->getName(ch_);
	cg->add(new Text(cn + ".clear();"));
}

void Join::genChannelBased(CodeGen *cg) {
	string v = cg->pushLoopVar();
	string i = cg->pushLoopVar();
	string j = cg->pushLoopVar();
	string nl = cg->getMM()->getName(l_->getId());
	string nr = cg->getMM()->getName(r_->getId());
	l_->setLoopVar(cg, i);
	r_->setLoopVar(cg, j);
	cg->add(new Text("vector<int> r1 = get_range(" + nl + ");"));
	cg->add(new Text("vector<int> r2 = get_range(" + nr + ");"));
	sprintf(buf, "for (int %s = 0; %s < numv(); %s++) {",
			v.c_str(), v.c_str(), v.c_str());
	cg->add(new Text(string(buf)));
	cg->add(new Increase());
	sprintf(buf, "for (int %s = r1[%s]; %s < r1[%s + 1]; %s++)",
			i.c_str(), v.c_str(), i.c_str(), v.c_str(), i.c_str());
	cg->add(new Text(string(buf)));
	cg->add(new Increase());
	sprintf(buf, "for (int %s = r2[%s]; %s < r2[%s + 1]; %s++) {",
			j.c_str(), v.c_str(), j.c_str(), v.c_str(), j.c_str());
	cg->add(new Text(string(buf)));	
	cg->add(new Increase());
	p_->genChannelBased(cg);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Decrease());
	cg->add(new Decrease());
	cg->add(new Text("}"));
}

void Join2::genChannelBased(CodeGen *cg) {
	// outer loop
	string vi = cg->pushLoopVar(); // for vertex table
	string vj = cg->pushLoopVar(); // for vector
	string vec = cg->getMM()->getName(vec_->getId());
	arr_->setLoopVar(cg, vi);
	vec_->setLoopVar(cg, vj);
	sprintf(buf, "for (int %s = 0, %s = 0; %s < %s.size(); %s++) {",
			vi.c_str(), vj.c_str(), vj.c_str(), vec.c_str(), vj.c_str());
	cg->add(new Text(string(buf)));
	cg->add(new Increase());
	sprintf(buf, "while (get_id(%s) != %s[%s].first) %s++;",
			vi.c_str(), vec.c_str(), vj.c_str(), vi.c_str());
	cg->add(new Text(string(buf)));
	p_->genChannelBased(cg);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	arr_->clear(cg);
	vec_->clear(cg);
}

void JoinVectors::genChannelBased(CodeGen *cg) {
	fail("JoinVectors::code generation not implemented");
}

void VertexArray::setLoopVar(CodeGen *cg, const string &var) const {
	cg->bindLoopVar(id_, var);
}

void ZippedArray::setLoopVar(CodeGen *cg, const std::string &var) const {
	v1_->setLoopVar(cg, var);
	v2_->setLoopVar(cg, var);
}

void Vector::setLoopVar(CodeGen *cg, const string &var) const {
	cg->bindLoopVar(id_, var);
}

void ZippedVector::setLoopVar(CodeGen *cg, const std::string &var) const {
	v1_->setLoopVar(cg, var);
	v2_->setLoopVar(cg, var);
}

void VertexArray::inc() {
	cr_++;
}

void ZippedArray::inc() {
	v1_->inc();
	v2_->inc();
}

void Vector::inc() {
	cr_++;
}

void ZippedVector::inc() {
	v1_->inc();
	v2_->inc();
}

void VertexArray::clear(CodeGen *cg) {
	return; // disabled
	if ((--cr_) == 0) {
		string cn = cg->getMM()->getName(id_);
		cg->add(new Text(cn + ".clear();"));
	}
}

void ZippedArray::clear(CodeGen *cg) {
	v1_->clear(cg);
	v2_->clear(cg);
}

void Vector::clear(CodeGen *cg) {
	return; // disabled
	if ((--cr_) == 0) {
		string cn = cg->getMM()->getName(id_);
		cg->add(new Text(cn + ".clear();"));
	}
}

void ZippedVector::clear(CodeGen *cg) {
	v1_->clear(cg);
	v2_->clear(cg);
}

bool ArrayIR::uniqueKey() const {
	return true;
}

bool VectorIR::uniqueKey() const {
	return false;
}

int VertexArray::getId() const {
	return id_;
}

int ZippedArray::getId() const {
	return v1_->getId();
}

int Vector::getId() const {
	return id_;
}

int ZippedVector::getId() const {
	return v1_->getId();
}

void Superstep::add(StmtIR *stmt) {
	vec_.push_back(stmt);
}

void Superstep::append(Superstep *s) {
	for (auto stmt : s->vec_)
		vec_.push_back(stmt);
}

void Superstep::dump(FILE *f) const {
	for (auto stmt : vec_)
		debug(stmt, f);
}

void Superstep::genChannelBased(CodeGen *cg) {
	for (StmtIR *stmt : vec_)
		stmt->genChannelBased(cg);
}

void STM::print(FILE *f) const {
	dump(f, 0);
}

Superstep *SeqSTM::head() {
	return head_;
}

Superstep *SeqSTM::dupStep() {
	Superstep *ret = new Superstep();
	ret->append(head_);
	return ret;
}

STM *SeqSTM::next() {
	return next_;
}

STM *SeqSTM::jump() {
	return (jump_ == nullptr ? next_ : jump_);
}

void SeqSTM::setJump(STM *jump) {
	jump_ = jump;
}

bool SeqSTM::isReading() const {
	return reading_;
}

void SeqSTM::setStep(CodeGen *cg, int step) {
	step_ = step;
	if (next_ != nullptr)
		next_->setStep(cg, cg->nextStep());
}

void SeqSTM::genChannelBased(CodeGen *cg) {
	sprintf(buf, "if (phase == %d) {", step_);
	cg->add(new Text(buf));
	cg->add(new Increase());
	head_->genChannelBased(cg);
	cg->addJump(jump(), step_);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	if (next_ != nullptr)
		next_->genChannelBased(cg);
}

void SeqSTM::append(SeqSTM *stm) {
	if (next_ == nullptr) {
		head_->append(stm->head_);
		next_ = stm->next_;
		jump_ = stm->jump_;
	} else
		next_->append(stm);
}

void SeqSTM::add(STM *stm) {
	if (next_ == nullptr) {
		next_ = stm;
	} else
		next_->add(stm);
}

void SeqSTM::dump(FILE *f, int step) const {
	fprintf(f, "-------- [STEP %2d] --------\n", step);
	head_->dump(f);
	if (next_ != nullptr)
		next_->dump(f, step + 1);
}

void CondSTM::append(SeqSTM *stm) {
	exit_->append(stm);
}

void CondSTM::add(STM *stm) {
	exit_->add(stm);
}

void CondSTM::dump(FILE *f, int step) const {
	fprintf(f, "-------- [CondSTM] --------\n");
	cond_->print(f);
	fprintf(f, "\n");
	fprintf(f, "-------- [Branch1] --------\n");
	loop_->dump(f, step);
	fprintf(f, "-------- [Branch2] --------\n");
	exit_->dump(f, step);
}

void CondSTM::setStep(CodeGen *cg, int step) {
	step_ = step;
	loop_->setStep(cg, step);
	exit_->setStep(cg, step);
}

void CondSTM::genChannelBased(CodeGen *cg) {
	sprintf(buf, "if (phase == %d) {", step_);
	cg->add(new Text(buf));
	cg->add(new Increase());
	Text *text = new Text("if (");
	cond_->genChannelBased(cg, text);
	text->append(") {");
	cg->add(text);
	cg->add(new Increase());
	loop_->head()->genChannelBased(cg);
	cg->addJump(loop_->jump(), step_);
	cg->add(new Decrease());
	cg->add(new Text("} else {"));
	cg->add(new Increase());
	exit_->head()->genChannelBased(cg);
	cg->addJump(exit_->jump(), step_);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	if (loop_->next() != nullptr)
		loop_->next()->genChannelBased(cg);
	if (exit_->next() != nullptr)
		exit_->next()->genChannelBased(cg);
}

IREnv::IREnv() {
	cm_ = new ChannelManager();
	mm_ = new MemoryManager();
}

void IREnv::reset(int numStep) {
	steps_.clear();
	for (int i = 0; i <= numStep; i++)
		steps_.push_back(new Superstep());
}

void IREnv::addInit(int lv, StmtIR *ir) {
	fail(inits_.empty(), "IREnv::addInit() -- empty");
	inits_[lv]->add(ir);
}

void IREnv::add(int step, StmtIR *ir) {
	fail(step >= steps_.size(), "IREnv::add() -- index out of range");
	steps_[step]->add(ir);
}

SeqSTM *IREnv::produceSTM(bool db) {
	SeqSTM *prog = nullptr;
	fail(steps_.empty(), "IREnv::produceSTM(): empty array");
	prog = new SeqSTM(false, steps_.back(), prog);
	int size = steps_.size();
	for (int i = size - 2; i >= 0; i--)
		prog = new SeqSTM(true, steps_[i], prog);
	steps_.clear();
	return prog;
}

void IREnv::enterLoop() {
	inits_.push_back(new Superstep());
}

SeqSTM *IREnv::exitLoop() {
	SeqSTM *ret = new SeqSTM(false, inits_.back(), nullptr);
	inits_.pop_back();
	return ret;
}

ChannelManager *IREnv::getCM() const {
	return cm_;
}

MemoryManager *IREnv::getMM() const {
	return mm_;
}

StmtIR *Conj2IR(const vector<ExprIR*> &conds, StmtIR *stmt) {
	for (ExprIR *e : conds)
		stmt = new CondIR(e, stmt);
	return stmt;
}

static IREnv *env = new IREnv();
IREnv *getIREnv() {
	return env;
}

SeqSTM *transform(Program *prog, bool db) {
	ControlPlan *pl = prog->transform(getTypeEnv());
	if (db) debug(pl, stderr);
	IREnv *env = getIREnv();
	CodeGen *cg = getCodeGen();
	cg->setCM(getIREnv()->getCM());
	cg->setMM(getIREnv()->getMM());
	pl->prepare(env, 0);
	return pl->transform(env);
}
