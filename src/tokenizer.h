#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <cstdio>
#include <cstdlib>

#include "errorinfo.h"

enum TokenID : int {
	tok_eof = 1,
	// numeric
	tok_ival,
	tok_fval,
	tok_bval,
	// identifier
	tok_name,
	tok_tname,
	// symbols
	tok_lparen,
	tok_rparen,
	tok_dot,
	tok_comma,
	tok_colon,
	tok_semicolon,
	// keywords
	tok_select,
	tok_from,
	tok_where,
	tok_join,
	tok_and,
	tok_or,
	tok_not,
	tok_in,
	tok_group,
	tok_by,
	tok_set,
	tok_while,
	tok_do,
	tok_until,
	tok_fix,
	tok_end,
	tok_for,
	tok_to,
	tok_if,
	tok_then,
	tok_else,
	tok_input,
	tok_void,
	tok_vid,
	tok_int,
	tok_bool,
	tok_float,
	// table schema:
	tok_vt, // vertex table
	tok_et, // edge table
	tok_gv, // global value
	tok_id, // in degree
	tok_od, // out degree
	tok_combiner,
	tok_identity,
	tok_inf,
	// operations
	tok_add,
	tok_sub,
	tok_mul,
	tok_div,
	tok_mod,
	tok_xor,
	tok_eq,
	tok_ne,
	tok_lt,
	tok_le,
	tok_gt,
	tok_ge,
	// build_ins
	tok_max,
	tok_min,
	tok_sum,
	tok_cnt,
	tok_avg,
	tok_END
};

struct Token : Pos {
	int type;
	char *val;

	Token():type(0),val(nullptr) {}
	Token(int row, int col, int type, char *val = nullptr) :
			Pos(row, col), type(type), val(val) {}
};

void setInputStream(FILE *f);
void initTokenizer();

// checkpoint
void lexerSetCP();
void lexerRollBack();
void lexerClear();

Token getNextToken();

const char *getTokenStr(Token tok);
const char *getTokenStr(int type);

const char *getCppTokenStr(Token tok);
const char *getCppTokenStr(int type);

#endif
