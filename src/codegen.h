#ifndef CODEGEN_H
#define CODEGEN_H

#include <vector>
#include <map>
#include <set>

#include "types.h"

// from ast.h
class Program;
// from intermediate.h
class MemoryManager;
class ChannelManager;
class VariableManager;
class STM;
class SeqSTM;
// from query.h
class Content;

class Doc {
public:
	virtual int print(int lvl, FILE *f) const = 0;
};

class Text : public Doc {
protected:
	std::string cont_;
public:
	Text() {}
	Text(std::string s):cont_(s) {}

	size_t length() const;
	char back() const;
	void clear();
	void append(const std::string &s);
	const std::string& cont() const;
	// override
	int print(int lvl, FILE *f) const override;
};

class Increase : public Doc {
public:
	Increase() {}

	int print(int lvl, FILE *f) const override;
};

class Decrease : public Doc {
public:
	Decrease() {}

	int print(int lvl, FILE *f) const override;
};

class CodeGen {
private:
	bool ext_; // whether external function is used
	std::vector<Doc*> seq_; // the whole code
	MemoryManager *mm_;
	ChannelManager *cm_;
	VariableManager *vm_;

	std::map<STM*, int> stm2step_;
	int step_;         // for assigning step number
	int loopVar_;
	std::map<int, std::string> bind_;

	// type for vertex/edge
	Type *key_;

public:
	CodeGen():vm_(nullptr), mm_(nullptr), cm_(nullptr),
			ext_(false), step_(0), loopVar_(0),
			key_(getIntType()) {}

	void add(Doc *f) { seq_.push_back(f); }
	// channel manager
	void setCM(ChannelManager *cm) { cm_ = cm; }
	ChannelManager *getCM() { return cm_; }
	// message manager
	void setMM(MemoryManager *mm) { mm_ = mm; }
	MemoryManager *getMM() {return mm_; }

	// ...
	int nextStep();
	// type / or user-defined structure
	Type *key() const { return key_; }
	// (de)allocate loop variables
	std::string pushLoopVar();
	std::string currLoopVar() const;
	void bindLoopVar(int uid, const std::string &var);
	std::string getLoopVar(int uid) const;
	// code generation
	void addJump(STM *s, int step);
	void pregel_header();
	void pregel_worker(SeqSTM *prog);
	void pregel_main();
	// void dump
	void dump(FILE *f = stderr);
};

CodeGen *getCodeGen();
void genChannelBased(Program *prog, SeqSTM *stm, FILE *f = stdout, bool debug = false);

#endif
