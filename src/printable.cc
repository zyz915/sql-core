#include <cstdio>

#include "printable.h"

void debug(Printable *obj, FILE *f) {
	obj->print(f);
	fprintf(f, "\n");
	fflush(f);
}