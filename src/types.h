#ifndef TYPES_H
#define TYPES_H

#include <map>
#include <set>
#include <string>
#include <vector>
#include <typeinfo>

#include "errorinfo.h"

// from ast.h
class Identifier;
class Program;
class TypeVar;
class TypeEnv;
class ExprIR;
class TableName;
// from query.h
class Context;
// from codegen.h
class CodeGen;

class Type {
public:
	Type() {}
	virtual ~Type() {}
	virtual bool equals(Type *t) const;
	virtual int bytes() const = 0;
	virtual void print(FILE *f) const = 0;
	virtual std::string toString(CodeGen *cg) const = 0;
	virtual std::string toMPIType(CodeGen *cg) const = 0;
};

class VidType : public Type {
private:
	VidType():Type() {}

public:
	static VidType *vtype;

	VidType(const VidType &) = delete;
	VidType &operator=(const VidType &) = delete;

	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
};

class IntType : public Type {
private:
	IntType():Type() {}

public:
	static IntType *itype;

	IntType(const IntType &) = delete;
	IntType &operator=(const IntType &) = delete;

	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
};

class FloatType : public Type {
private:
	FloatType():Type() {}

public:
	static FloatType *ftype;

	FloatType(const FloatType &) = delete;
	FloatType &operator=(const FloatType &) = delete;

	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
};

class UnitType : public Type {
private:
	UnitType():Type() {}

public:
	static UnitType *utype;

	UnitType(const FloatType &) = delete;
	UnitType &operator=(const FloatType &) = delete;

	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
};

class CompoundType : public Type {
public:
	CompoundType():Type() {}

	virtual std::string access(int index) const = 0;
};

class PairType : public CompoundType {
private:
	Type *l_, *r_;

public:
	PairType(Type *l, Type *r):l_(l), r_(r) {}

	bool equals(Type *t) const override;
	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
	std::string access(int index) const override;
};

class TupleType : public CompoundType {
private:
	std::vector<Type*> list_;

public:
	TupleType() {}
	TupleType(std::initializer_list<Type*> ts):list_(ts) {}

	void add(Type *t);
	int size() const;

	bool equals(Type *t) const override;
	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
	std::string access(int index) const override;
};

class BoolType : public Type {
public:
	static BoolType *btype;

	BoolType():Type() {}
	~BoolType() {}

	int bytes() const override;
	void print(FILE *f) const override;
	std::string toString(CodeGen *cg) const override;
	std::string toMPIType(CodeGen *cg) const override;
};

class Schema {
protected:
	Type *type_;

public:
	Schema(Type *t):type_(t) {}
	virtual ~Schema() {}

	virtual Type *type() const final {
		return type_;
	}

	virtual double numRows() const = 0;
	virtual int numCols() const = 0;
	virtual bool equals(Schema *other) const;
	virtual Type *getType(int index) const = 0;
	virtual void dump(FILE *f = stderr) const;
};

class LoopInfo {
private:
	std::vector<std::set<std::string> >mod_;
	std::vector<int> mask_;

public:
	LoopInfo() {}

	int push(const std::set<std::string> &s);
	void pop();
	bool insideLoop();

	void calcMask(Context *context);
	int calcLevel(int mask);
};

class TypeEnv {
private:
	class Scope {
	protected:
		std::map<std::string, Type*> types_;
	public:
		Scope() {}
		~Scope() {}

		void add(Identifier *name, Type *t);
		Type *get(Identifier *name) const;
		bool exists(Identifier *name) const;
	};

	std::vector<Scope*> stack_;
	std::map<std::string, Schema*> tables_;
	LoopInfo *loopInfo_ = nullptr;

public:
	TypeEnv();

	// flag: 1 -> input, 2 -> output
	void addTable(TableName *tn, Schema *s);
	bool tableExists(TableName *tn) const;
	Schema *getSchema(TableName *tn) const;

	void addConstraint(Type *t1, Type *t2, Pos pos) const;
	Type *checkArithmicOp(Type *t1, Type *t2, Pos pos) const;
	bool exists(Identifier *name) const;
	void addBinding(Identifier *name, Type *t);
	Type *getType(Identifier *name) const;
	Type *realType(Type *t) const; // convert VidType to others

	void pushScope();
	void popScope();

	LoopInfo *getLoopInfo();
};

Type *getIntType();
Type *getVidType();
BoolType *getBoolType();
FloatType *getFloatType();
UnitType *getUnitType();

TypeEnv *getTypeEnv();
void typeCheck(Program *prog, bool debug = false);
void debug(const Type *t, FILE *f = stderr);

#endif
