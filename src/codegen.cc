#include <vector>
#include <algorithm>

#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "intermediate.h"
#include "types.h"

using namespace std;

static char buf[128];

size_t Text::length() const {
	return cont_.length();
}

char Text::back() const {
	return cont_.back();
}

void Text::clear() {
	cont_.clear();
}

void Text::append(const string &s) {
	cont_ += s;
}

const string& Text::cont() const {
	return cont_;
}

/* Implement the interfaces for class Doc
 *   void print(int lvl, FILE *f);
 */
int Text::print(int lvl, FILE *f) const {
	int spaces = getConfig()->spaces;
	if (spaces > 0) {
		for (int i = 0; i < lvl * spaces; i++)
			fputc(' ', f);
	} else {
		for (int i = 0; i < lvl; i++)
			fputc('\t', f);
	}
	fprintf(f, "%s\n", cont_.c_str());
	return lvl;
}

int Increase::print(int lvl, FILE *f) const {
	return lvl + 1;
}

int Decrease::print(int lvl, FILE *f) const {
	return lvl - 1;
}

string CodeGen::pushLoopVar() {
	++loopVar_;
	sprintf(buf, "i%d", loopVar_);
	return string(buf);
}

string CodeGen::currLoopVar() const {
	sprintf(buf, "i%d", loopVar_);
	return string(buf);
}

void CodeGen::bindLoopVar(int uid, const string &var) {
	bind_[uid] = var;
}

string CodeGen::getLoopVar(int uid) const {
	auto it = bind_.find(uid);
	if (it == bind_.end()) {
		sprintf(buf, "undefined loop variable %d", uid);
		prtError(buf);
		return string(buf);
	}
	//fail(it == bind_.end(), "CodeGen::getLoopVar()");
	return it->second;
}

void CodeGen::addJump(STM *s, int step) {
	if (s != nullptr) {
		if (s->step() != step) {
			sprintf(buf, "phase = %d;", s->step());
			add(new Text(string(buf)));
		}
	    add(new Text("return false;"));
	} else
	    add(new Text("return true;"));
}

int CodeGen::nextStep() {
	return ++step_;
}

void CodeGen::pregel_header() {
	add(new Text("#include \"pregel-ext.h\""));
	add(new Text("#include <climits>"));
	add(new Text("#include <cfloat>"));
	add(new Text());
	add(new Text("using namespace std;"));
	add(new Text());
}

void CodeGen::pregel_worker(SeqSTM *stm) {
	Config *config = getConfig();
	string w_class = string("class ") + string(config->worker_name);
	add(new Text(w_class + " : public Worker<int, "
			+ mm_->getEdgeType()->toString(this) + "> {"));
	add(new Text("protected:"));
	add(new Increase());
	// generate the channels
	cm_->listChannels(this);
	add(new Text());
	// tables
	mm_->defineTables(this);
	add(new Text());
	// phase variable
	add(new Text("int phase;"));
	add(new Text());
	add(new Decrease());
	// add vertex attributes
	add(new Text("public:"));
	add(new Increase());
	// constructor
	add(new Text(string(config->worker_name) + "():"));
	add(new Increase());
	cm_->listConstructors(this);
	add(new Text("edge(this), phase(1) {}")); // own variables
	add(new Text());
	add(new Decrease());
	// load tables (no longer needed)
	/*
	add(new Text(key_->toString(this) + " parse_line(const char *line) override {"));
	add(new Increase());
	mm_->parseLine(this);
	add(new Decrease());
	add(new Text("}"));
	add(new Decrease());
	add(new Text());
	// dump tables (not implemented yet)
	add(new Increase());
	add(new Text("void dump_tables() override {"));
	add(new Increase());
	mm_->dumpTables(this);
	add(new Decrease());
	add(new Text("}"));
	add(new Decrease());
	add(new Text());
	*/
	// resize tables
	add(new Text("void load_channels(const EdgeBuffer &es) override {"));
	add(new Increase());
	cm_->loadChannels(this);
	mm_->initTables(this);
	add(new Decrease());
	add(new Text("}"));
	add(new Text());
	// compute function
	add(new Text("bool compute() override {"));
	add(new Increase());
	stm->setStep(this, nextStep());
	stm->genChannelBased(this);
	//add(new Text("pregel_error(\"should not reach\");"));
	add(new Text("return true;"));
	add(new Decrease());
	add(new Text("}"));
	// end of public
	add(new Decrease());
	add(new Text("};"));
	add(new Text());
}

void CodeGen::pregel_main() {
	add(new Text("int main(int argc, char* argv[]) {"));
	add(new Increase());
	add(new Text("pregel_initialize();"));
	Config *config = getConfig();
	string workerName(config->worker_name);
	add(new Text(workerName + " worker;"));
	add(new Text("worker.run(parse_params(argc, argv));"));
	add(new Text("pregel_finalize();"));
	add(new Text("return 0;"));
	add(new Decrease());
	add(new Text("}"));
}

void CodeGen::dump(FILE *f) {
	int lvl = 0;
	for (auto s : seq_)
		lvl = s->print(lvl, f);
}

static CodeGen *cg = new CodeGen();
CodeGen *getCodeGen() {
	return cg;
}

void genChannelBased(Program *prog, SeqSTM *stm, FILE *f, bool d) {
	if (d) debug(stm);
	cg->getMM()->setIO(prog->headers); // input tables
	cg->pregel_header();
	cg->pregel_worker(stm);
	cg->pregel_main();
	cg->dump(f);
}
