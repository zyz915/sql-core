#include "parser.h"
#include "ast.h"

#include <cstdio>

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	Program *prog = parse();
	debug(prog, stdout);
	return 0;
}
