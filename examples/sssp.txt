// Single-Source Shortest Path (SSSP)
input edge_table(void) Edges;

set Dist : vertex_table(int) =
  select u, if (u < 10) then 0 else inf
  from VertexSet(u)

set Active : vertex_table(bool) =
   select u, if (u < 10) then true else false
   from VertexSet(u)

do
  set NewD : vertex_table(int) =
    select v, min(du + 1)
    from Edges(u, v) join Dist(u, du) join Active(u, active)
    where active
    group by v

  set Active : vertex_table(bool) =
    select u, new < old
    from Dist(u, old) join NewD(u, new)

  set Dist : vertex_table(int) =
  	select u, if (new < old) then new else old
  	from Dist(u, old) join NewD(u, new)

until fix(Dist)
